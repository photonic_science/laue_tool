#!/bin/bash
DIR=$PWD
#echo "DIR=${DIR}"
#ls $DIR
CRYSFML="${DIR}/../CrysFML/Src"
echo "#### IN ####"
echo "################# List of CRYSFML/Src/*.f90"
ls -l $CRYSFML
echo " "

#outDIR="${DIR}/../CodePython/LaueTool/bin/"
#echo "outDIR=${outDIR}"
#ls $outDIR


echo "CrysFML for GFortran Compiler (Optimization)"

#@echo off
cd $CRYSFML

echo "**---- Level 0 ----**"
echo ".... Mathematical(I), String_Utilities, Messages, Powder Profiles"

gfortran -c CFML_GlobalDeps_Windows.f90         -O3  -std=f2003 -funroll-loops -m64 -fPIC #>> out

gfortran -c CFML_Math_Gen.f90         -O3  -std=f2003 -funroll-loops -m64 -fPIC #>> out
gfortran -c CFML_LSQ_TypeDef.f90      -O3  -std=f2003 -funroll-loops -m64 -fPIC #>> out
gfortran -c CFML_Spher_Harm.f90       -O3  -std=f2003 -funroll-loops -m64 -fPIC #>> out
gfortran -c CFML_Random.f90           -O3  -std=f2003 -funroll-loops -m64 -fPIC #>> out
gfortran -c CFML_FFTs.f90             -O3  -std=f2003 -funroll-loops -m64 -fPIC #>> out
gfortran -c CFML_String_Util.f90      -O1  -std=f2003 -funroll-loops -m64 -fPIC #>> out
gfortran -c CFML_IO_Mess.f90          -O3  -std=f2003 -funroll-loops -m64 -fPIC #>> out

echo "-std=f2003 removed in TOF because probably there is a conflict of names"
echo " is erfc an intrinsic function in  F2003?"

gfortran -c CFML_Profile_TOF.f90      -O3             -funroll-loops -m64 -fPIC #>> out
gfortran -c CFML_Profile_Finger.f90   -O3  -std=f2003 -funroll-loops -m64 -fPIC #>> out
gfortran -c CFML_Profile_Functs.f90   -O3  -std=f2003 -funroll-loops -m64 -fPIC #>> out

echo "**---- Level 1 ----**"
echo ".... Mathematical(II), Optimization, Tables, Patterns"

gfortran -c CFML_Math_3D.f90          -O3  -std=f2003 -funroll-loops -m64 -fPIC #>> out
gfortran -c CFML_Optimization.f90     -O3  -std=f2003 -funroll-loops -m64 -fPIC #>> out
gfortran -c CFML_Optimization_LSQ.f90 -O3  -std=f2003 -funroll-loops -m64 -fPIC #>> out
gfortran -c CFML_Sym_Table.f90        -O0  -std=f2003 -funroll-loops -m64 -fPIC #>> out
gfortran -c CFML_Chem_Scatt.f90       -O0  -std=f2003 -funroll-loops -m64 -fPIC #>> out
gfortran -c CFML_Diffpatt.f90         -O3  -std=f2003 -funroll-loops -m64 -fPIC #>> out

echo "**---- Level 2 ----**"
echo ".... Bonds, Crystal Metrics, Symmetry, ILL_Instr"

gfortran -c CFML_Bonds_Table.f90      -O0  -std=f2003 -funroll-loops -m64 -fPIC #>> out
gfortran -c CFML_Cryst_Types.f90      -O3  -std=f2003 -funroll-loops -m64 -fPIC #>> out

gfortran -c CFML_Symmetry.f90         -O3  -std=f2003 -funroll-loops -m64 -fPIC #>> out

gfortran -c CFML_ILL_Instrm_Data.f90  -O3  -std=gnu   -funroll-loops -m64 -fPIC #>> out

echo "**---- Level 3 ----**"
echo ".... Reflections, Atoms, SXTAL geometry"

gfortran -c CFML_Reflct_Util.f90      -O3  -std=f2003 -funroll-loops -m64 -fPIC #>> out
gfortran -c CFML_Atom_Mod.f90         -O3  -std=f2003 -funroll-loops -m64 -fPIC #>> out
gfortran -c CFML_SXTAL_Geom.f90       -O3  -std=f2003 -funroll-loops -m64 -fPIC #>> out

echo "**---- Level 4 ----**"
echo ".... Structure Factors, Geometry Calculations, Propag Vectors"

gfortran -c CFML_Sfac.f90             -O3  -std=f2003 -funroll-loops -m64 -fPIC #>> out
gfortran -c CFML_Geom_Calc.f90        -O3  -std=f2003 -funroll-loops -m64 -fPIC #>> out
gfortran -c CFML_Propagk.f90          -O3  -std=f2003 -funroll-loops -m64 -fPIC #>> out

echo "**---- Level 5 ----**"
echo ".... Molecules, Maps, BVS, Energy Configurations"

gfortran -c CFML_Maps.f90             -O3  -std=f2003 -funroll-loops -m64 -fPIC #>> out
gfortran -c CFML_Molecules.f90        -O3  -std=f2003 -funroll-loops -m64 -fPIC #>> out

echo "-std=f2003 removed because calls to flush subroutine"

gfortran -c CFML_Conf_Calc.f90        -O3             -funroll-loops -m64 -fPIC #>> out

echo "**---- Level 6 ----**"
echo ".... Formats"

gfortran -c CFML_Form_CIF.f90         -O1  -std=f2003 -funroll-loops -m64 -fPIC #>> out
gfortran -c CFML_Export_Vtk.f90       -O3  -std=gnu   -funroll-loops -m64 -fPIC #>> out

echo "**---- Level 7 ----**"
echo ".... Keywords Parser, Simulated Annealing, Magnetic Symmetry"

echo "-std=f2003 removed because calls to flush subroutine"

gfortran -c CFML_Optimization_SAn.f90 -O3             -funroll-loops -m64 -fPIC #>> out
gfortran -c CFML_MagSymm.f90          -O3  -std=f2003 -funroll-loops -m64 -fPIC #>> out
gfortran -c CFML_Refcodes.f90         -O3  -std=f2003 -funroll-loops -m64 -fPIC #>> out

echo "**---- Level 8 ----**"
echo ".... Magnetic Structure Factors, Polarimetry"

gfortran -c CFML_Msfac.f90            -O3  -std=f2003 -funroll-loops -m64 -fPIC #>> out
gfortran -c CFML_Polar.f90            -O3  -std=f2003 -funroll-loops -m64 -fPIC #>> out

echo "**---- Crysfml Library: Console version ----**"

ar cr libcrysfml.a *.o

echo "**---- GFortran Directory ----**"

rm -rf ./../../GFortran
mkdir -p ./../../GFortran//LibWin64

cp *.mod ./../../GFortran/LibWin64
mv *.a ./../../GFortran/LibWin64
rm *.o
rm *.mod
rm *.lst
rm *.bak

echo "#### OUT #### "
echo "################# List of generated files in GFortran/LibWin64/"
ls -l ./../../GFortran/LibWin64
echo " "

cd $DIR
