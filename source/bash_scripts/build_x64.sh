#!/bin/bash
rm -rf ../GFortran/
rm ./*_log.txt
chmod +x make_psl_orient_dll_x64.sh make_crysfml_win64.sh
dos2unix -o make_psl_orient_dll_x64.sh make_crysfml_win64.sh
./make_crysfml_win64.sh | tee ./make_crysfml_win64_log.txt
./make_psl_orient_dll_x64.sh | tee ./make_psl_orient_dll_x64_log.txt
