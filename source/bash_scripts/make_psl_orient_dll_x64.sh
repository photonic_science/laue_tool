#!/bin/bash
DIR=$PWD
#echo "DIR=${DIR}"
#ls $DIR
LAUESUI="${DIR}/../sxtalsoft/Laue_Suite_Project/Src"
echo "#### IN ####"
echo "################# List of sxtalsoft/Laue_Suite_Project/Src/*.f90"
ls -l $LAUESUI
echo " "
#echo "LAUESUI=${LAUESUI}"

CRYSFML="${DIR}/../GFortran/LibWin64"
echo "#### IN ####"
echo "################# List of generated files in GFortran/LibWin64/"
ls -l $CRYSFML
echo " "
#echo "CRYSFML=${CRYSFML}"

mkdir -p $CRYSFML
outDIR="${DIR}/../CodePython/LaueTool/bin/"
#echo "outDIR=${outDIR}"
#ls $outDIR
mkdir -p $outDIR

gfortran $LAUESUI/gfortran_specific.f90 -c -O3 -funroll-loops -m64 -fPIC
gfortran $LAUESUI/Laue_Module.f90 -c -O3 -funroll-loops -m64 -I$CRYSFML -fPIC
gfortran $LAUESUI/Data_Trt.f90 -c -O3 -funroll-loops -m64 -I$CRYSFML -fPIC
gfortran $LAUESUI/Laue_Intensity.f90 -c -O3 -funroll-loops -m64 -I$CRYSFML -fPIC
gfortran $LAUESUI/Laue_tiff_read.f90 -c -O3 -funroll-loops -m64 -I$CRYSFML -fno-range-check -fPIC
gfortran $LAUESUI/rnw_bin.f90 -c -O3 -funroll-loops -m64 -fno-range-check -fPIC
gfortran $LAUESUI/Laue_Utilities_Mod.f90 -c -O3 -funroll-loops -m64 -I$CRYSFML -fPIC
gfortran $LAUESUI/Laue_Suite_Global_Parameters.f90 -c -O3 -funroll-loops -m64 -I$CRYSFML -fPIC
gfortran Laue_Sim_Mod.f90 -c -O3 -funroll-loops -m64 -I$CRYSFML -fPIC
gfortran lafg_IMG.f90 -c -O3 -funroll-loops -m64 -I$CRYSFML -fPIC

echo "#### OUT ####"
echo "################# List of generated LAUESUI .mods and other in ./"
ls -l $DIR
echo " "

echo "Attempting to creade shared object psl_calc_x64.dll with compiled files from CRYSFML and LAUESUI"
echo " "
gfortran *.o -shared -o psl_calc_x64.dll -O3 -funroll-loops -m64 -I$CRYSFML -L$CRYSFML -lcrysfml

rm ./*.obj
rm ./*.mod
rm ./*.o
cp psl_calc_x64.dll $outDIR
