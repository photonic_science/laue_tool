!!---- This file is part of the "Esmeralda" project,
!!---- a tool for the treatment of Laue diffraction data
!!----
!!---- The "Esmeralda" project is distributed under LGPL. In agreement with the
!!---- Intergovernmental Convention of the ILL, this software cannot be used
!!---- in military applications.
!!----
!!---- Copyright (C)  2010-2012  Institut Laue-Langevin (ILL), Grenoble, FRANCE
!!----
!!---- Authors: Luis Fuentes-Montero    (ILL)
!!----          Juan Rodriguez-Carvajal (ILL)
!!----
!!---- This library is free software; you can redistribute it and/or
!!---- modify it under the terms of the GNU Lesser General Public
!!---- License as published by the Free Software Foundation; either
!!---- version 3.0 of the License, or (at your option) any later version.
!!----
!!---- This library is distributed in the hope that it will be useful,
!!---- but WITHOUT ANY WARRANTY; without even the implied warranty of
!!---- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
!!---- Lesser General Public License for more details.
!!----
!!---- You should have received a copy of the GNU Lesser General Public
!!---- License along with this library; if not, see <http://www.gnu.org/licenses/>.
!!----
  Module laue_siml_modl

   Use Laue_Suite_Global_Parameters

   Use CFML_GlobalDeps,               only: pi,OPS_SEP
   Use CFML_Math_general,             only: sort
   Use CFML_String_Utilities,         only: pack_string,l_case,Get_Logunit
   Use CFML_crystallographic_symmetry,only: Write_SpaceGroup, Set_SpaceGroup, Space_Group_Type
   Use CFML_Atom_TypeDef,             only: Atom_List_Type, Write_Atom_List
   Use CFML_crystal_metrics,          only: Write_Crystal_Cell, Rot_matrix
   Use CFML_Reflections_Utilities,    only: Reflection_List_Type, Hkl_Uni, Hkl_Gen_Sxtal, get_maxnumref
   Use CFML_IO_Formats,               only: Readn_set_Xtal_Structure, Read_file_cell, Read_File_SPG,err_form_mess,&
                                            err_form,file_list_type,File_to_filelist
   Use CFML_Structure_Factors,        only: Structure_Factors, Write_Structure_Factors, &
                                            Init_Structure_Factors
   use CFML_Geometry_Calc,            only: Matrix_Rx, Matrix_Ry, Matrix_Rz
   use CFML_Propagation_Vectors,      only: Group_k_Type,K_Star,Write_Group_k
   use CFML_Math_3D ,                 only: Cross_Product
   Use compilers_specific
   Use Laue_Mod
   Use Data_Trt,                      only:  Read_Excluded_Regions, integrated_intensity, &
                                            Arrange_Double_PeakList, Mask_Excl !,Excluded_Regions_Type,end_p_bar, ini_p_bar, update_p_bar
   Use Laue_Intensity_Mod
   Use ReadnWrite_Binary_Files,       only: Read_Binary_File_16bits
   Use Laue_Utilities_Mod,            only: Calc_Visible_Reflections
   implicit none
   private
   public :: recalc, finl , Read_CFL_File, Read_instrument, Reflection_generation , F_A_Orient, F_A_Orient_1, &
             Write_Reflection_File, Read_Binary_Image, Write_Binary_Image, Update_Orientation,                &
             Write_CFL_File, Read_Reo_Image, Mask_Trt_f_calculated_pos, arr_ppeak_f_calculated_pos,           &
             Prof_Cut, rotate_around_a_vector, recalc_monochromatic, Output_Indexed_Reflections,              &
             Write_Refinement_File, Set_Mask_HKL, Write_Integrated_Reflections, Integrate_circular_boundary,  &
             Save_Indexed_Peaks_all_images, calc_angle_betwen_vectors

   public :: Calc_and_Dif, Allocate_Pattern_Masks


   Type , Public :: Orient_angl_cond_type
      integer    :: xfrom, xto,xstep, yfrom, yto, ystep, zfrom, zto, zstep, resl
   End Type Orient_angl_cond_type

   Type , Public :: Orient_hkl_rot_cond_type
      integer    :: h_from,h_to,k_from,k_to,l_from,l_to,ang_step
   End Type Orient_hkl_rot_cond_type

   Type (File_List_Type),               save :: fich_cfl
   Type (Reflection_List_Type), public, save :: hkl
   Type (Laue_Ref_Type),                save :: Ref
   Type (Laue_Ref_list_type),   public, save :: ORefL
   Type (Orient_cond_type),     public, save :: Cnd
   Type (Orient_Solution_List), public, save :: S
   Type (Sample_Type),          public, save :: Sample

   Type (Space_Group_type)             :: Sp_Gr !Auxiliary space group

   Character(Len=256)                  :: mess       !Message after reading the CFL file for ref_gen
   Real                                :: stlmax     !Minimum and Maximum Sin(Theta)/Lambda
   Real                                :: delta, sx, sz
   Real,    Dimension(3,3)             :: ub,MRUB
   Real,    Dimension(2)               :: x,xo
   Real,    Dimension(3)               :: sig
   Integer, Dimension(3)               :: ord
   Integer                             :: MaxNumRef, ier,i,j,k, n, L, j1,j2,k1,k2,npx,npz, iop
   Integer                             :: narg, xres, yres, num_im

   Logical                             :: ok=.false.

  Contains

    Subroutine Write_CFL_File(CFLString,CellString,SpgString,InstrString,img_n_first)
      character(len=*),      intent(in)    :: CFLString,CellString,SpgString,InstrString
      integer,               intent(in)    :: img_n_first
      !---- Local Variables ----!
      character(len=80) :: fil_nam   ! name of file where peaks will be saved
      character(len=80) :: tmp_char  ! temporal character var
      Integer           :: i_CFL, i, n, i_f,j

      call Get_Logunit(i_CFL)
      open(unit=i_CFL,file=trim(CFLString),status="replace",action="write")
      write(unit=i_CFL,fmt="(a)")  "TITLE    CFL-file generated from a Laue Suite application"
      write(unit=i_CFL,fmt="(a)")  "!             a         b         c       Alpha     Beta    Gamma"
      write(unit=i_CFL,fmt="(a)")  "CELL    "//trim(CellString)
      write(unit=i_CFL,fmt="(a)")  "!        Space Group symbol"
      write(unit=i_CFL,fmt="(a)")  "SpGR    "//trim(SpgString)
      write(unit=i_CFL,fmt="(/,a)")"!        Instrument file"
      write(unit=i_CFL,fmt="(a)")  "INSTRM  "//trim(InstrString)

      if (img_n_first /= -1) then
          do i=1,size(cfl_file_cond)
            if(cfl_file_cond(i) == 1) then

               Select Case(i)

                 Case(1)  ! Save current orientation
                      write(unit=i_CFL,fmt="(/,a)")       "! Orientation angles of U-matrix (Phi_x, Phi_y, Phi_z)"
                      write(unit=i_CFL,fmt="(a,3f11.5)")"ORIENT ",Curr_U_angles

                 Case(2)  ! Save list of image files in CFL file
                      if(n_img > 0) then
                          write(unit=i_CFL,fmt="(/,a)")"!   List of image file names:"
                          write(unit=i_CFL,fmt="(a,i4)")"IMAGE_FILES ",n_img

                          do j=1,n_img
                              write(unit=i_CFL,fmt="(a)")"   "//trim(File_Im(j))
                          end do

                          write(unit=i_CFL,fmt="(a)")"END_IMAGE_FILES "
                      end if

                 Case(3)  ! Save conditions for peak_find_threshold
                      write(unit=i_CFL,fmt="(/,a)") "!      Conditions for Peak_Find_Threshold (CW) (common to all images)"
                      write(unit=i_CFL,fmt="(a)")   "!                  CutOff   BlockSz   Min.Area  Min.Dist"
                      write(unit=i_CFL,fmt="(a,4i10)")"Peak_Find_CW ", Fnd_param%CutOff, Fnd_param%BlockSz,Fnd_param%pk_ar, &
                      Fnd_param%d_min

                 Case(4)  ! Save conditions for peak_find_LFM
                      write(unit=i_CFL,fmt="(/,a)") "!      Conditions for Peak_Find (LFM) (common to all images)"
                      write(unit=i_CFL,fmt="(a)")   "!               Min.Dist   Min.Slope  Min.Area"
                      write(unit=i_CFL,fmt="(a,3i10)")"Peak_Find_LFM ", Fnd_param%d_min, Fnd_param%m_pk, Fnd_param%pk_ar
                 Case(5) ! Save peaks files
                      write(unit=i_CFL,fmt="(/,a)") "!     Current Peaks"
                      write(unit=i_CFL,fmt="(a,i4)")"peaks_files ",n_img

                      tmp_char=File_Im(img_n_first)
                      j=index(tmp_char,".",back=.true.)
                      if(j /= 0) then
                        fil_nam=tmp_char(1:j-1)//'_orient_pk.peak'
                      else
                        fil_nam=trim(tmp_char)//'_orient_pk.peak'
                      end if
                      !write(unit=*,fmt="(a)") " => File with the proper mask: "//trim(tmp_char)

                      write(unit=i_CFL,fmt="(a)")"   "//trim(fil_nam)

                      call Peak_Write(img_n_first,fil_nam)
                      do i_f=1,n_img,1
                          if (i_f /= img_n_first) then
                              tmp_char=File_Im(i_f)
                              j=index(tmp_char,".",back=.true.)
                              if(j /= 0) then
                                fil_nam=tmp_char(1:j-1)//'_orient_pk.peak'
                              else
                                fil_nam=trim(tmp_char)//'_orient_pk.peak'
                              end if
                              write(unit=i_CFL,fmt="(a)")"   "//trim(fil_nam)
                              call Peak_Write(i_f,fil_nam)
                          end if
                      end do
                      write(unit=i_CFL,fmt="(a)") "end_peaks_files"

                      !The following statement allows to use peaks after clicking without using automatic peak search
                      peaks_found=.true.

                  Case(6)  ! Save conditions for indexing
                      write(unit=i_CFL,fmt="(/,a)") "       Conditions for indexing (common to all images)"
                      write(unit=i_CFL,  fmt="(a)") "GLB_Indexing "
                      write(unit=i_CFL,  fmt="(a)") "! Ang_Type coprime  nmax_ref nmax_sol norient maxind"// &
                      " exhaust angtol  dmmtol ang_min ang_max  Strict"
                      write(unit=i_CFL,  fmt="(a)") "XYZ      .false.      80       20      5      3    "// &
                      "   0     2.5      1.5     30.0    150.0   .true."

                  End Select

              end if
          end do
      else
          write(unit=*,fmt='(a)') ' => No orienting instructions in CFL file'
      end if
      close(unit=i_CFL)
    return
    End Subroutine Write_CFL_File

     ! Subroutine Peak_Write(num_img,nam_fil)
     ! for writing the position and intensities
     ! of maxima in experimental pattern
     ! just the way they are in the mask

    Subroutine Peak_Write(num_img,nam_fil)
        integer ,         intent(in)   :: num_img   ! number of the image
        character(len=80),intent(in)   :: nam_fil   ! file name
        !--- Local Variables ---!
        integer :: i         ! cycle control vars
        real    :: x_wr,y_wr ! coordinates in millimeters, the way that will be saved
        integer :: lun
        integer :: p_cnt_1, p_cnt_2,  p_cnt_tot  ! Number of Experimental peaks
        integer :: contr
        type(Peak_Type),dimension(:), allocatable :: l_peak1,l_peak2      ! Experimental peaks

        call Arrange_Double_PeakList(Data2D(:,:,num_img),Mask2D(:,:,num_img),l_peak1,p_cnt_1,l_peak2,p_cnt_2)
        p_cnt_tot = p_cnt_2 + p_cnt_1

        !  Now save peaks
        call Get_LogUnit(lun)
        open(unit=lun, file=trim(nam_fil), status="replace", action="write")
            write(lun,"(a)") 'For use by another application '
            write(lun,"(a)") 'if you modify the following lines, you ll get what you deserve'
            write(lun,"(a,i8,f10.4)") 'XZ_spots  ', p_cnt_tot, img_spdl(num_img)

            contr = 0
            do i=1,p_cnt_2,1
                call pix_to_mil(LaueDiff,l_peak2(i)%pos(2),l_peak2(i)%pos(1),x_wr,y_wr)
                contr =  contr + 1
                write(lun,"(2f12.4,f14.1,i8)") x_wr,y_wr,l_peak2(i)%intensity,contr
            end do

            do i=1,p_cnt_1,1
                call pix_to_mil(LaueDiff,l_peak1(i)%pos(2),l_peak1(i)%pos(1),x_wr,y_wr)
                contr =  contr + 1
                write(lun,"(2f12.4,f14.1,i8)") x_wr,y_wr,l_peak1(i)%intensity,contr
            end do

        close(unit=lun)
    return
    End Subroutine Peak_Write

    !!----   Subroutine Update_Orientation(phi_angles,spindle,M_matrix,U_angles,chi_phi)
    !!----      Real, dimension(3),   optional, intent(in) :: phi_angles
    !!----      Real,                 optional, intent(in) :: spindle
    !!----      Real, dimension(3,3), optional, intent(in) :: M_matrix
    !!----      Real, dimension(3),   optional, intent(in) :: U_angles
    !!----      Real, dimension(2),   optional, intent(in) :: chi_phi
    !!----
    !!----     M = Rm Ch Ph UB    (M= R UB)
    !!-----    M = Rot B => Rot = Rm.Ch.Ph.U = M B^-1,  U=Pht.Cht,Rmt.Rot= Rt Rot
    !!----   This subroutine update orientation matrices depending on the input arguments.
    !!----   Can be called with either "phi_angles", "M_matrix", "UB_Matrix" or "U_angles".
    !!----   If "Spindle" is not provided it is supposed that the R-matrix is already stored ,
    !!----   as "Current_R" otherwise it is calculated and stored from "Spindle". The subroutine
    !!----   assumes that the Busing-Levy B and B(inv) matrices are stored as BL and BL_Inv.
    !!----   The optional angles Chi and Phi (corresponding to an Eulerian cradle) can be
    !!----   also be given if the orienting device is a four circle diffractometer.
    !!----   The corresponding matrices Ch and Ph are updated and saved each time the
    !!----   call to de subroutine contains the chi_phi argument.
    !!----
    !!----   Created September 2010?: JRC
    !!----   Updated January 2012: JRC
    !!----
    Subroutine Update_Orientation(phi_angles,spindle,UB_Matrix,M_matrix,U_angles,chi_phi)
       Real, dimension(3),   optional, intent(in) :: phi_angles
       Real,                 optional, intent(in) :: spindle
       Real, dimension(3,3), optional, intent(in) :: UB_matrix,M_matrix
       Real, dimension(3),   optional, intent(in) :: U_angles
       Real, dimension(2),   optional, intent(in) :: chi_phi
       !
       Real, dimension(3,3), save :: Rot,Rmt,Rm
       Real, dimension(3,3), save :: R_Ch=reshape((/1.0,0.0,0.0,0.0,1.0,0.0,0.0,0.0,1.0/),(/3,3/)), &
                                     R_Ph=reshape((/1.0,0.0,0.0,0.0,1.0,0.0,0.0,0.0,1.0/),(/3,3/))
       Real :: Phix,Phiy,Phiz

       if(present(phi_angles)) Curr_orient=phi_angles

       if (present(spindle)) then ! Matrix Rm,Spindle corresponds to Omega rotation
        Current_R=Matrix_Rz(-spindle,"D")
       end if
       if (present(chi_phi)) then
         R_Ch=Matrix_Ry(chi_phi(1),"D")  !Chi
         R_Ph=Matrix_Rz(chi_phi(2),"D")  !Phi
       end if
       Rm=matmul(matmul(Current_R,R_Ch),R_Ph)
       Rmt=transpose(Rm)

       ! => M = Rm Ch Ph UB = R UB
       ! => M = Rot B => Rot = Rm.Ch.Ph.U = M B^-1,  U=Pht.Cht,Rmt.Rot = Rt Rot

       if (present(UB_matrix)) then
          Current_UB = UB_Matrix
          Current_M  = Matmul( Rm,Current_UB)
          Current_Rot= Matmul(Current_M,BL_inv)
          Current_U  = Matmul(Rmt,Current_Rot)

       else if (present(M_matrix)) then
          Current_M  = M_matrix
          Current_UB = Matmul( Rmt,Current_M)
          Current_Rot= Matmul(Current_M,BL_inv)
          Current_U  = Matmul(Rmt,Current_Rot)

       else if (present(phi_angles)) then
          !Calculate Rot, U, UB and M matrices corresponding to angles Curr_Orient
          call Set_Rotation_Matrix(Curr_orient,Rot)
          Current_Rot=Rot
          Current_U=Matmul(Rmt,Rot)
          Current_UB=Matmul(Current_U,BL)
          Current_M=Matmul(Rm,Current_UB)

       else if (present(U_angles)) then
          !Calculate Rot, U, UB and M matrices corresponding to angles Curr_Orient
          call Set_Rotation_Matrix(U_angles,Current_U)
          Current_UB=Matmul(Current_U,BL)
          Current_Rot=Matmul(Current_R,Current_U)
          Current_M=Matmul(Rm,Current_UB)

       end if

       if(present(U_angles)) then
         Curr_U_angles=U_angles
       else
         call Get_Phi_xyz(Current_U,Phix,Phiy,Phiz,"D")
         Curr_U_angles=(/Phix,Phiy,Phiz/)
       end if

       if(.not. present(Phi_angles)) then
         call Get_Phi_xyz(Current_Rot,Phix,Phiy,Phiz,"D")
         Curr_Orient=(/Phix,Phiy,Phiz/)
       end if

       return
    End Subroutine Update_Orientation

    Subroutine Read_CFL_File(read_inst,resize)

        character(len=*), optional, intent(in)  :: read_inst
        Logical, optional,          intent(out) :: resize
        !
        character(len=20)  :: spg_symb
        character(len=132) :: line
        integer :: i,j, n, ier, lun, nimg
        logical :: esta,  ok, opnd
        logical :: hkl_gen
        write(unit=*,fmt="(a)") " => Opening <<"//trim(CFL_file)//">> file as a cfl"
        hkl_gen=.false.
        
        i=index(CFL_file,".",back=.true.)
        if(i /= 0) then
          filecod=CFL_file(1:i-1)
        else
          filecod=CFL_file
        end if
        
        
        i=index(filecod,OPS_SEP,back=.true.)
        if(i /= 0) filecod=filecod(i+1:)
        
        call Readn_set_Xtal_Structure(CFL_file,Cell,SpG,A,Mode="CFL",file_list=fich_cfl)
        
        If(err_form) then
          write(unit=*,fmt="(a)") " => "//trim(err_form_mess)
          Cell_Loaded=.false.
        else
          Cell_Loaded=.true.
          struc_fact_new=.true.
          aCell=Cell !Auxiliary cell is initialised to the cell read in CFL file
          if(Program_Name == "Laue_Simulator") then
            inquire(file=trim(filecod)//".sfa", opened=opnd)
            if(.not. opnd) then
              open(unit=i_sf,file=trim(filecod)//".sfa", status="replace",action="write")
            end if
            write(unit=i_sf,fmt="(/,/,7(a,/))")                                               &
                 "            ------ PROGRAM GENERAL_DIFFRACTOMETER_SIMULATOR ------"       , &
                 "                     ---- Version 0.2 November-2009----"                  , &
                 "    *******************************************************************"  , &
                 "    * Generates single crystal reflections and structure factors      *"  , &
                 "    * (if atoms are given ) reading a *.CFL file (Laue geometries)    *"  , &
                 "    *******************************************************************"  , &
                 "                     (JRC- Created in November 2009 )"

            call Write_Crystal_Cell(Cell,i_sf)
            call Write_SpaceGroup(SpG,i_sf)
            if(A%natoms /= 0) call Write_Atom_List(A,lun=i_sf)
          Else if(Program_Name == "Esmeralda") then
            inquire(file=trim(filecod)//".log", opened=opnd)
            if(.not. opnd) then
              open(unit=i_sf,file=trim(filecod)//".log", status="replace",action="write")
            end if
            write(unit=i_sf,fmt="(/,/,7(a,/))")                                               &
                 "                    ------ PROGRAM ESMERALDA ------"                      , &
                 "                     ---- Version 0.3 May-2011----"                       , &
                 "    *******************************************************************"  , &
                 "    * Log file of ESMERALDA program of the ILL/ANSTO Laue Suite       *"  , &
                 "    * Information about actions and results of running ESMERALDA      *"  , &
                 "    *******************************************************************"  , &
                 "                          (LFM-JRC- May -2011 )"
          End If
        End if
        
        
        !
        !--- Look for initial orientation, either orient or UB matrix
        !
        write(*,*) fich_cfl%nlines
        do n=1,fich_cfl%nlines
          line=adjustl(l_case(fich_cfl%line(n)))
          !write(unit=*,fmt="(a)") trim(line)
          if(line(1:6) == "orient") then
            read(unit=line(7:),fmt=*,iostat=ier) Curr_U_angles
            if(ier /= 0) Curr_U_angles=0.0
            Saved_U_Angles=Curr_U_angles
            write(unit=*,fmt="(a,3f9.4)") " => Current orientation read: ",Curr_U_angles
            call Update_Orientation(U_angles=Curr_U_angles,spindle=0.0)
            Init_Orient_given=.true.
            Orient_known=.true.
            exit
          end if

          if(line(1:9) == "ub_matrix") then
            read(unit=fich_cfl%line(n+1),fmt=*,iostat=ier) Current_UB(1,:)
            read(unit=fich_cfl%line(n+2),fmt=*,iostat=ier) Current_UB(2,:)
            read(unit=fich_cfl%line(n+3),fmt=*,iostat=ier) Current_UB(3,:)
            if(ier /= 0) then
              Curr_U_angles=0.0
              write(unit=*,fmt="(a)") " => Error Reading the UB-matrix! "
            else
              write(unit=*,fmt="(a/3(tr10,3f12.5/))") " => Read UB-matrix: ",transpose(Current_UB)
              call Update_Orientation(UB_Matrix=Current_UB,spindle=0.0,chi_phi=(/0.0,0.0/))
              Saved_UB=Current_UB
              Init_UB_given=.true.
              Saved_U_Angles=Curr_U_angles
              Orient_known=.true.
            end if
            exit
          end if
        end do
        !
        !--- Look for Mosaic spread and divergence commands
        do n=1,fich_cfl%nlines
          line=adjustl(l_case(fich_cfl%line(n)))

          if(line(1:10) == "mosaic_min") then
            read(unit=line(11:),fmt=*,iostat=ier) mosaic_spread
            if(ier /= 0) then
              mosaic_spread=0.3*to_rad
              write(unit=*,fmt="(a,f8.4,a)") " => Error reading Mosaic spread, set to default: ",mosaic_spread," radians"
            else
              write(unit=*,fmt="(a,f8.4,a)") " => Mosaic spread read: ",mosaic_spread," minutes"
              mosaic_spread=mosaic_spread*to_rad/60.0
              write(unit=*,fmt="(a,f8.4,a)") " => Mosaic spread     : ",mosaic_spread," radians"
            end if
          end if

          if(line(1:6) == "divergence_min") then
            read(unit=line(15:),fmt=*,iostat=ier) max_divergence
            if(ier /= 0) then
              max_divergence=0.2*to_rad
              write(unit=*,fmt="(a,f8.4,a)") " => Error reading maximum divergence, set to default: ",max_divergence," radians"
            else
              write(unit=*,fmt="(a,f8.4,a)") " => Maximum divergence read: ",max_divergence," minutes"
              max_divergence=max_divergence*to_rad/60.0
              write(unit=*,fmt="(a,f8.4,a)") " => Maximum divergence     : ",max_divergence," radians"
            end if
          end if

        end do
        !
        !--- Look for Twin commands
        call Read_Twinlaw(Tw,ok,fich_cfl=fich_cfl)
        if(ok .and. Tw%N_twins > 1) then
          Twin_given=.true.
          call Write_Twinlaw(Tw,i_sf,Cell)
        else
          Tw%N_twins =1
        end if

        !--- Look for Propagation vector commands, set up Gk and {k} and  write the information
        i=0
        do n=1,fich_cfl%nlines
          line=adjustl(l_case(fich_cfl%line(n)))
          if(line(1:4) == "kvec") then
            i=i+1
            read(unit=line(5:),fmt=*,iostat=ier) kvec(:,i)
            if(ier /= 0) then
             i=i-1
             cycle
            end if
            write(unit=*,fmt="(a,3f9.4)") " => k-vector: ",kvec(:,i)
            j=index(line,"harmonic")
            if(j /= 0) then
              read(unit=line(j+8:), fmt=*,iostat=ier) nharm(i)
              if(ier /= 0) nharm(i)=1
            end if
            j=index(line,"multik")
            if(j /= 0) multik(i)=.true.
            call K_Star(Kvec(:,i),SpG,G_k(i))
            call Write_Group_k(G_k(i),i_sf)

            if(G_k(i)%k_equiv_minusk) then
               if(.not. multik(i))  then
                 G_k(i)%nk=1 !Consider only 1 arm of the star
                 write(unit=i_sf,fmt="(/,a,/)") " => Only 1 arm of the star will be considered for satellite generation"
               else
                 write(unit=i_sf,fmt="(/,a,/)") " => All arms of the star will be considered for satellite generation"
               end if
            else
               if(.not. multik(i)) then
                 call Set_SpaceGroup(SpG%SPG_lat//" -1",Sp_Gr)
                 call K_Star(Kvec(:,i),Sp_Gr,G_k(i))     !Consider only the pair {k,-k}
                 write(unit=i_sf,fmt="(/,a,/)") " => Only the pair {k,-k} will be considered for satellite generation"
               else
                 !Test if -k is within the star
                 if (.not. G_k(i)%k_equiv_minusk .and. G_k(i)%G0%Centred == 1) then
                    do j=1,G_k(i)%nk
                     G_k(i)%stark(:,j+G_k(i)%nk)= -G_k(i)%stark(:,j)
                    end do
                    G_k(i)%nk=G_k(i)%nk*2
                    write(unit=i_sf,fmt="(/,a,/)") " => The star has been augmented with {-k} for satellite generation"
                 else
                    write(unit=i_sf,fmt="(/,a,/)") " => All arms of the star will be considered for satellite generation"
                 end if
               end if
            end if
            if(line(1:5) == "dkmin") then
              read(unit=line(6:),fmt=*,iostat=ier) dklim
              if(ier /= 0) dklim=1.5
              write(unit=i_sf,fmt="(a,f8.4/)") " => The minimum d-spacing for satellites is: ",dklim
            end if
          end if
        end do
        nkvec=i
        if(nkvec /= 0) kvec_given=.true.
        !
        !--- Look for Excluded Regions ---!
        !
        i=0
        ExclR%Nexc_Rect=0
        ExclR%Nexc_Circ=0
        do n=1,fich_cfl%nlines
          i=i+1
          if(i > fich_cfl%nlines) exit
          line=adjustl(l_case(fich_cfl%line(i)))

          if(line(1:9) == "excl_rect") then
            j=index(fich_cfl%line(i)," ")
            read(unit=fich_cfl%line(i)(j:),fmt=*,iostat=ier)   ExclR%Nexc_Rect
            if(ier /= 0) then
              write(unit=*,fmt="(a)") " => Warning! Error reading number of rectangular excluded regions in "//trim(CFL_File)
            end if
            if(allocated(ExclR%Exc_Rect)) deallocate(ExclR%Exc_Rect)
            allocate(ExclR%Exc_Rect(2,2,ExclR%Nexc_Rect))

            if(allocated(ExclR%eliptic)) deallocate(ExclR%eliptic)
            allocate(ExclR%eliptic(ExclR%Nexc_Rect))
            ExclR%eliptic = .false.

            do k=1, ExclR%Nexc_Rect
              i=i+1
              read(unit=fich_cfl%line(i),fmt=*,iostat=ier) ExclR%Exc_Rect(:,1,k), ExclR%Exc_Rect(:,2,k)
              if(ier /= 0) then
                write(unit=*,fmt="(a,i3)") " => Error reading rectangular excluded region: ",k
              end if
            end do
          end if

          if(line(1:9) == "excl_circ") then
            j=index(fich_cfl%line(i)," ")
            read(unit=fich_cfl%line(i)(j:),fmt=*,iostat=ier)   ExclR%Nexc_Circ
            if(ier /= 0) then
              write(unit=*,fmt="(a)") " => Warning! Error reading number of circular excluded regions in "//trim(CFL_File)
            end if
            if(allocated(ExclR%Exc_Circ)) deallocate(ExclR%Exc_Circ)
            allocate(ExclR%Exc_Circ(3,ExclR%Nexc_Circ))
            do k=1, ExclR%Nexc_Circ
              i=i+1
              read(unit=fich_cfl%line(i),fmt=*,iostat=ier) ExclR%Exc_Circ(:,k)
              if(ier /= 0) then
                write(unit=*,fmt="(a,i3)") " => Error reading circular excluded region: ",k
              end if
            end do
          end if
        End do
        !
        write(unit=*,fmt="(a,i5)") " => Number of rectangular excluded regions ", ExclR%Nexc_Rect
        write(unit=*,fmt="(a)")    "    i-min j-min    i-max j-max  "
        do k=1, ExclR%Nexc_Rect
          write(unit=*,fmt="(2(2i5,tr2))") ExclR%Exc_Rect(:,1,k), ExclR%Exc_Rect(:,2,k)
        end do
        write(unit=*,fmt="(a,i5)") " => Number of circular excluded regions ", ExclR%Nexc_Circ
        write(unit=*,fmt="(a)")    "    i-cent j-cent    radius  "
        do k=1, ExclR%Nexc_Circ
          write(unit=*,fmt="(2i5,tr2,i4)") ExclR%Exc_Circ(:,k)
        end do
        !
        if(present(read_inst)) then
          ! Search for instrument file name
          do i=1,fich_cfl%nlines
             line=adjustl(l_case(fich_cfl%line(i)))
             if(line(1:5) == "instr") then
                  j=index(fich_cfl%line(i)," ")
                  Instrm_File=adjustl(fich_cfl%line(i)(j:))
                  write(unit=*,fmt="(a)") " => Instrument file: "//trim(Instrm_File)
                  inquire(file=trim(Instrm_File), exist=esta)
                  if(.not. esta)  then
                     write(unit=*,fmt="(a)") " => Instrument file NOT found!"
                     Instrm_Loaded=.false.
                  else
                     if (present(resize)) then
                         call Read_instrument(resize)
                     else
                         call Read_instrument()
                     end if
                     if (ExclR%Nexc_Rect == 0 .and. ExclR%Nexc_Circ == 0) then
                       call Read_Excluded_Regions(trim(Instrm_File),ExclR,ok)
                       if(.not. ok) write(unit=*,fmt="(a)") " => No excluded regions found!"
                     end if
                     if(Instrm_Loaded) hkl_gen=.true.
                  end if
                  exit
             end if
          end do
        end if
        !
        !--- Look for Image files ---!
        !
        do n=1,fich_cfl%nlines
          line=adjustl(l_case(fich_cfl%line(n)))
          if(line(1:11) == "image_files") then
            write(unit=*,fmt="(a)") " => Reading image list"
            read(unit=line(12:),fmt=*,iostat=ier) nimg
              dummy_image=.false.
              Image_File = '.'  !means current directory
              i=0
              do
                i=i+1
                if(i > nimg) exit
                line=adjustl(fich_cfl%line(n+i))
                if(len_trim(line) == 0) cycle  !Allows non-filled lines
                j=index(line," ")  !in order to eliminate other instruction accompanying the file names
                line=line(1:j-1)
                Image_File(len(trim(Image_File))+1:) = char(0) // trim(line)
              end do
              exit !All image file names have been read
          end if
        end do

        if(Instrm_Loaded .and. .not. hkl_gen) call Reflection_generation()
        !if(Instrm_Loaded ) call Reflection_generation()
        ! look for information concerning how to tread the incident spectrum

     return
    End Subroutine Read_CFL_File

    Subroutine Read_Binary_Image(filenam,npz,npx,datam,ok)
       character(len=*),        intent(in)  :: filenam
       integer,                 intent(in)  :: npz,npx
       integer, dimension(:,:), intent(out) :: datam
       logical,                 intent(out) :: ok
       !---- Local Variables ----!
       integer :: ins, ier, nx,nz
       integer(kind=2), dimension(npz,npx) :: short_data
       logical         :: exists, opnd

       ok=.false.
       ! Check if the file exist and it is already opened
       inquire(file=filenam, exist=exists, opened=opnd, number=ins)
       if(.not. exists) return
       if(opnd) then
          rewind(unit=ins)
       else
          Call Get_Logunit(ins)
          open(unit=ins, file=trim(filenam), access="stream",status="old", action="read", form="unformatted",position="rewind")
       end if
       read(unit=ins, iostat=ier) nz,nx
       !write(unit=*,fmt="(2(a,i6))") "Nrow-input:",npz," Ncol-input:",npx
       !write(unit=*,fmt="(2(a,i6))") "Nrow:",nz," Ncol:",nx
       if(nz /= npz  .or. nx /= npx) return
       read(unit=ins, iostat=ier) short_data
       if(ier == 0) ok=.true.
       !write(unit=*,fmt="(a,i6)") " ier=",ier
       if(.not. opnd) close(unit=ins)

       where(short_data < 0)
         datam = short_data + 65536
       else where
         datam = short_data
       end where

       return
    End Subroutine Read_Binary_Image

    Subroutine Read_Reo_Image(filenam,npz,npx,datam,ok)
       character(len=*),        intent(in)  :: filenam
       integer,                 intent(in)  :: npz,npx
       integer, dimension(:,:), intent(out) :: datam
       logical,                 intent(out) :: ok
       !---- Local Variables ----!
       character(len=135) :: Mess
       integer :: Offsti
       ok=.false.
       Offsti=0
       Call Read_Binary_File_16bits(filenam,Offsti,npz,npx,Datam,OK,Mess,"Col")
       if(.not. ok) then
          write(unit=*,fmt="(a)") " => "//trim(mess)
       end if
       return
    End Subroutine Read_Reo_Image

    Subroutine Write_Binary_Image(filenam,npz,npx,datam)
       character(len=*),        intent(in)  :: filenam
       integer,                 intent(in)  :: npz,npx
       integer, dimension(:,:), intent(in)  :: datam
       !---- Local Variables ----!
       integer :: ins, i, j
       integer(kind=2), dimension(npz,npx) :: short_data   !The image is written in 16 bits

       do j=1,npx
         do i=1,npz
           if( datam(i,j) > 32767) then
             short_data(i,j) = datam(i,j) - 65536
           else
             short_data(i,j) = datam(i,j)
           end if
         end do
       end do
       Call Get_Logunit(ins)
       open(unit=ins, file=trim(filenam), access="stream",status="replace", action="write", form="unformatted")
        write(unit=ins) npz,npx
        write(unit=ins) short_data
        flush(unit=ins)
       close(unit=ins)
       return
    End Subroutine Write_Binary_Image

    Subroutine Read_instrument(resize)
      Logical, optional, intent (out) :: resize
      integer :: nx,nz
      real    :: wspread
          if(present(resize)) then
           resize = .true.
           if(Instrm_Loaded) then !There was an already loaded instrument
             nx=LaueDiff%np_h
             nz=LaueDiff%np_v
           else
             nx=0; nz=0
           end if
          end if
          call Read_Laue_Instrm(Instrm_File, LaueDiff)
          if( .not. Error_Laue_Mod) then
            Instrm_Loaded=.True.
            if(present(resize)) then
              if(nx == LaueDiff%np_h .and. nz == LaueDiff%np_v) resize=.false.
            end if
            wspread=(LaueDiff%L_max-LaueDiff%L_min)/LaueDiff%L_Central
            if( wspread < 0.02) Monochromatic=.true.
            call Reflection_generation()
            if(monochromatic) then
               write(unit=*,fmt="(a,f7.2,a)") " =>  The wavelength spread is less than 2%: ",100.0*wspread,"%"
               write(unit=*,fmt="(a)")      "     The beam is (quasi) monochromatic "
            else
               write(unit=*,fmt="(a,f7.2,a)") " =>  The wavelength spread is: ",100.0*wspread,"%"
            end if
          else
            Instrm_Loaded=.false.
            write(unit=*,fmt="(a)") "     Mess: "//trim(Error_Mess_Laue_Mod)
          end if
    return
    End Subroutine Read_instrument

    Subroutine Reflection_generation()
        integer :: m,i,j
        if( Instrm_Loaded .and. Cell_Loaded )then

          if( struc_fact_new ) then
            ord=(/3,2,1/)
            stlmax=1.0/(2.0*LaueDiff%d_min)
            MaxNumRef = get_maxnumref(stlmax,Cell%CellVol,mult=SpG%Multip)
            MaxNumRef=MaxNumRef*Spg%NumOps*max(Spg%Centred,1)
            write(unit=*,fmt="(a,i6,a,f8.4)") " => MaxNumRef:",MaxNumRef," stlmax: ",stlmax

            call Hkl_gen_sxtal(cell,SpG,0.0,stlmax,MaxNumRef,hkl,ord)
            write(unit=*,fmt="(a,i6,a)") " => Generated ",hkl%nref," reflections"

            !Calculation of Structure factors for neutron scattering
            if(A%natoms /= 0) then
              atoms_given=.true.
              call Init_Structure_Factors(hkl,A,Spg,mode="NUC",lun=i_sf)
              call Structure_Factors(A,SpG,hkl,mode="NUC")
              call Write_Structure_Factors(i_sf,hkl,mode="NUC")
              write(unit=*,fmt="(a,f8.4,a)") ' => Structure factors calculated and written up to a resolution of: ',&
                                              LaueDiff%d_min,' Angstroms'
            end if
            struc_fact_new=.false.
          end if

          !Calculate the expected maximum number of Laue Reflections that can be simultaneously
          !stimulated according to the limits stored in "Limits"
           !if(LaueDiff%L_max < 2.0 * LaueDiff%d_min) then  !ITC vol C, p. 28
           ! n= 0.25*pi*(LaueDiff%L_max-LaueDiff%L_min)*Cell%CellVol/real(Spg%NumLat)/LaueDiff%d_min**4
           !else
            n = 4.0/3.0*pi*((1.0/LaueDiff%L_min)**3-(1.0/LaueDiff%L_max)**3)*Cell%CellVol !/real(Spg%NumLat)
            n = min(hkl%nref,n)
           !end if
          write(unit=*,fmt="(a,3f10.4)") " => Lambda min, max, d_min: ", LaueDiff%L_min, LaueDiff%L_max , LaueDiff%d_min
          write(unit=*,fmt="(a,i7)") " => Estimated maximum number of stimulated reflections: ", n

          if(kvec_given) then      !Consider propagation vectors to allocate Laue reflections
            m=0
            do i=1,nkvec
             m=m+G_k(i)%nk
            end do
            n=n*(1+m)        !The number of possible reflections has been augmented
            write(unit=*,fmt="(a,i7)") " => Maximum number of stimulated reflections, including k-vectors: ", n
          end if

          !Allocation of memory and initialization of arrays
          if(Tw%N_twins > 1) then
            n=n*Tw%N_twins
            write(unit=*,fmt="(a,i7)") " => Total maximum number of stimulated reflections, due to twinning: ", n
          end if
          call Allocate_Laue_Ref_list(Ref_Lst,n)

          BL=Cell%BL_M
          BL_Inv=Cell%BL_Minv
 !**********************************************************************************************************
          write(unit=*,fmt="(a)")  "        Busing-Levy                       Orth_Cr_cel"
          write(unit=*,fmt="(3f10.5,4x,3f10.5)")  BL(1,:),Cell%Orth_Cr_cel(1,:)
          write(unit=*,fmt="(3f10.5,4x,3f10.5)")  BL(2,:),Cell%Orth_Cr_cel(2,:)
          write(unit=*,fmt="(3f10.5,4x,3f10.5)")  BL(3,:),Cell%Orth_Cr_cel(3,:)
 !**********************************************************************************************************
          !call Update_Orientation(U_angles=Curr_U_angles,spindle=0.0)
          !call Update_Orientation(phi_angles=(/phi_x,phi_y,phi_z/),spindle=img_spdl(img_n))
          num_im=0
          write(unit=*,fmt="(a)") ' => HKLs have been generated'
        else
          write(unit=*,fmt="(a)") ' => HKLs have NOT been generated'
        end if
    return
    End Subroutine Reflection_generation

    Subroutine Write_Reflection_File(visible,strf, write_image,filenam,smpl,spindle,Calc_Image)
     Character(len=*), optional, intent(in) :: visible
     Character(len=*), optional, intent(in) :: strf
     logical,          optional, intent(in) :: write_image
     Character(len=*), optional, intent(in) :: filenam
     Type(Sample_type),optional, intent(in) :: smpl
     real,             optional, intent(in) :: spindle
     integer, optional,dimension(:,:), allocatable, intent(out) :: Calc_Image
     !
     integer :: lun, i, ix, iz,ix1,ix2,iz1,iz2, ins
     integer, dimension(:),allocatable :: indx
     real, dimension(:,:), allocatable :: datam
     Type(Sample_type)                 :: smp
     real                              :: prev
     real, dimension(3,3)              :: Rot
     !
     if(.not. present(visible) .and. .not. present(strf) ) then
        Call Recalc(0.0, 0.0, 0.0, MRUB,"All_Reflections")
     end if
     if(present(spindle)) then
       Rot=Matrix_Rz(-spindle,"D")
       MRUB=matmul(Rot,Current_UB)
       Call Recalc(0.0, 0.0, 0.0, MRUB)
     end if
     call Get_Logunit(lun)
     if(present(filenam)) then
        open(unit=lun, file=trim(filenam),status="replace",action="write")
     else
        open(unit=lun, file="Reflections_File.hkl",status="replace",action="write")
     end if
     write(unit=lun,fmt="(a)") "       -------------------------------------------------------- "
     write(unit=lun,fmt="(a)") "       --------- Reflection List from Laue Simulator   -------- "
     write(unit=lun,fmt="(a)") "       -------------------------------------------------------- "

     if(.not. present(visible)) then
        write(unit=lun,fmt="(/a)") "  => The full list of excited reflections is output"
     else
        write(unit=lun,fmt="(/a)") "  => The list of visible reflections (Status=0) is output"
     end if
     !Sort by increasing 2Theta if no structure factors are provided
     !Sort by increasing Lambda if structure factors are provided
     !
     if(allocated(Indx)) deallocate(Indx)
     allocate(Indx(Ref_Lst%nref))
     do i=1,Ref_Lst%nref
       Indx(i)=i
     end do
     if(present(visible)) then
       if(present(strf)) then
         call sort(Ref_Lst%LR(:)%Lambda,Ref_Lst%nref,Indx)
         if(present(smpl)) then
           smp=smpl
         else
           smp= Sample
         end if
       else
         call sort(Ref_Lst%LR(:)%ttheta,Ref_Lst%nref,Indx)
       end if
     end if
     if(present(strf)) then
       call Get_Logunit(ins)
       open(unit=ins,file='incident_spectrum.dat',status="replace",action="write")
       write(unit=ins,fmt="(a)") "Incident_Spectrum     ####    <--- Put here the number of (Lambda, Flux(Lambda)) points below"
       prev=0.0
       !
       write(unit=lun,fmt="(//a)") &
       "   h   k   l   2theta   d-spacing   Lambda   Flux(L)     Gamma   "// &
       "   Nu         X         Z    Mult     Int-Obs        Int-Calc " // &
       "  Lorentz  Efficiency  Absorption   Domain"
       do j=1, Ref_Lst%nref
           i=Indx(j)
           Call Laue_Mult(LaueDiff,SpG,Ref_Lst%LR(i))
           write(unit=lun,fmt="(3i4,4f10.4,4f10.2,i6,2f14.3,3f10.4,i6)") &
                            nint(Ref_Lst%LR(i)%h),Ref_Lst%LR(i)%Ttheta,Ref_Lst%LR(i)%ds, Ref_Lst%LR(i)%lambda, &
                            Icorr%spectrum(i),Ref_Lst%LR(i)%Gamma, Ref_Lst%LR(i)%Nu, Ref_Lst%LR(i)%x, Ref_Lst%LR(i)%z,        &
                            Ref_Lst%LR(i)%Mult, Ref_Lst%LR(i)%Obs_int, Ref_Lst%LR(i)%Cal_int,Icorr%Lorentz(i),Icorr%Eff(i),   &
                            Icorr%Absorpt(i),Ref_Lst%LR(i)%domain
           if( Ref_Lst%LR(i)%lambda - prev > 0.001) then
             write(unit=ins,fmt="(2f14.5)")  Ref_Lst%LR(i)%lambda, Icorr%spectrum(i)
             prev = Ref_Lst%LR(i)%lambda
           end if
       end do
       flush(unit=ins)
       close(unit=ins)
       !Calculate Laue Image and write it in a file
       if(present(write_image) .and. present(spindle)) then   !LaueDiff is a global variable
         call Calc_Laue_Image(LaueDiff,Ref_Lst,Spindle,smpl,datam)
         if( present(Calc_Image)) then
           if(allocated(Calc_Image)) deallocate(Calc_Image)
           allocate(Calc_Image(LaueDiff%Np_v,LaueDiff%Np_h))
           Calc_Image=Datam
           call Write_Binary_Image("laue_image.img",LaueDiff%Np_v,LaueDiff%Np_h,Calc_Image)
         end if
       end if

     else
       write(unit=lun,fmt="(//a)") &
       "   h   k   l   Intensity    2theta  d-spacing    Gamma      Nu      Lambda  "// &
       "    X         Z     Status    Mult   Nodal Domain"
       do j=1, Ref_Lst%nref
           i=Indx(j)
           Call Laue_Mult(LaueDiff,SpG,Ref_Lst%LR(i))
           write(unit=lun,fmt="(3i4,f12.1,7f10.4,3i8,i6)") &
                            nint(Ref_Lst%LR(i)%h),Ref_Lst%LR(i)%Obs_int,Ref_Lst%LR(i)%Ttheta,Ref_Lst%LR(i)%ds,Ref_Lst%LR(i)%Gamma, &
                            Ref_Lst%LR(i)%Nu, Ref_Lst%LR(i)%lambda, Ref_Lst%LR(i)%x, Ref_Lst%LR(i)%z,        &
                            Ref_Lst%LR(i)%stats, Ref_Lst%LR(i)%Mult,Ref_Lst%LR(i)%Nodal,&
                            Ref_Lst%LR(i)%domain
       end do
     end if
     flush(unit=lun)
     close(unit=lun)
     return
    End Subroutine write_reflection_file

    Subroutine Write_Integrated_Reflections(filenam,Ref)
     Character(len=*),         intent(in)    :: filenam
     Type(Laue_Ref_List_Type), intent(in out):: Ref

     integer :: lun, i, j, ig, n
     integer, dimension(:),   allocatable :: indx
     real    :: xpc,zpc, intensity

     call Get_Logunit(lun)
     open(unit=lun, file=trim(filenam),status="replace",action="write")
     write(unit=lun,fmt="(a)") "       -------------------------------------------------------- "
     write(unit=lun,fmt="(a)") "       ------- Integrated Reflection List from Esmerada ------- "
     write(unit=lun,fmt="(a)") "       -------------------------------------------------------- "
     write(unit=*,fmt="(a)") "       -------------------------------------------------------- "
     write(unit=*,fmt="(a)") "       ------- Integrated Reflection List from Esmerada ------- "
     write(unit=*,fmt="(a)") "       -------------------------------------------------------- "

     write(unit=lun,fmt="(/a)") "  => The list of visible reflections (Status=0) is output with integrated intensity"
     write(unit=*,fmt="(a)")    "  => The list of the first 200 visible reflections is output in the screen"

     if(allocated(Indx)) deallocate(Indx)
     allocate(Indx(Ref%nref))
     Indx=1
     call sort(Ref%LR(:)%Ttheta,Ref%nref,Indx)
     write(unit=lun,fmt="(//a)") &
     "   h   k   l  Integ-Intens     Sigma   2theta   d-spacing    Gamma      Nu      Lambda    "// &
     " X(mm)     Z(mm)    X(pix)    Z(Pix)   Mult  Nodal"
     write(unit=*,fmt="(//a)") &
     "   h   k   l  Integ-Intens     Sigma   2theta   d-spacing    Gamma      Nu      Lambda    "// &
     " X(mm)     Z(mm)    X(pix)    Z(Pix)   Mult  Nodal"
     n=0
     j=0
     do
        j=j+1
        if(j > Ref%Nref) exit
        i=Indx(j)
        Call Laue_Mult(LaueDiff,SpG,Ref%LR(i))
        call Mil_To_Pix(LaueDiff,Ref%LR(i)%x, Ref%LR(i)%z,xpc,zpc)

        ig = Ref%LR(i)%Mult
        if( ig > 1 ) then
           j=j-1
           do n=1,ig-1
              j=j+1
              i=Indx(j)
              Call Laue_Mult(LaueDiff,SpG,Ref%LR(i))
              write(unit=lun,fmt="(3i4,2f12.1,5f10.4,4f10.2,2i6,tr3)")            &
              nint(Ref%LR(i)%h),-1.0,0.1,Ref%LR(i)%Ttheta,Ref%LR(i)%ds,Ref%LR(i)%Gamma,    &
              Ref%LR(i)%Nu, Ref%LR(i)%lambda, Ref%LR(i)%x, Ref%LR(i)%z,xpc,zpc,   &
              Ref%LR(i)%Mult,Ref%LR(i)%Nodal
              if(j <= 200)  write(unit=*,fmt="(3i4,2f12.1,5f10.4,4f10.2,2i6,tr3)")&
              nint(Ref%LR(i)%h),-1.0,0.1,Ref%LR(i)%Ttheta,Ref%LR(i)%ds,Ref%LR(i)%Gamma,    &
              Ref%LR(i)%Nu, Ref%LR(i)%lambda, Ref%LR(i)%x, Ref%LR(i)%z,xpc,zpc,   &
              Ref%LR(i)%Mult,Ref%LR(i)%Nodal
           end do
           j=j+1
           i=Indx(j)
           intensity=Ref%LR(i)%Obs_int
           Call Laue_Mult(LaueDiff,SpG,Ref%LR(i))
           write(unit=lun,fmt="(3i4,2f12.1,5f10.4,4f10.2,2i6,tr3)")                  &
           nint(Ref%LR(i)%h),intensity,Ref%LR(i)%Sigma,Ref%LR(i)%Ttheta,     &
           Ref%LR(i)%ds,Ref%LR(i)%Gamma,Ref%LR(i)%Nu, Ref%LR(i)%lambda, Ref%LR(i)%x, &
           Ref%LR(i)%z,xpc,zpc, Ref%LR(i)%Mult,Ref%LR(i)%Nodal
           if(j <= 200)  write(unit=*,fmt="(3i4,2f12.1,5f10.4,4f10.2,2i6,tr3)")&
           nint(Ref%LR(i)%h),intensity,Ref%LR(i)%Sigma,Ref%LR(i)%Ttheta,     &
           Ref%LR(i)%ds,Ref%LR(i)%Gamma,Ref%LR(i)%Nu, Ref%LR(i)%lambda, Ref%LR(i)%x, &
           Ref%LR(i)%z,xpc,zpc, Ref%LR(i)%Mult,Ref%LR(i)%Nodal

        else
           intensity=Ref%LR(i)%Obs_int
           write(unit=lun,fmt="(3i4,2f12.1,5f10.4,4f10.2,2i6,tr3)")                  &
           nint(Ref%LR(i)%h),intensity,Ref%LR(i)%Sigma,Ref%LR(i)%Ttheta,     &
           Ref%LR(i)%ds,Ref%LR(i)%Gamma,Ref%LR(i)%Nu, Ref%LR(i)%lambda, Ref%LR(i)%x, &
           Ref%LR(i)%z,xpc,zpc, Ref%LR(i)%Mult,Ref%LR(i)%Nodal
           if(j <= 200)  write(unit=*,fmt="(3i4,2f12.1,5f10.4,4f10.2,2i6,tr3)")&
           nint(Ref%LR(i)%h),intensity,Ref%LR(i)%Sigma,Ref%LR(i)%Ttheta,     &
           Ref%LR(i)%ds,Ref%LR(i)%Gamma,Ref%LR(i)%Nu, Ref%LR(i)%lambda, Ref%LR(i)%x, &
           Ref%LR(i)%z,xpc,zpc, Ref%LR(i)%Mult,Ref%LR(i)%Nodal
        end if
     end do
     flush(unit=lun)
     close(unit=lun)
     return
    End Subroutine Write_Integrated_Reflections


    Subroutine Recalc(phi_x, phi_y, phi_z, M, allr, LDiff)
       real,                                 intent(in) :: phi_x, phi_y, phi_z ! Variables that describe orientation (angles in order)
       real, dimension(3,3),       optional, intent(in) :: M                   ! Matrix transforming a reflection hkl to the Cartesian in L-system
       character(len=*),           optional, intent(in) :: allr
       Type(Laue_Instrument_Type), optional, intent(in) :: LDiff
       real :: f2, lorentz, polar, absorpt, extinc, eff, vol, Isp
       real, dimension(2)   :: pos, shift
       real, dimension(3,3) :: UM, Rotd
       integer :: m1,L,k
       Type(Laue_Instrument_Type) :: L_Diff

       if(present(LDiff)) then
         L_Diff=LDiff
       else
         L_Diff=LaueDiff
       end if
       n=0
       if(present(M)) then
         MRUB=M   !This corresponds to the matrix M=R.UB (or M= Omega Chi Phi UB)
       else
         call set_rotation_matrix((/phi_x, phi_y, phi_z/),UM) ! UM here is the U matrix
         MRUB=matmul(UM,BL)
       end if
   !    write(unit=*,fmt="(a/3(10x,3(f9.4),/))") " => Recip-to-Lab matrix: ", (MRUB(i,:),i=1,3)
       call Init_Laue_Ref(Ref)
       if(present(allr)) then   !Calculate all reflections (Ref%stats >= 0) that may be detected with an appropriate detector
          do i=1,hkl%nref
            Ref%h=hkl%Ref(i)%h
            call Calc_Laue_Spot_XZ_Angles(L_Diff,Ref,MRUB)
            if(Ref%stats >= 0)then
              n=n+1
              Ref_Lst%LR(n)=Ref
              Ref_Lst%LR(n)%Cal_Int = hkl%ref(i)%Fc * hkl%ref(i)%Fc
            end if
          end do
          if(kvec_given .and. .not. kvec_supress) then
            do j=1,nkvec
              do k=1,G_k(j)%nk
                do m1=1,nharm(j)
                  Ref%h=hkl%Ref(i)%h+real(m1)*G_k(j)%stark(:,k)
                  Ref%kindex=(/j,k,m1/)
                  call Calc_Laue_Spot_XZ_Angles(L_Diff,Ref,MRUB,dk_min=dklim)
                  if(Ref%stats >= 0)then
                    n=n+1
                    Ref_Lst%LR(n)=Ref
                  end if
                end do
              end do
            end do
          end if
       else              !Calculate only visible reflections (Ref%stats = 0) in the current detector
          do i=1,hkl%nref
            Ref%h=hkl%Ref(i)%h
            Ref%kindex=0
            call Calc_Laue_Spot_XZ_Angles(L_Diff,Ref,MRUB)
            if(Ref%stats == 0)then
              if(Cheb_Ref .or. Apply_Offsets) then  !Correct for distortions
                pos=(/Ref%x , Ref%z/)
                !Only the dimensions of the detector are used in Corr_xz_Chebychev
                call Corr_xz_Chebychev(pos,LDiff,Ncoeff,Cheb,shift)
                Ref%x=pos(1)+shift(1)
                Ref%z=pos(2)+shift(2)
              end if
              n=n+1
              Ref_Lst%LR(n)=Ref
              Ref_Lst%LR(n)%Domain=1
              Ref_Lst%LR(n)%Cal_Int = hkl%ref(i)%Fc * hkl%ref(i)%Fc
            end if
            if(kvec_given .and. .not.kvec_supress ) then
              do j=1,nkvec
                do k=1,G_k(j)%nk
                  do m1=1,nharm(j)
                    Ref%h=hkl%Ref(i)%h+real(m1)*G_k(j)%stark(:,k)
                    Ref%kindex=(/j,k,m1/)
                    call Calc_Laue_Spot_XZ_Angles(L_Diff,Ref,MRUB,dk_min=dklim)
                    if(Ref%stats == 0)then
                      if(Cheb_Ref .or. Apply_Offsets) then  !Correct for distortions
                        pos=(/Ref%x , Ref%z/)
                        !Only the dimensions of the detector are used in Corr_xz_Chebychev
                        call Corr_xz_Chebychev(pos,LDiff,Ncoeff,Cheb,shift)
                        Ref%x=pos(1)+shift(1)
                        Ref%z=pos(2)+shift(2)
                      end if
                      n=n+1
                      Ref_Lst%LR(n)=Ref
                      Ref_Lst%LR(n)%Domain=1
                    end if
                  end do
                end do
              end do
            end if
          end do

          if(Twin_given) then  !Only visible reflections
            do j=2,Tw%N_twins
              Rotd=matmul(UM,matmul(Tw%Twin_Mat(:,:,j),BL))
              do i=1,hkl%nref
                Ref%h=hkl%Ref(i)%h
                Ref%kindex=0
                call Calc_Laue_Spot_XZ_Angles(L_Diff,Ref,Rotd)
                if(Ref%stats == 0)then
                  n=n+1
                  Ref_Lst%LR(n)=Ref
                  Ref_Lst%LR(n)%Domain=j
                  Ref_Lst%LR(n)%Cal_Int = hkl%ref(i)%Fc * hkl%ref(i)%Fc
                end if
                if(kvec_given .and. .not. kvec_supress ) then
                  do L=1,nkvec
                    do k=1,G_k(L)%nk
                      do m1=1,nharm(L)
                        Ref%h=hkl%Ref(i)%h+real(m1)*G_k(L)%stark(:,k)
                        Ref%kindex=(/L,k,m1/)
                        call Calc_Laue_Spot_XZ_Angles(L_Diff,Ref,Rotd,dk_min=dklim)
                        if(Ref%stats == 0)then
                          if(Cheb_Ref .or. Apply_Offsets) then  !Correct for distortions
                            pos=(/Ref%x , Ref%z/)
                            !Only the dimensions of the detector are used in Corr_xz_Chebychev
                            call Corr_xz_Chebychev(pos,LDiff,Ncoeff,Cheb,shift)
                            Ref%x=pos(1)+shift(1)
                            Ref%z=pos(2)+shift(2)
                          end if
                          n=n+1
                          Ref_Lst%LR(n)=Ref
                          Ref_Lst%LR(n)%Domain=j
                        end if
                      end do
                    end do
                  end do
                end if

              end do
            end do
          end if
       end if
       Ref_Lst%nref=n
       ! Set up calculated intensities
       call Allocate_Corrections(n,Icorr,ok)
       if(ok) then
         do i=1,Icorr%nref
            lorentz=Lorentz_Factor(Ref_Lst%LR(i)%ttheta)
            polar=Polarization(ttheta=0.0,phi=0.0,rho=0.0)  ! polar=1 for neutron diffraction
            call Incident_Spectrum(Ref_Lst%LR(i)%lambda,300.0,Isp)
            eff=Efficiency(LaueDiff,Ref_Lst%LR(i))
            Ref_Lst%LR(i)%Cal_Int= lorentz*polar*Isp*eff*Ref_Lst%LR(i)%Cal_Int
            Icorr%lambda(i)  = Ref_Lst%LR(i)%lambda
            Icorr%lorentz(i) = lorentz
            Icorr%polar(i)   = polar
            Icorr%spectrum(i)= Isp
            Icorr%eff(i)     = eff
         end do
       end if

     return
    End Subroutine Recalc

    Subroutine Recalc_monochromatic(phi_x, phi_y, phi_z, M)
       real,                          intent(in) :: phi_x, phi_y, phi_z ! Variables that describe orientation (angles in order)
       real, dimension(3,3),optional, intent(in) :: M                   ! Matrix transforming a reflection hkl to the Cartesian in L-system
       !
       real :: eff, vol
       real, dimension(3,3) :: UM, Rotd
       integer :: m1,L,k

       n=0
       if(present(M)) then
         MRUB=M   !This corresponds to the matrix M=R.UB (or M= Omega Chi Phi UB)
       else
         call set_rotation_matrix((/phi_x, phi_y, phi_z/),UM) ! UM here is the U matrix
         MRUB=matmul(UM,BL)
       end if
   !    write(unit=*,fmt="(a/3(10x,3(f9.4),/))") " => Recip-to-Lab matrix: ", (MRUB(i,:),i=1,3)
       call Init_Laue_Ref(Ref)

       do i=1,hkl%nref
         Ref%h=hkl%Ref(i)%h
         Ref%kindex=0
         call Calc_Monochromatic_Spot_XZ_Angles(LaueDiff,Ref,MRUB,mosaic_spread,Max_divergence)
         if(Ref%stats == 0)then
           n=n+1
           Ref_Lst%LR(n)=Ref
           Ref_Lst%LR(n)%Domain=1
           Ref_Lst%LR(n)%Cal_Int = hkl%ref(i)%Fc * hkl%ref(i)%Fc
         end if
         if(kvec_given .and. .not.kvec_supress ) then
           do j=1,nkvec
             do k=1,G_k(j)%nk
               do m1=1,nharm(j)
                 Ref%h=hkl%Ref(i)%h+real(m1)*G_k(j)%stark(:,k)
                 Ref%kindex=(/j,k,m1/)
                 call Calc_Monochromatic_Spot_XZ_Angles(LaueDiff,Ref,MRUB,mosaic_spread,Max_divergence,dk_min=dklim)
                 if(Ref%stats == 0)then
                   n=n+1
                   Ref_Lst%LR(n)=Ref
                   Ref_Lst%LR(n)%Domain=1
                 end if
               end do
             end do
           end do
         end if
       end do

       if(Twin_given) then
         do j=2,Tw%N_twins
           Rotd=matmul(UM,matmul(Tw%Twin_Mat(:,:,j),BL))
           do i=1,hkl%nref
             Ref%h=hkl%Ref(i)%h
             Ref%kindex=0
             call Calc_Monochromatic_Spot_XZ_Angles(LaueDiff,Ref,Rotd,mosaic_spread,Max_divergence)
             if(Ref%stats == 0)then
               n=n+1
               Ref_Lst%LR(n)=Ref
               Ref_Lst%LR(n)%Domain=j
               Ref_Lst%LR(n)%Cal_Int = hkl%ref(i)%Fc * hkl%ref(i)%Fc
             end if
             if(kvec_given .and. .not. kvec_supress ) then
               do L=1,nkvec
                 do k=1,G_k(L)%nk
                   do m1=1,nharm(L)
                     Ref%h=hkl%Ref(i)%h+real(m1)*G_k(L)%stark(:,k)
                     Ref%kindex=(/L,k,m1/)
                     call Calc_Monochromatic_Spot_XZ_Angles(LaueDiff,Ref,Rotd,mosaic_spread,Max_divergence,dk_min=dklim)
                     if(Ref%stats == 0)then
                       n=n+1
                       Ref_Lst%LR(n)=Ref
                       Ref_Lst%LR(n)%Domain=j
                     end if
                   end do
                 end do
               end do
             end if

           end do
         end do
       end if

       Ref_Lst%nref=n

     return
    End Subroutine Recalc_monochromatic


    ! subroutine F_A_Orient(condt,f_an_x,f_an_y,f_an_z)
    ! For calculating the rotation angles (X_Angl,Y_Angl,Z_Angl)
    ! that correspond to the best orientation
    ! By comparing different calculated patterns with the experimental one
    ! doing this cycle of comparisons, several times while increasing resolution.
    
    ! PERCEVAL NOTE:  there are errors in that function !!!
    ! => The first step is not done: doing an_x = xfrom + step !!!
    ! => The second pass with search in [-5;+5;1] is not done because:
    !      xfrom = f_an_x + 5 instead of xfrom = f_an_x - 5
    !      xto   = f_an_x - 5 instead of xto   = f_an_x + 5
    
    Subroutine F_A_Orient(condt,f_an_x,f_an_y,f_an_z)
      Type (Orient_angl_cond_type), intent(in out):: condt                ! limits and step of cycle
      real,                         intent(out)   :: f_an_x,f_an_y,f_an_z ! final angle (ang_x,ang_y,ang_z)
      integer, Dimension(:,:), Allocatable :: Mask_Calc,Mask_Expr,Mask_Sum ! masks that describes Laue pattern
      integer                              :: tmp_Ncol,tmp_Nrow            ! number of columns and rows  that changes with resolution
      integer                              :: m_conv,convg                 ! convergence (higher the best)
      integer                              :: Nm_Ex_Ps                     ! Number of experimental spots
      integer                              :: resol                        ! resolution
      integer                              :: same_res,tms                 ! cycle control vars
      real                                 :: an_x,an_y,an_z               ! Orienting rotations (scanned by cycle)
      real                                 :: stp,up_lim                   ! artificial cycle control vars

      if( condt%resl < 10 )then
        condt%resl = 10
      end if
      !write(*,*) "condt%resl =", condt%resl
      resol=condt%resl

!      an_x=11.0
      call Allocate_Pattern_Masks(Mask_Expr,Mask_Calc,Mask_Sum,tmp_Ncol,tmp_Nrow,resol)
      !write(unit=*,fmt="(a,2i6)") 'n_Row,n_Col =',tmp_Nrow,tmp_Ncol
      m_conv=0.0
      !do an_x=condt%xfrom,condt%xto,condt%xstep
      an_x =condt%xfrom
      do while( an_x <= condt%xto  )
          an_x = an_x + condt%xstep

          !do an_y=condt%yfrom,condt%yto,condt%ystep
          an_y =condt%yfrom
          do while( an_y <= condt%yto  )
              an_y = an_y + condt%ystep

              !do an_z=condt%zfrom,condt%zto,condt%zstep
              an_z =condt%zfrom
              do while( an_z <= condt%zto  )
                  an_z = an_z + condt%zstep

                  call Calc_and_Dif(an_x,an_y,an_z,Mask_Expr,Mask_Calc,Mask_Sum,tmp_Ncol,tmp_Nrow,resol,convg,Nm_Ex_Ps)
                  if( convg > m_conv )then
                      m_conv=convg
                      f_an_x=an_x
                      f_an_y=an_y
                      f_an_z=an_z
                      !write(unit=*,fmt="(a)")       '  _______________________________________________'
                      !write(unit=*,fmt="(2(a,i6))") '  convergence = ',convg, '/', Nm_Ex_Ps
                      !write(unit=*,fmt="(a,3f9.4)") '  angls(x,y,z) =',f_an_x,f_an_y,f_an_z
                  end if
              end do
          end do
          !write(unit=*,fmt="(a)")
          !write(unit=*,fmt="(a,f9.4)") '  ang_X =',an_x
      end do

      condt%xfrom=f_an_x+5
      condt%xto=f_an_x-5
      if( condt%xfrom < 0 ) condt%xfrom=0
      if( condt%xfrom > 360 ) condt%xfrom=360
      condt%xstep=1

      condt%yfrom=f_an_y+5
      condt%yto=f_an_y-5
      if( condt%yfrom < 0 ) condt%yfrom=0
      if( condt%yfrom > 360 ) condt%yfrom=360
      condt%ystep=1

      condt%zfrom=f_an_z+5
      condt%zto=f_an_z-5
      if( condt%zfrom < 0 ) condt%zfrom=0
      if( condt%zfrom > 360 ) condt%zfrom=360
      condt%zstep=1

      m_conv=0.0
      !do an_x=condt%xfrom,condt%xto,condt%xstep
      an_x =condt%xfrom
      do while( an_x <= condt%xto  )
          an_x = an_x + condt%xstep

          !do an_y=condt%yfrom,condt%yto,condt%ystep
          an_y =condt%yfrom
          do while( an_y <= condt%yto  )
              an_y = an_y + condt%ystep

              !do an_z=condt%zfrom,condt%zto,condt%zstep
              an_z =condt%zfrom
              do while( an_z <= condt%zto  )
                  an_z = an_z + condt%zstep

                  call Calc_and_Dif(an_x,an_y,an_z,Mask_Expr,Mask_Calc,Mask_Sum,tmp_Ncol,tmp_Nrow,resol,convg,Nm_Ex_Ps)
                  if( convg>m_conv )then
                      m_conv=convg
                      f_an_x=an_x
                      f_an_y=an_y
                      f_an_z=an_z
                      !write(unit=*,fmt="(a)")       '  _______________________________________________'
                      !write(unit=*,fmt="(2(a,i6))") '  convergence = ',convg, '/', Nm_Ex_Ps
                      !write(unit=*,fmt="(a,3f9.4)") '  angls(x,y,z) =',f_an_x,f_an_y,f_an_z
                  end if
              end do
          end do
          !write(unit=*,fmt="(a)")
          !write(unit=*,fmt="(a,f9.4)") '  ang_X =',an_x
      end do

      stp=.1
      do tms=1,10
        m_conv=0.0
        do same_res=1,3
          an_x=f_an_x-stp*20.0
          up_lim=f_an_x+stp*20.0

          an_y=f_an_y
          an_z=f_an_z

          do
            call Calc_and_Dif(an_x,an_y,an_z,Mask_Expr,Mask_Calc,Mask_Sum,tmp_Ncol,tmp_Nrow,resol,convg,Nm_Ex_Ps)
            if( convg>m_conv )then
                m_conv=convg
                f_an_x=an_x
                f_an_y=an_y
                f_an_z=an_z
                !write(unit=*,fmt="(a)")       '  _______________________________________________'
                !write(unit=*,fmt="(2(a,i6))") '  convergence = ',convg, '/', Nm_Ex_Ps
                !write(unit=*,fmt="(a,3f9.4)") '  angls(x,y,z) =',f_an_x,f_an_y,f_an_z
            end if
            an_x=an_x+stp
            if(an_x > up_lim) exit
          end do

          an_x=f_an_x

          an_y=f_an_y-stp*20.0
          up_lim=f_an_y+stp*20.0

          an_z=f_an_z

          do
            call Calc_and_Dif(an_x,an_y,an_z,Mask_Expr,Mask_Calc,Mask_Sum,tmp_Ncol,tmp_Nrow,resol,convg,Nm_Ex_Ps)
            if( convg>m_conv )then
                m_conv=convg
                f_an_x=an_x
                f_an_y=an_y
                f_an_z=an_z
                !write(unit=*,fmt="(a)")       '  _______________________________________________'
                !write(unit=*,fmt="(2(a,i6))") '  convergence = ',convg, '/', Nm_Ex_Ps
                !write(unit=*,fmt="(a,3f9.4)") '  angls(x,y,z) =',f_an_x,f_an_y,f_an_z
            end if
            an_y=an_y+stp
            if(an_y > up_lim) exit
          end do

          an_x=f_an_x
          an_y=f_an_y

          an_z=f_an_z-stp*20.0
          up_lim=f_an_z+stp*20.0

          do
            call Calc_and_Dif(an_x,an_y,an_z,Mask_Expr,Mask_Calc,Mask_Sum,tmp_Ncol,tmp_Nrow,resol,convg,Nm_Ex_Ps)
            if( convg > m_conv )then
                m_conv=convg
                f_an_x=an_x
                f_an_y=an_y
                f_an_z=an_z
                !write(unit=*,fmt="(a)")       '  _______________________________________________'
                !write(unit=*,fmt="(2(a,i6))") '  convergence = ',convg, '/', Nm_Ex_Ps
                !write(unit=*,fmt="(a,3f9.4)") '  angls(x,y,z) =',f_an_x,f_an_y,f_an_z
            end if
            an_z=an_z+stp
            if(an_z > up_lim) exit
          end do
        end do

        stp=stp/2
        resol=resol-1
        if( resol < 2 )then
            resol = 2
            !write(*,*) 'Redundant iteration'
        end if
        !write(*,*) 'resol =', resol
        call Allocate_Pattern_Masks(Mask_Expr,Mask_Calc,Mask_Sum,tmp_Ncol,tmp_Nrow,resol)
        !write(unit=*,fmt="(a,i6)") '  iter # ',tms
      end do

      write(unit=*,fmt="(a)")       '  *********************************************************************'
      write(unit=*,fmt="(a,i6)")    '  Final_resol =', resol
      write(unit=*,fmt="(2(a,i6))") '  Max_convergence =',m_conv, '/', Nm_Ex_Ps
      write(unit=*,fmt="(a,3f9.4)") '  Best_angles(x,y,z) =',f_an_x,f_an_y,f_an_z
      write(unit=*,fmt="(a)")       '  *********************************************************************'

     return
    End Subroutine F_A_Orient


    ! subroutine F_A_Orient_1(condt,f_an_x,f_an_y,f_an_z)
    ! For calculating the rotation angles (X_Angl,Y_Angl,Z_Angl)
    ! that correspond to the best orientation
    ! By comparing different calculated patterns with the experimental one
    ! doing this cycle of comparisons, several times while increasing resolution.

    subroutine F_A_Orient_1(hkl_condt,f_an_x,f_an_y,f_an_z)
        Type (Orient_hkl_rot_cond_type) , intent(in)        :: hkl_condt
        real                  ,        intent(out)          :: f_an_x,f_an_y,f_an_z             ! final angle (ang_x,ang_y,ang_z)
        Type (Orient_angl_cond_type)                        :: ang_condt                            ! limits and step of cycle

        integer , Dimension(:,:) , Allocatable              :: Mask_Calc,Mask_Expr,Mask_Sum     ! masks that describes Laue pattern
        integer                                             :: tmp_Ncol,tmp_Nrow                ! number of columns and rows  that changes with resolution
        integer                                             :: m_conv,convg                     ! convergence (higher the best)
        integer                                             :: Nm_Ex_Ps                         ! Number of experimental spots
        integer                                             :: resol                            ! resolution
        integer                                             :: same_res,tms                     ! cycle control vars
        real                                                :: an_x,an_y,an_z                   ! Orienting rotations (scanned by cycle)
        real                                                :: stp,up_lim                       ! artificial cycle control vars

        integer                                             :: col, row, num_nodl, tot_num_nodl
        Real, dimension(3)                                  :: u_mask, r_mask, tmp_h, u, axis
        logical                                             :: ok
        real, Dimension(1:3,1:3)                            :: tmp_guimat, U_mat, Rl, Rot_mat
        integer                                             :: ang_grr
        integer                                             :: hkl_h, hkl_k, hkl_l
        real                                                :: rad_ang_grr
        real, dimension(:), allocatable                     :: x_mm,y_mm
        num_nodl = 0
        do row=1,Nrow,1
            do col=1,Ncol,1
                if(  Mask2D(row,col,img_n)==2  )then
                    num_nodl = num_nodl + 1
                end if
            end do
        end do
        if( num_nodl == 0 )then
            write(unit=*,fmt="(a)") ' => NO nodal spot selected'
            return
        end if
        allocate(x_mm(1:num_nodl))
        allocate(y_mm(1:num_nodl))
        num_nodl = 0
        do row=1,Nrow,1
            do col=1,Ncol,1
                if(  Mask2D(row,col,img_n)==2  )then
                    num_nodl=num_nodl + 1
                    call pix_to_mil(LaueDiff,real(col),real(row),x_mm(num_nodl),y_mm(num_nodl))
                end if
            end do
        end do

        m_conv=0.0
        tot_num_nodl=num_nodl
        do num_nodl=1,tot_num_nodl
            write(unit=*,fmt="(2(a,i6))")  " => Now trying multiple rotations cycle :", num_nodl, " of", tot_num_nodl
            write(unit=*,fmt="(a,2f10.4)") "    x_mm, y_mm =", x_mm(num_nodl), y_mm(num_nodl)
            call Get_Scattered_wavevector_from_xz(LaueDiff, x_mm(num_nodl), y_mm(num_nodl), u_mask, ok)
            call Get_Reciprocal_vector_from_Scattered_wavevector(u_mask, r_mask)


            resol=30
            call Allocate_Pattern_Masks(Mask_Expr,Mask_Calc,Mask_Sum,tmp_Ncol,tmp_Nrow,resol)

            call set_rotation_matrix( (/0.0, 0.0, 0.0/),  Rot_mat)
            Rl=matmul(Rot_mat, BL)

            do hkl_h = hkl_condt%h_from, hkl_condt%h_to, 1
             do hkl_k = hkl_condt%k_from, hkl_condt%k_to, 1
              do hkl_l = hkl_condt%l_from, hkl_condt%l_to, 1
                    tmp_h= real((/hkl_h, hkl_k, hkl_l/))

                    if( hkl_h == 0 .and.  hkl_k == 0 .and. hkl_l == 0 ) then
                        cycle
                    end if

                    write(unit=*,fmt="(a,3f8.3)") ' => Rotating around H K L =', tmp_h
                    u=matmul(Rl, tmp_h) !Cartesian coordinates of h in the Lab system

                    call set_rotation_matrix( (/0.0, 0.0, 0.0/),  U_mat)
                    call calc_angle_betwen_vectors(u, r_mask, rad_ang_grr)
                    axis = Cross_Product(u, r_mask)
                    call rotate_around_a_vector(axis, rad_ang_grr, U_mat)

                    do ang_grr=0,hkl_condt%ang_step,1
                        rad_ang_grr = pi*real(ang_grr)/(hkl_condt%ang_step/2.0)
                        tmp_guimat = U_mat
                        call rotate_around_a_vector(r_mask,rad_ang_grr,tmp_guimat)
                        call Get_Phi_xyz(tmp_guimat,an_x,an_y,an_z,"deg")
                        call Calc_and_Dif(an_x,an_y,an_z,Mask_Expr,Mask_Calc,Mask_Sum,tmp_Ncol,tmp_Nrow,resol,convg,Nm_Ex_Ps)
                        if( convg>m_conv )then
                            m_conv=convg
                            f_an_x=an_x
                            f_an_y=an_y
                            f_an_z=an_z
                            write(unit=*,fmt="(a)")       '  _______________________________________________'
                            write(unit=*,fmt="(a,3f9.4)") '  angls(x,y,z) =',f_an_x,f_an_y,f_an_z
                            write(unit=*,fmt="(2(a,i6))") '  convergence = ',convg, '/', Nm_Ex_Ps
                        end if
                    end do
              end do
             end do
            end do
        end do

        call Calc_and_Dif(f_an_x,f_an_y,f_an_z,Mask_Expr,Mask_Calc,Mask_Sum,tmp_Ncol,tmp_Nrow,resol,convg,Nm_Ex_Ps)    !!!!!!!!!!!!!!!!!!!!!!!!

        ang_condt%xfrom=f_an_x+5
        ang_condt%xto=f_an_x-5
        if( ang_condt%xfrom < 0 )   ang_condt%xfrom=0
        if( ang_condt%xfrom > 360 ) ang_condt%xfrom=360
        ang_condt%xstep=1

        ang_condt%yfrom=f_an_y+5
        ang_condt%yto=f_an_y-5
        if( ang_condt%yfrom < 0 )   ang_condt%yfrom=0
        if( ang_condt%yfrom > 360 ) ang_condt%yfrom=360
        ang_condt%ystep=1

        ang_condt%zfrom=f_an_z+5
        ang_condt%zto=f_an_z-5
        if( ang_condt%zfrom < 0 )   ang_condt%zfrom=0
        if( ang_condt%zfrom > 360 ) ang_condt%zfrom=360
        ang_condt%zstep=1
        ang_condt%resl = 6
        call F_A_Orient(ang_condt, f_an_x, f_an_y, f_an_z)

    return
    end subroutine F_A_Orient_1
    
    
 !
 !
    subroutine calc_angle_betwen_vectors(vec_ini, vec_fin, ang)
    real, Dimension(1:3), intent(in)    :: vec_ini, vec_fin
    real,                 intent(out)   :: ang
    real, Dimension(1:3)                :: vec_1, vec_2
    real                                :: angle, dst
        vec_1 = vec_ini
        vec_2 = vec_fin
        vec_1=vec_ini/sqrt(dot_product(vec_ini,vec_ini))
        vec_2=vec_fin/sqrt(dot_product(vec_fin,vec_fin))
        ang=acos(dot_product(vec_1,vec_2))

    return
    end subroutine calc_angle_betwen_vectors
    !
    ! Subroutine rotate_around_a_vector(r_vec,ang,r_mat)
    !
    ! This subroutine converts the rotation matrix "r_mat"
    ! into a new matrix that is the result of a rotation
    ! of "r_mat" around the vector "r_vec", rotates an
    ! angle of "ang"
    Subroutine rotate_around_a_vector(r_vec,ang,r_mat)
      real, Dimension(1:3),     intent(in)     :: r_vec
      real,                     intent(in)     :: ang
      real, Dimension(1:3,1:3), intent(in out) :: r_mat
      ! Local Vars
      real, Dimension(1:3)    :: axis_vec
      real                    :: azm_ang, yz_ang    !azimuthal angle, angle in the plane YZ

      axis_vec=r_vec

      azm_ang =atan2(axis_vec(1),axis_vec(3))
      axis_vec=matmul(Matrix_Ry(-azm_ang),axis_vec)
      yz_ang  =atan2(axis_vec(3),axis_vec(2))
      r_mat   =matmul(Matrix_Ry(-azm_ang),r_mat)
      r_mat   =matmul(Matrix_Rx(-yz_ang),r_mat)
      r_mat   =matmul(Matrix_Ry(ang),r_mat)
      r_mat   =matmul(Matrix_Rx(yz_ang),r_mat)
      r_mat   =matmul(Matrix_Ry(azm_ang),r_mat)

      return
    End Subroutine rotate_around_a_vector
 !    Calc_and_Dif(x_an, y_an, z_an, Expr, Calc, Prod, Numcol, Numrow, resl, Coincid)
 !    for calculating and comparing with experimental pattern
    Subroutine Calc_and_Dif(x_an, y_an, z_an, Expr, Calc, Prod, Numcol, Numrow, resl, Coincid, Num_Expr_Spots)
      real,                     intent (in    ) :: x_an,y_an,z_an     ! Orienting rotations
      integer, Dimension(:,:),  intent (in    ) :: Expr               ! masks that describes Laue pattern (experimental)
      integer,                  intent (in    ) :: resl,Numcol,Numrow ! resolution ,number of columns, number of rows
      integer, Dimension(:,:),  intent (   out) :: Calc,Prod          ! masks that describes Laue pattern (Calculated, Product)
      integer,                  intent (   out) :: Coincid            ! convergence (higher the best)
      integer,                  intent (   out) :: Num_Expr_Spots     ! Number of experimental spots
      integer                                   :: row,col            ! cycle control vars
      integer                                   :: tm_rw,tm_cl        ! Position in matrix
      integer                                   :: i                  ! control cycle variables for scanning the calculated reflections
      real                                      :: r_col,r_row        ! temporal calculated position in matrix

      call recalc(x_an, y_an, z_an)
      Calc(1:Numrow,1:Numcol)=0
      do i=1,Ref_Lst%nref
          call mil_to_pix(LaueDiff,Ref_Lst%LR(i)%x,Ref_Lst%LR(i)%z,r_col,r_row)
          col=nint(r_col)
          row=nint(r_row)
          tm_rw=int(row/resl)+1
          tm_cl=int(col/resl)+1

          if(tm_rw > Numrow) tm_rw=Numrow
          if(tm_cl > Numcol) tm_cl=Numcol
          if(tm_rw < 1) tm_rw=1
          if(tm_cl < 1) tm_cl=1

          Calc(tm_rw,tm_cl)=1
      end do

      Prod=Calc*Expr
      !convg=sum(Mask_Sum)
      Coincid = 0
      Num_Expr_Spots = 0
      do row=1, Numrow
          do col=1, Numcol
              Coincid = Coincid + Prod(row,col)
              Num_Expr_Spots = Num_Expr_Spots + Expr(row,col)
          end do
      end do
      return
    End Subroutine Calc_and_Dif


    Subroutine Allocate_Pattern_Masks(Expr,Calc,Prod,Numcol,Numrow,resl)
      integer, Dimension(:,:), Allocatable, intent (in out) :: Calc,Expr,Prod  ! masks that describes Laue pattern (Calculated,Product,experimental)
      integer,                              intent (   out) :: Numcol,Numrow   ! number of columns, number of rows
      integer,                              intent (in    ) :: resl            ! resolution
      !---- Local Variables ----!
      integer :: row,col      ! cycle control vars
      integer :: tm_rw,tm_cl  ! Position in matrix
      integer :: i
      real    :: r_col,r_row

      Numrow=Nrow/resl
      Numcol=Ncol/resl

      !write(unit=*,fmt="(3(a,i6))") '  tmp_rowmax=',Numrow,'tmp_colmax=',Numcol,'resol=',resl
      if( allocated(Expr) ) deallocate(Expr)
      allocate (Expr(1:Numrow,1:Numcol))
      if( allocated(Calc) ) deallocate(Calc)
      allocate (Calc(1:Numrow,1:Numcol))
      if( allocated(Prod) ) deallocate(Prod)
      allocate (Prod(1:Numrow,1:Numcol))

      Expr=0
      do row=1 , Nrow ,1
        do col=1 , Ncol ,1
          if( Mask2D(row,col,img_n) == 1 .or. Mask2D(row,col,img_n) == 2 )then
            tm_rw=int(row/resl)+1
            tm_cl=int(col/resl)+1
            if(tm_rw > Numrow) tm_rw=Numrow
            if(tm_cl > Numcol) tm_cl=Numcol
            if(tm_rw < 1) tm_rw=1
            if(tm_cl < 1) tm_cl=1
            Expr(tm_rw,tm_cl)=1
          end if
        end do
      end do
      return
    End Subroutine Allocate_Pattern_Masks

 !*********************************************************************************************************************************
    Subroutine arr_ppeak_f_calculated_pos(InDat2D, Msk2D, pL, siz_circl_cut, edg_circl_cut,bck2D)

      Integer,Dimension(:,:),        intent(in)  :: InDat2D  ! 2D Pattern
!      integer,Dimension(:,:) ,       intent(in)  :: hkl_Msk
      integer(kind=1),Dimension(:,:),intent(in)  :: Msk2D    ! 2D Mask (0 = Background, 1 = maximum, 2 = nodal maximum, 3 = peak area but not maximum),
      type(Peak_List_Type),          intent(out) :: pL       ! Experimental peaks
      integer ,                      intent(in)  :: siz_circl_cut
      integer ,                      intent(in)  :: edg_circl_cut
      Integer,Dimension(:,:),        intent(in)  :: Bck2D
      !----
      integer :: i,j      ! cycle's control variables
      integer :: y,x      ! coordinates of the experimental maximum
      integer :: lb1,ub1,lb2,ub2
      integer :: dif
      integer :: pcount
      integer :: err_cd


      type (Peak_Type) :: tmp_p    !Temporal variable used for swapping
        lb1=lbound(Msk2D,1)
        ub1=ubound(Msk2D,1)
        lb2=lbound(Msk2D,2)
        ub2=ubound(Msk2D,2)

        pcount=0
        do y=lb1,ub1
            do x=lb2,ub2
                if ( hkl_mask(y,x) /= 0 ) pcount=pcount+1
            end do
        end do
        if( pcount <= 0 ) return

        if (allocated(pL%Peak)) deallocate (pL%Peak)
        allocate(pL%Peak(1:pcount))
        pL%N_Peaks=pcount
        pcount=0

        do y=lb1,ub1
            call prog_bar(lb1,y,ub1,"integrating")
            do x=lb2,ub2
                if ( hkl_mask(y,x) /= 0 ) then
                      pcount=pcount+1
                      pL%Peak(pcount)%pos=(/y,x/)
                      pL%Peak(pcount)%pnt=hkl_mask(y,x)
                      call integrated_intensity(InDat2D,Msk2D,bck2D,pL%Peak(pcount),&
                                                rad_cut=siz_circl_cut, edg_cut=edg_circl_cut)
                end if
            end do
        end do
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! start  temporal algorithm for sorting with debugging purpose
        if (pcount>=2) then                                                          !!!
            do i=1 ,pcount-1,1                                                       !!!
                do j=i+1,pcount,1                                                    !!!
                    if (pL%Peak(i)%Intensity < pL%Peak(j)%Intensity) then            !!!
                        tmp_p=pL%Peak(j)                                             !!!
                        pL%Peak(j)=pL%Peak(i)                                        !!!
                        pL%Peak(i)=tmp_p                                             !!!
                    end if                                                           !!!
                end do                                                               !!!
            end do                                                                   !!!
        end if                                                                       !!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! end  temporal algorithm for sorting with debugging purpose
        !Ordering has been removed ... it is done in visible reflections
    return
    end subroutine arr_ppeak_f_calculated_pos

    !!---- Subroutine Set_Mask_HKL(LaueDiff,VRef,VRef_mask)
    !!----   Type(Laue_Instrument_Type),    intent(in)  :: LaueDiff
    !!----   Type(Laue_Ref_List_Type),      intent(in)  :: VRef
    !!----   integer,        dimension(:,:),intent(out) :: VRef_Mask
    !!----
    !!----  Subroutine setting the visible reflections mask. The number
    !!----  stored in the mask correspond to the ordinal number of the
    !!----  reflections list VRef.
    !!----
    !!----  Created: April 2012 (JRC)
    !!----  Updated: April 2012
    !!----
    Subroutine Set_Mask_HKL(LaueDiff,VRef,VRef_mask)
      Type(Laue_Instrument_Type),    intent(in)      :: LaueDiff
      Type(Laue_Ref_List_Type),      intent(in out)  :: VRef
      integer,        dimension(:,:),intent(out)     :: VRef_Mask
      !--- Local Variables ---!
      integer :: i,j,lbi,lbj,ubi,ubj
      real    :: x,z

      lbi=lbound(VRef_Mask,1); lbj=lbound(VRef_Mask,2)
      ubi=ubound(VRef_Mask,1); ubj=ubound(VRef_Mask,2)
      VRef_Mask=0
      do k=1,VRef%Nref
        call Mil_To_Pix(LaueDiff,VRef%LR(k)%x,VRef%LR(k)%z,x,z)
        j=nint(x); i= nint(z)
        if( i >= lbi .and. i <= ubi .and. j >= lbj .and. j <= ubj)  VRef_Mask(i,j)=k
      end do
      return
    End Subroutine Set_Mask_HKL

    !!---- Subroutine Integrate_circular_boundary_old(rad,edg)
    !!----
    !!---- This subroutine has been introduced in order to preserve the complete Peak_List when the
    !!---- threshold algorithm has been used. This method is faster than using the mask.
    !!----
    Subroutine Integrate_circular_boundary_old(rad,edg)
      integer , intent(in)  :: rad, edg
      !--- Local variables
      Type (Laue_Ref_Type)  :: tmp_p        !temporal peak
      integer               :: i, j
      character(len=256)    :: filenam
      Type(Peak_Type)       :: Peak        !Local Peak variable


      !The calculated reflections correspond VRef_List correspond always
      !to the refined orientation. No need of MASK for integration and the
      !Observed Peak_List is not corrupted

      do i=1,VRef_List(img_n)%Nref
           call Mil_To_Pix(LaueDiff,VRef_List(img_n)%LR(i)%x,VRef_List(img_n)%LR(i)%z,&
           peak%Pos(2),peak%Pos(1))
           call integrated_intensity(Data2D(:,:,img_n),Mask2D(:,:,img_n),background2D,Peak, rad_cut=rad, edg_cut=edg)
           if(Peak%Intensity < 0.0) then
             VRef_List(img_n)%LR(i)%Obs_Int= 0.1
           else
             VRef_List(img_n)%LR(i)%Obs_Int= Peak%Intensity
           end if
           VRef_List(img_n)%LR(i)%Sigma  = Peak%Sigma
      end do

      i=index(File_Im(img_n),".",back=.true.)
      if(i /= 0) then
        filenam=File_Im(img_n)(1:i-1)//"_IntegInt.hkl"
      else
        filenam=trim(File_Im(img_n))//"_IntegInt.hkl"
      end if

      call Write_Integrated_Reflections(filenam,VRef_List(img_n))
      write(unit=*,fmt="(a)") ' => Done!'

    End Subroutine Integrate_circular_boundary_old
!

    Subroutine Integrate_circular_boundary(rad,edg)
        integer , intent(in)  :: rad, edg
        integer               :: i_img, i,k
        character(len=256)    :: filenam
        i_img=img_n
        write(unit=*,fmt="(a)") ' => Integrating Intensities'

   !     !First ensure that the calculated reflections correspond properly
   !     !to the refined orientation
   !     if(Apply_offsets) then                                            ! this commented lines follow a different
   !       call Set_Mask_HKL(Mod_LDiff(i_img),VRef_List(i_img),hkl_mask)   ! approach and will not be used for now
   !     else
   !       call Set_Mask_HKL(LaueDiff,VRef_List(i_img),hkl_mask)
   !     end if

        call arr_ppeak_f_calculated_pos(Data2D(:,:,i_img),Mask2D(:,:,i_img),Peak_List(i_img),rad,edg,background2D)

        !Assign the integrated intensities and sigmas to the visible reflections

        do i=1,Peak_List(i_img)%N_peaks
           k=Peak_List(i_img)%Peak(i)%pnt
           if( k <= VRef_List(i_img)%nref )then
             if(Peak_List(i_img)%Peak(i)%Intensity < 0.0) then
                VRef_List(i_img)%LR(k)%Obs_Int= 0.1
             else
                VRef_List(i_img)%LR(k)%Obs_Int=Peak_List(i_img)%Peak(i)%Intensity
             end if
             VRef_List(i_img)%LR(k)%sigma=Peak_List(i_img)%Peak(i)%Sigma
           end if
        end do

        i=index(File_Im(i_img),".",back=.true.)
        if(i /= 0) then
          filenam=File_Im(i_img)(1:i-1)//"_IntegInt.hkl"
        else
          filenam=trim(File_Im(i_img))//"_IntegInt.hkl"
        end if
        call Write_Integrated_Reflections(filenam,VRef_List(i_img))
        write(unit=*,fmt="(a)") ' => Done!'
     return
    End Subroutine Integrate_circular_boundary


    subroutine Mask_Trt_f_calculated_pos(xmin,ymin,xmax,ymax,Data2D,siz_circl_cut,Mask2D)
       real,                          intent(in)  :: xmin,ymin       ! lower left corner of the viewing area in pixels
       real,                          intent(in)  :: xmax,ymax       ! uper right corner of the viewing area in pixels
       Integer,Dimension(:,:),        intent(in)  :: Data2D          ! Converted data
       Integer,                       intent(in)  :: siz_circl_cut   ! size of circular area that will be cutted
       integer(kind=1),Dimension(:,:),intent(out) :: Mask2D

       !---- Local Variables ----!
        integer    :: dp,dp_cuad,ytm1,ytm2,xtm1,xtm2,x,y
        integer    :: dx,dy
        integer    :: x_ps,y_ps
        integer    :: lb1,ub1,lb2,ub2

        lb1=lbound(Mask2D,1)
        ub1=ubound(Mask2D,1)
        lb2=lbound(Mask2D,2)
        ub2=ubound(Mask2D,2)

        Mask2D(nint(ymin):nint(ymax),nint(xmin):nint(xmax))=0
        dp=siz_circl_cut
        dp_cuad=dp*dp

        do x=nint(xmin),nint(xmax)
            do y=nint(ymin),nint(ymax)
                if( hkl_mask(y,x)/=0 )then
                    Mask2D(y,x)=1
                    ytm1=y-dp
                    ytm2=y+dp
                    xtm1=x-dp
                    xtm2=x+dp
                    if( ytm1 < lb1 ) ytm1=lb1
                    if( ytm2 > ub1 ) ytm2=ub1
                    if( xtm1 < lb2 ) xtm1=lb2
                    if( xtm2 > ub2 ) xtm2=ub2

                    do x_ps=xtm1,xtm2
                        do y_ps=ytm1,ytm2
                            if( Mask2D(y_ps,x_ps)==0 )then
                                dx=x_ps-x
                                dy=y_ps-y
                                if( dx*dx+dy*dy < dp_cuad ) Mask2D(y_ps,x_ps)=4
                            end if
                        end do
                    end do
                end if
            end do
        end do

    return
    end subroutine Mask_Trt_f_calculated_pos

    !
    ! prof_cut(d,dat1d,minz,maxz,Data2D,dx,dy,xini,yini,Ncol,Nrow)
    ! from a 2D image, assigning the profile along a line to an array
    Subroutine Prof_Cut(Data2D,dx,dy,xini,yini,d,dat1d,hkl_1d)!,Mask2D) ! var [Mask2D] could be uncommented for debugging purpose
        Integer ,Dimension(:,:),   intent( in) :: Data2D    ! image stored in a 2D array
        real,                      intent( in) :: dx,dy     ! coordinates difference (in X and Y )
        real,                      intent( in) :: xini,yini ! starting point in the 2D image
        real,                      intent(out) :: d         ! distance and array size (same var)
        integer ,Dimension(:),     intent(out) :: dat1d     ! profile in a 1D array
        integer ,Dimension(:,:),   intent(out) :: hkl_1d
  !      integer(kind=1),Dimension(:,:),  intent(in out) :: Mask2D  ! could be uncommented for debugging purpose

        !local vars
        integer                                     :: Ncol,Nrow            ! limits of 2D array
        real                                        :: ang
        integer                                     :: x,y,dc,itsd          !
        integer                                     :: x_df,y_df            !
        integer         ,    Dimension(1:3)         :: h

        Ncol=ubound(Data2D, 2)
        Nrow=ubound(Data2D, 1)

        ang=atan2(dy,dx)
        d=sqrt(dx*dx+dy*dy)+1
        dat1d=0
        hkl_1d=0
        if(dx*dy > 0)then
            y_df=-1
            x_df=1
        else
            y_df=1
            x_df=1
        end if
        do dc=1, int(d)
            x=nint(xini+dc*cos(ang))
            y=nint(yini+dc*sin(ang))
    ! __ next commented lines could be uncommented for debugging purpose
    !         if(Mask2D(y,x)==0)then
    !             Mask2D(y,x)=4
    !         else
    !             Mask2D(y,x)=5
    !         end if
            if (x > 1 .and. x < Ncol-1 .and. y > 1 .and. y < Nrow-1) then
                itsd=Data2D(y,x)+Data2D(y+y_df,x+x_df)
                if( hkl_mask(y,x)/=0 )then
                    h= nint(Ref_Lst%LR(hkl_mask(y,x))%h)
                else if( hkl_mask(y+y_df,x+x_df)/=0 )then
                    h= nint(Ref_Lst%LR(hkl_mask(y+y_df,x+x_df))%h)
                else
                    h=0.0
                end if
            else
                itsd=0
                h=0.0
            end if
            hkl_1d(:,dc)=h
            dat1d(dc)=itsd
        end do

    return
    End Subroutine Prof_Cut

    Subroutine Write_Refinement_File(fileres,vary_lines,correl,tol,maxfun,zero_off)
       character(len=*),                 intent(in) :: fileres
       character(len=*), dimension(:),   intent(in) :: vary_lines
       integer,                          intent(in) :: maxfun
       real,                             intent(in) :: tol, correl
       Character(len=*),optional,        intent(in) :: zero_off

       integer           :: iexp, i
       character(len=80) :: fileiref

       Call Get_LogUnit(iexp)
       open(unit=iexp,file=trim(fileres)//".cfl",status="replace",action="write", position="rewind")
       write(unit=iexp,fmt="(a)") "Title: UB-Refinement file for Laue data (Generated by Laue_Simulator)"
       write(unit=iexp,fmt="(a)") "!          a         b        c        alpha     beta    gamma"
       write(unit=iexp,fmt="(a,3f12.5,3f10.4)") "Cell ",Off_set%Cell,Off_set%CellAng
       write(unit=iexp,fmt="(a)") "!     Space Group"
       write(unit=iexp,fmt="(a)") "SpGr  "//trim(SpG%SPG_Symb)
       write(unit=iexp,fmt="(a)") "!     External Files"
       fileiref=File_Im(img_n)
       i=index(fileiref,".",back=.true.)
       if(i /= 0) fileiref=fileiref(1:i-1)
       write(unit=iexp,fmt="(a)") "PEAKS_FILE    "//trim(fileiref)//"_peaks.inf"
       write(unit=iexp,fmt="(a)") "INSTR         "//trim(Instrm_File)
       write(unit=iexp,fmt="(a)") "!           Phi_x      Phi_y     Phi_z   of  Busing-Levy U-matrix"
       write(unit=iexp,fmt="(a,3f11.5)") "ORIENT  ",Off_set%Orient
       write(unit=iexp,fmt="(a)") "      "
       write(unit=iexp,fmt="(a)") "OFFSETS      "
       if(present(zero_off))  then
         write(unit=iexp,fmt="(a,f12.5)") "   Dist       ",0.0
         write(unit=iexp,fmt="(a,f12.5)") "   Tilt_x     ",0.0
         write(unit=iexp,fmt="(a,f12.5)") "   Tilt_y     ",0.0
         write(unit=iexp,fmt="(a,f12.5)") "   Tilt_z     ",0.0
         write(unit=iexp,fmt="(a,f12.5)") "   Gamma_c    ",0.0
         write(unit=iexp,fmt="(a,f12.5)") "   Nu_c       ",0.0
         write(unit=iexp,fmt="(a,f12.5)") "   rOD_x      ",0.0
         write(unit=iexp,fmt="(a,f12.5)") "   rOD_y      ",0.0
         write(unit=iexp,fmt="(a,f12.5)") "   rOD_z      ",0.0
         write(unit=iexp,fmt="(a,f12.5)") "   orig_x     ",0.0
         write(unit=iexp,fmt="(a,f12.5)") "   orig_z     ",0.0
       else
         write(unit=iexp,fmt="(a,f12.5)") "   Dist       ",Off_set%Dist
         write(unit=iexp,fmt="(a,f12.5)") "   Tilt_x     ",Off_set%Tilt_x
         write(unit=iexp,fmt="(a,f12.5)") "   Tilt_y     ",Off_set%Tilt_y
         write(unit=iexp,fmt="(a,f12.5)") "   Tilt_z     ",Off_set%Tilt_z
         write(unit=iexp,fmt="(a,f12.5)") "   Gamma_c    ",Off_set%Gamma_c
         write(unit=iexp,fmt="(a,f12.5)") "   Nu_c       ",Off_set%Nu_c
         write(unit=iexp,fmt="(a,f12.5)") "   rOD_x      ",Off_set%rOD_x
         write(unit=iexp,fmt="(a,f12.5)") "   rOD_y      ",Off_set%rOD_y
         write(unit=iexp,fmt="(a,f12.5)") "   rOD_z      ",Off_set%rOD_z
         write(unit=iexp,fmt="(a,f12.5)") "   orig_x     ",Off_set%orig_x
         write(unit=iexp,fmt="(a,f12.5)") "   orig_z     ",Off_set%orig_z
       end if
       write(unit=iexp,fmt="(a)") "END_OFFSETS"
       write(unit=iexp,fmt="(a)") "      "
       write(unit=iexp,fmt="(a)") "CHEBYCHEV    8"
       write(unit=iexp,fmt="(8f8.4,a)") Cheb(1:8,1),"   !x"
       write(unit=iexp,fmt="(8f8.4,a)") Cheb(1:8,2),"   !z"
       write(unit=iexp,fmt="(a)") "END_CHEBYCHEV     "
       write(unit=iexp,fmt="(a)") "      "
       write(unit=iexp,fmt="(a)") "LSQ_CONDITIONS     "
       write(unit=iexp,fmt="(a,f8.3)")    "  Corrmax",correl
       write(unit=iexp,fmt="(a,i5)")      "  Maxfun ",maxfun
       write(unit=iexp,fmt="(a,1pe12.4)") "  Tol    ",tol
       write(unit=iexp,fmt="(a)") "END_LSQ_CONDITIONS "
       write(unit=iexp,fmt="(a)") "      "
       do i=1,9
         if(trim(vary_lines(i)) == "VARY") cycle
         write(unit=iexp,fmt="(a)") vary_lines(i)
       end do
       flush(unit=iexp)
       close(unit=iexp)
       return
    End Subroutine Write_Refinement_File

    Subroutine Save_Indexed_Peaks_all_images()
      integer :: i
      write(unit=*,fmt="(a)") "  "
      do i=1,n_img
        write(unit=*,fmt="(a)") " => Saving Indexed Peaks of image: "//trim(File_Im(i))
        call Output_Indexed_Reflections(LaueDiff,Cnd%dmmtol,i)
      end do
      call Update_Orientation(U_angles=Curr_U_angles,spindle=img_spdl(img_n))
      return
    End Subroutine Save_Indexed_Peaks_all_images

    Subroutine Output_Indexed_Reflections(LaueDiff,tol,im_num)
      Type(Laue_Instrument_Type),intent(in)     :: LaueDiff
      Real,                      intent(in)     :: tol
      integer, optional,         intent(in)     :: im_num
      !
      Integer :: zop,i,imn

       if(present(im_num)) then
         imn=im_num
       else
         imn=img_n
       end if

       If(Peak_List(imn)%N_Peaks <= 4) return !Abandon if no experimental peaks are stored

         !Output the indexed reflection file of the current image (similar to Laue_Orient)
         !Here is the place in which VRef_List is set up!
         if(present(im_num)) then
           ! A call to Update_Orientation is needed for calculating the Current_M matrix for
           ! the image number sent in the optional argument im_num
           call Update_Orientation(U_angles=Curr_U_angles,spindle=img_spdl(imn))
         end if
         !
         Call Calc_Visible_Reflections(hkl%nref,LaueDiff,hkl,Current_M,VRef_List(imn),mask=Mask_Excl)
         VRef_List_Set(imn)=.true.
         !
         call Allocate_Laue_Ref_list(ORefL,Peak_List(imn)%N_Peaks)
         !if(Peaks_Threshold) then
         !  do i=1,Peak_List(img_n)%N_Peaks
         !      call pix_to_mil(LaueDiff,Peak_List(imn)%Peak(i)%cdm(2),Peak_List(imn)%Peak(i)%cdm(1),ORefL%LR(i)%x,ORefL%LR(i)%z)
         !      call Calc_Spherical_angles_from_xz(LaueDiff,ORefL%LR(i),zop)
         !      ORefL%LR(i)%Obs_int=Peak_List(img_n)%Peak(i)%Intensity
         !  end do
         !else
           do i=1,Peak_List(imn)%N_Peaks
               call pix_to_mil(LaueDiff,Peak_List(imn)%Peak(i)%pos(2),Peak_List(imn)%Peak(i)%pos(1),ORefL%LR(i)%x,ORefL%LR(i)%z)
               call Calc_Spherical_angles_from_xz(LaueDiff,ORefL%LR(i),zop)
               ORefL%LR(i)%Obs_int=Peak_List(imn)%Peak(i)%Intensity
           end do
         !end if

         Call File_Indexed_Reflections(File_Im(imn),img_spdl(imn),Peak_List(imn),ORefL,VRef_List(imn),tol)
       return
    End Subroutine Output_Indexed_Reflections

    Subroutine File_Indexed_Reflections(imgname,spindle,PL,ORfL,RfL,tol)
      character(len=*),           intent(in)     :: imgname  !Name of the image file for output
      real,                       intent(in)     :: spindle  !Used only for output in the file
      Type(Peak_List_Type),       intent(in out) :: PL       !Experimental Observed peaks (passed to update pnt)
      Type(Laue_Ref_List_Type),   intent(in out) :: ORfL     !Observed peaks
      Type(Laue_Ref_List_Type),   intent(in)     :: RfL      !Visible reflections
      Real,                       intent(in)     :: tol      !tolerance for indexing
      !---- Local variables ----!
      character(len=132) :: fileiref   !file with data on indexed reflections
      character(len=132) :: unindexed_name
      integer ::  n,i,j,k,is, ncoi,unin,lun, iexp
      real    :: dist, rms
      integer, dimension(ORfL%nref) :: io, poi
      integer, dimension(RfL%nref)  :: ic

      !Opening the files containing indexed and unindexed peaks
      i=index(imgname,".",back=.true.)
      if(i /= 0) then
        fileiref=imgname(1:i-1)//"_peaks.inf"
        unindexed_name=imgname(1:i-1)//"_unindexed_peaks.inf"
      else
        fileiref=trim(imgname)//"_peaks.inf"
        unindexed_name=trim(imgname)//"_unindexed_peaks.inf"
      end if
      call Get_logUnit(iexp)
      open(unit=iexp,file=trim(fileiref),status="replace",action="write")
      write(unit=iexp,fmt="(a)") "NUMBER OF DATA SETS:  1"
      Call Get_LogUnit(lun)
      open(unit=lun,file=trim(unindexed_name),status="replace",action="write")
      write(unit=lun,fmt="(a)") "  UNINDEXED PEAKS OF IMAGE: "//trim(imgname)

      !Test for a 10% decrease of tolerance
      call matching_4V(ORfL%LR(:)%x,ORfL%LR(:)%z,RfL%LR(:)%x,RfL%LR(:)%z,ORfL%nref,RfL%nref,tol*0.9,io,ic,ncoi)

      !Mapping of observed reflections into coincidences
      poi=0
      do is=1,ncoi
         j=io(is)
         k=ic(is)
         poi(j)=k
      end do
      n=0
      rms=0.0
      unin=0
      do j=1,ORfL%nref
         k=poi(j)
         if( k /= 0) then
           n=n+1
           ORfL%LR(j)%h=RfL%LR(k)%h  !/RfL%LR(k)%nodal  !Attributing indices to observed reflections
           dist=(ORfL%LR(j)%x-RfL%LR(k)%x)**2+(ORfL%LR(j)%z-RfL%LR(k)%z)**2
           rms=rms+dist
           dist=sqrt(dist)
           PL%Peak(j)%pnt=k
         else
           unin=unin+1
           PL%Peak(j)%pnt=-1
           write(unit=lun,fmt="(i9,2f10.3,tr48,f14.0)") j,ORfL%LR(j)%x,ORfL%LR(j)%z,ORfL%LR(j)%Obs_int
         end if
      end do
      write(unit=lun,fmt="(a,i5)") "  TOTAL NUMBER OF UNINDEXED PEAKS: ",unin
      close(unit=lun)
      rms=sqrt(rms/real(n))
      write(unit=iexp,fmt="(a)") "NEW DATA SET :    "//trim(imgname)
      write(unit=iexp,fmt="(a,f10.4,i6,a,f8.4)") "SPINDLE ", spindle,n,"  reflections, initial RMS: ",rms
      do j=1,ORfL%nref
       k=poi(j)
       if( k /= 0) then
         write(unit=iexp,fmt="(2f11.4,tr3,3i5,f14.0)") ORfL%LR(j)%x,ORfL%LR(j)%z,nint(ORfL%LR(j)%h),ORfL%LR(j)%Obs_int
       end if
      end do
      flush(unit=iexp)
      close(unit=iexp)
      uPeak_File=.true.  !Update the logicals related to this two kind of files
      iPeak_File=.true.
      return
    End Subroutine File_Indexed_Reflections

    Subroutine Finl()
    ! none yet
    return
    End Subroutine Finl

  End Module laue_siml_Modl
