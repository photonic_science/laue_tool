set   CRYSFML=..
set   SXTALSOFT=..\\sxtalsoft
set   LAUESUI=%SXTALSOFT%\\Laue_Suite_Project

gfortran %LAUESUI%\\Src\\gfortran_specific.f90            -c -O3 -funroll-loops -m64
gfortran %LAUESUI%\\Src\\Laue_Module.f90                  -c -O3 -funroll-loops -m64  -I%CRYSFML%\\GFortran\\LibWin64
gfortran %LAUESUI%\\Src\\Data_Trt.f90                     -c -O3 -funroll-loops -m64  -I%CRYSFML%\\GFortran\\LibWin64
gfortran %LAUESUI%\\Src\\Laue_Intensity.f90               -c -O3 -funroll-loops -m64  -I%CRYSFML%\\GFortran\\LibWin64
gfortran %LAUESUI%\\Src\\Laue_tiff_read.f90               -c -O3 -funroll-loops -m64  -I%CRYSFML%\\GFortran\\LibWin64 -fno-range-check
gfortran %LAUESUI%\\Src\\rnw_bin.f90                      -c -O3 -funroll-loops -m64  -fno-range-check
gfortran %LAUESUI%\\Src\\Laue_Utilities_Mod.f90           -c -O3 -funroll-loops -m64  -I%CRYSFML%\\GFortran\\LibWin64
gfortran %LAUESUI%\\Src\\Laue_Suite_Global_Parameters.f90 -c -O3 -funroll-loops -m64  -I%CRYSFML%\\GFortran\\LibWin64


gfortran Laue_Sim_Mod.f90   -c -O3 -funroll-loops -m64  -I%CRYSFML%\\GFortran\\LibWin64
gfortran lafg_IMG.f90       -c -O3 -funroll-loops -m64  -I%CRYSFML%\\GFortran\\LibWin64

copy *.mod ..\\GFortran\\LibWin64


gfortran *.o -shared      -o psl_calc_x64.dll     -O3 -funroll-loops -m64  -L%CRYSFML%\\GFortran\\LibWin64 -lcrysfml

del *.obj *.mod *.o
