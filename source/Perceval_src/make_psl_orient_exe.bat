set   LAUESUI=%SXTALSOFT%\Laue_Suite_Project

gfortran %LAUESUI%\Laue_Modules\Src\gfortran_specific.f90            -c -O3 -funroll-loops
gfortran %LAUESUI%\Laue_Modules\Src\Laue_Module.f90                  -c -O3 -funroll-loops   -I%CRYSFML%\gfortran\libC
gfortran %LAUESUI%\Laue_Modules\Src\Data_Trt.f90                     -c -O3 -funroll-loops   -I%CRYSFML%\gfortran\libC
gfortran %LAUESUI%\Laue_Modules\Src\Laue_Intensity.f90               -c -O3 -funroll-loops   -I%CRYSFML%\gfortran\libC
gfortran %LAUESUI%\Image_handling\Src\Laue_tiff_read.f90             -c -O3 -funroll-loops   -I%CRYSFML%\gfortran\libC -fno-range-check
gfortran %LAUESUI%\Image_handling\Src\rnw_bin.f90                    -c -O3 -funroll-loops   -fno-range-check
gfortran %LAUESUI%\Laue_Modules\Src\Laue_Utilities_Mod.f90           -c -O3 -funroll-loops   -I%CRYSFML%\gfortran\libC
gfortran %LAUESUI%\Laue_Modules\Src\Laue_Suite_Global_Parameters.f90 -c -O3 -funroll-loops   -I%CRYSFML%\gfortran\libC

gfortran Laue_Sim_Mod.f90   -c -O3 -funroll-loops   -I%CRYSFML%\gfortran\libC
gfortran lafg_IMG.f90       -c -O3 -funroll-loops   -I%CRYSFML%\gfortran\libC
gfortran lafg.f90           -c -O3 -funroll-loops   -I%CRYSFML%\gfortran\libC

gfortran *.o -o psl_calc.exe   -O3 -funroll-loops -msse2 -L%CRYSFML%\gfortran\LibC -lcrysfml

del *.obj *.mod *.o
