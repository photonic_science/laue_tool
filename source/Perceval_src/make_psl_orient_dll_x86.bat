set   LAUESUI=%SXTALSOFT%\Laue_Suite_Project

gfortran %LAUESUI%\Laue_Modules\Src\gfortran_specific.f90            -c -O3 -funroll-loops
gfortran %LAUESUI%\Laue_Modules\Src\Laue_Module.f90                  -c -O3 -funroll-loops   -I%CRYSFML%\gfortran\libC
gfortran %LAUESUI%\Laue_Modules\Src\Data_Trt.f90                     -c -O3 -funroll-loops   -I%CRYSFML%\gfortran\libC
gfortran %LAUESUI%\Laue_Modules\Src\Laue_Intensity.f90               -c -O3 -funroll-loops   -I%CRYSFML%\gfortran\libC
gfortran %LAUESUI%\Image_handling\Src\Laue_tiff_read.f90             -c -O3 -funroll-loops   -I%CRYSFML%\gfortran\libC -fno-range-check
gfortran %LAUESUI%\Image_handling\Src\rnw_bin.f90                    -c -O3 -funroll-loops   -fno-range-check
gfortran %LAUESUI%\Laue_Modules\Src\Laue_Utilities_Mod.f90           -c -O3 -funroll-loops   -I%CRYSFML%\gfortran\libC
gfortran %LAUESUI%\Laue_Modules\Src\Laue_Suite_Global_Parameters.f90 -c -O3 -funroll-loops   -I%CRYSFML%\gfortran\libC


gfortran Laue_Sim_Mod.f90   -c -O3 -funroll-loops   -I%CRYSFML%\gfortran\libC
gfortran lafg_IMG.f90       -c -O3 -funroll-loops   -I%CRYSFML%\gfortran\libC

gfortran *.o -shared       -o psl_calc_x86.dll     -O3 -funroll-loops -msse2 -L%CRYSFML%\gfortran\LibC -lcrysfml
rem gfortran *.o -shared -mrtd -o psl_calc_win.dll -O3 -funroll-loops -msse2 -L%CRYSFML%\gfortran\LibC -lcrysfml

del *.obj *.mod *.o

rem copy psl_calc_x86.dll C:\CodePython\LaueTool\bin