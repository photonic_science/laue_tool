!! module lafg_image
!! This module contains most subroutines related with the GUI of LAFG
!! This subroutines are called from the main cycle (lafg_main program)
!! and from this module (lafg_image) are called most calculations and
!! data treatment subroutines
  module lafg_psl_image
    Use Data_Trt
    Use Laue_Suite_Global_Parameters
    Use Laue_TIFF_Read
    Use Laue_Mod
    Use laue_siml_modl
    Use CFML_String_Utilities,         only: Get_Logunit
    use CFML_Math_3D ,                 only: Cross_Product
    Use CFML_GlobalDeps,               only: pi

    use iso_c_binding,                 only: c_float, c_int

    implicit none
    public

    !!private
    !!public  ::  Open_cfl, Open_img, calc_mask_n_write_file, repaint_img, CLI_orient,   &
    !!          write_angles_into_file, show_peacks_console, read_experimental_pks_from_file, &
    !!          write_file_w_experimental_pks, read_angles_from_file, CLI_Find_Peak

    !!public  :: allocat_mask, ini_coord, read_orient_param_from_file, read_pk_fnd_param_from_file

    Type , Public :: psl_orient_params
      real    :: xfrom, xto, xstep, yfrom, yto, ystep, zfrom, zto, zstep
      integer :: resl
    End Type psl_orient_params

    INTEGER                     :: img_calc
    CHARACTER(LEN=256)          :: FNAME = ' '
    logical                     :: opn = .false. , MousDw = .false. , auto_rescale=.true.
    real                        :: xinidrag, yinidrag, x_mov_old, y_mov_old, asp_rat_pic_old
    integer                     :: img_xmin, img_ymin, img_xmax, img_ymax
    integer                     :: x_max_siz=300, y_max_siz=150
    integer                     :: dst_min = 5, peak_ar = 6, m_peak = 6
    integer                     :: From_X_Ang = 0, From_Y_Ang = 0, From_Z_Ang = 0
    integer                     :: To_X_Ang = 25, To_Y_Ang = 25, To_Z_Ang = 25
    integer                     :: x_step = 1, y_step = 1, z_step = 1, ini_resol = 10


    CONTAINS

!! subroutine allocat_mask()
!! once the program knows the instrument resolution
!! in pixels, it prepares the variables
    subroutine allocat_mask()
        Nrow=LaueDiff%np_v
        Ncol=LaueDiff%np_h

        if(allocated(hkl_mask))then
            deallocate(hkl_mask)
        end if
        allocate(hkl_mask(1:Nrow,1:Ncol))

        if (allocated(Data2D)) then
            deallocate (Data2D)
        end if
        allocate(Data2D(1:Nrow, 1:Ncol,1:n_img))

        if (allocated(Mask2D)) then
            deallocate (Mask2D)
        end if
        allocate(Mask2D(1:Nrow, 1:Ncol,1:n_img))
        Mask2D = 0
        if (allocated(img_spdl)) then
            deallocate (img_spdl)
        end if
        allocate(img_spdl(1:n_img))


    return
    end subroutine allocat_mask

!! SUBROUTINE Open_img()
!! shows the section dialog and then opens an image
    SUBROUTINE Open_img(nom)
    Character(Len=512)  , intent(in)            :: nom
    integer                                     :: i, j
    logical                                     :: ok
      !  Image_File='fel_22.tif'
        Image_File=nom
        write(*,*) '<<<<'//trim(Image_File)//'>>>>'
        if (trim(Image_File)/='') then
            call allocat_mask()
            img_spdl=0
            Data2D=0

            call read_raw_image(Image_File,Nrow,Ncol,Data2D(:,:,n_img))
            if( LaueDiff%invert =="Yes" .or. LaueDiff%invert =="YES" .or. LaueDiff%invert =="yes" )then
                write(*,*) "LaueDiff%invert =", LaueDiff%invert
                Call Invert_Intensities_2D(Data2D(:,:,n_img))
            end if
            opn=.true.
          !  call read_tiff_image(Image_File,Data2D(:,:,1),Nrow,Ncol,ok,ICondt)
          !  if(TIFF_READ_Error) then
          !      write(*,"(a)") " ===> "//trim(TIFF_READ_Error_Message)
          !  else
          !      write(*,*) "Image opened"
          !    !  write(*,*) "maxval(intens)", maxval(Data2D(:,:,1))
          !    !  write(*,*) "minval(intens)", minval(Data2D(:,:,1))
          !  end if
            if (allocated(Peak_List))  deallocate(Peak_List)
            allocate(Peak_List(1:n_img))

            if( ok ) then
                opn=.true.
                call ini_coord()
            end if
            img_spdl(1)=ICondt%expose_phi
        else
            write(*,*) 'Canceled'
        end if
    RETURN
    END SUBROUTINE Open_img

!! SUBROUTINE Open_cfl()
!! shows the section dialog and then opens a CFL File
    subroutine Open_cfl(nom)
    Character(Len=512) , intent(in)              :: nom
    Character(Len=6)                             :: read_inst
    integer                                      :: i, j

      CFL_File = trim(nom)
      !CFL_File = "PSLCRYS.cfl"
      !  CFL_File = "feind_tst.cfl"
      n_img = 1
      img_n = 1

      !write(*,*) 'opening CFL file'
      !write(*,*) 'File ='//trim(CFL_File)//'<='
      !write(*,*) 'File =', nom

      Instrm_Loaded=.true.
      call Read_CFL_File(read_inst)

      !write(*,*) 'CFL file has been read'

      if(Instrm_Loaded)then
          call allocat_mask()
          call ini_coord()
      end if

      !write(*,*) "LaueDiff%xo =", LaueDiff%xo
      !write(*,*) "LaueDiff%zo =", LaueDiff%zo

      !write(*,*) "Image_File =", Image_File

    RETURN
    END SUBROUTINE Open_cfl

    subroutine ini_coord()
        img_xmin=1
        img_ymin=1
        img_xmax=Ncol
        img_ymax=Nrow
    return
    end subroutine ini_coord

!! subroutine repaint_img()
!! repaints or refreshes the image that contains both
!! patterns, experimental and calculated.
    subroutine repaint_img(X_ang, Y_ang, Z_ang)
        real , intent(in)  :: X_ang, Y_ang, Z_ang
        if(Instrm_Loaded .and. Cell_Loaded)then
            !write(*,*) "Calculating"
            !write(*,*) 'Ang(x,y,z) =', X_ang, Y_ang, Z_ang
            call Recalc(X_ang, Y_ang, Z_ang)
            !call calc_mask_n_write_file()
        end if
    return
    end subroutine repaint_img

!! subroutine calc_mask_n_write_file
    subroutine calc_mask_n_write_file(calc_nam)
    Character(Len=512), intent(in)     :: calc_nam
    real            :: x_pc,y_pc, x_milm, z_milm
!    integer         :: absum_new , absum_old
    integer         :: j
    integer         :: x_pix,y_pix
    integer         :: lun
    logical         :: in_img
        call Get_LogUnit(lun)
        open(unit=lun, file=calc_nam, status="replace", action="write")
        hkl_mask=0
        do j=1,Ref_Lst%nref
            x_milm=Ref_Lst%LR(j)%x
            z_milm=Ref_Lst%LR(j)%z

            call mil_to_pix(LaueDiff,x_milm,z_milm,x_pc,y_pc)
            x_pix=nint(x_pc)
            y_pix=nint(y_pc)
            in_img = .true.
            if( x_pix>Ncol )then
                x_pix = Ncol
                in_img = .false.
            end if
            if( y_pix>Nrow )then
                y_pix = Nrow
                in_img = .false.
            end if

            if( x_pix<1 )then
                x_pix = 1
                in_img = .false.
            end if
            if( y_pix<1 )then
                y_pix = 1
                in_img = .false.
            end if
            if (in_img ) then
!            if (in_img .and. hkl_mask(y_pix,x_pix)==0 ) then
                if(hkl_mask(y_pix,x_pix)==0)then
                    write(unit=lun,fmt='(I6,A2,I6,A2,I3,A2,I3,A2,I3)')  x_pix, ', ', y_pix, ', ', int(Ref_Lst%LR(j)%h(1)),&
                                                         ', ', int(Ref_Lst%LR(j)%h(2)), ', ', int(Ref_Lst%LR(j)%h(3))
                else
                    write(unit=lun,fmt='(I6,A2,I6,A2,I3,A2,I3,A2,I3,A4)')  x_pix, ', ', y_pix, ', ', int(Ref_Lst%LR(j)%h(1)),&
                                                         ', ', int(Ref_Lst%LR(j)%h(2)), ', ', int(Ref_Lst%LR(j)%h(3)), ', <<'
                end if
                hkl_mask(y_pix,x_pix)=j
            end if
        end do
        close(lun)
    return
    end subroutine calc_mask_n_write_file

    subroutine read_angles_from_file(x_ang, y_ang, z_ang, ang_nam)
    real,         intent(out) :: x_ang,y_ang,z_ang
    Character(Len=512), intent(in)     :: ang_nam
    integer                   :: lun
        call Get_LogUnit(lun)
        open(unit=lun, file=ang_nam, action="read")
    !                        angles.dat
            read(unit=lun,fmt=*) x_ang,y_ang,z_ang
        close(lun)
    return
    end subroutine read_angles_from_file

    subroutine write_angles_into_file(x_ang, y_ang, z_ang, ang_nam)
    real,            intent(in) :: x_ang, y_ang, z_ang
    Character(Len=512), intent(in)     :: ang_nam
    integer                     :: lun

        call Get_LogUnit(lun)
        open(unit=lun, file=ang_nam, status="replace", action="write")
            write(unit=lun,fmt='(F7.2,A2,F7.2,A2,F7.2)') x_ang, ',', y_ang, ',', z_ang
            write(unit=lun,fmt=*) ""
        close(lun)

    return
    end subroutine write_angles_into_file

    subroutine read_pk_fnd_param_from_file()
        integer                 :: lun
        call Get_LogUnit(lun)
        open(unit=lun, file="pk_par.dat", action="read")
    !                        pk_par.dat
            read(unit=lun,fmt=*) dst_min, peak_ar, m_peak
        close(lun)
    return
    end subroutine read_pk_fnd_param_from_file

    subroutine read_orient_param_from_file(ori_nam)
        Character(Len=512), intent(in) :: ori_nam
        integer                 :: lun

        call Get_LogUnit(lun)
        open(unit=lun, file=ori_nam, action="read")
    !                        orient_par.dat
            read(unit=lun,fmt=*) From_X_Ang, From_Y_Ang, From_Z_Ang
            read(unit=lun,fmt=*) To_X_Ang, To_Y_Ang, To_Z_Ang
            read(unit=lun,fmt=*) x_step, y_step, z_step
            read(unit=lun,fmt=*) ini_resol

            write(unit=*,fmt=*) "From_X_Ang, From_Y_Ang, From_Z_Ang =", From_X_Ang, From_Y_Ang, From_Z_Ang
            write(unit=*,fmt=*) "To_X_Ang, To_Y_Ang, To_Z_Ang       =", To_X_Ang, To_Y_Ang, To_Z_Ang
            write(unit=*,fmt=*) "x_step, y_step, z_step             =", x_step, y_step, z_step
            write(unit=*,fmt=*) "ini_resol                          =", ini_resol
        close(lun)
    return
    end subroutine read_orient_param_from_file

    subroutine CLI_Find_Peak()
            real        ::  xmin,ymin,xmax,ymax
            xmin = lbound(Data2D,2)
            ymin = lbound(Data2D,1)
            xmax = ubound(Data2D,2)
            ymax = ubound(Data2D,1)
            call read_pk_fnd_param_from_file()
            Fnd_param%d_min = dst_min
            Fnd_param%pk_ar = peak_ar
            Fnd_param%m_pk = m_peak
            write(*,*) "P find Par =", Fnd_param
            write(unit=*,fmt="(a,4f8.1)") " => Calling Peak_Find with x-y range: ",xmin,ymin,xmax,ymax
!!!!!!!!!!!!!!!!!Peak_Find(xmin,ymin,xmax,ymax,Data2D,Mask2D,ExclR,Fnd_par,Peaks)
            call Peak_Find(xmin,ymin,xmax,ymax,Data2D(:,:,1),Mask2D(:,:,1),ExclR,Fnd_param,Peak_List(1))
!            call Arrange_PeakList(Data2D(:,:,1),Mask2D(:,:,1),Peak_List(1)%Peak,Peak_List(1)%N_Peaks,Fnd_param)
    return

    end subroutine  CLI_Find_Peak

    subroutine write_file_w_experimental_pks()
        integer         :: lun, col,row
        call Get_LogUnit(lun)
        open(unit=lun, file="exp_pk.dat", status="replace", action="write")
        do col = 1, Ncol
            do row = 1, Nrow
                if( Mask2D(row,col,1) == 1)then
                    write(unit=lun,fmt='(I6,A2,I6)')  col, ', ', row
                end if
            end do
        end do
        close(lun)
    return
    end subroutine write_file_w_experimental_pks

    subroutine read_experimental_pks_from_file(pk_nam)
        Character(Len=512), intent(in)     :: pk_nam
        integer             :: lun, col,row, err
        character(len=210)  :: line
        call Get_LogUnit(lun)
        open(unit=lun, file=pk_nam, action="read")
        Mask2D(:,:,1) = 0
        do
            read(unit=lun,fmt="(a)",iostat=err) line
            if( err /= 0 )then
                exit
            end if
            read(unit=line,fmt=*) col, row
            Mask2D(row,col,1) = 1
        end do

        close(lun)
    return
    end subroutine read_experimental_pks_from_file

    subroutine show_peacks_console()
        integer         :: tot, col,row
        tot = 0
        do col = 1, Ncol
            do row = 1, Nrow
                if( Mask2D(row,col,1) == 1)then
                    write(*,*) "( X, Y) =", col, row
                    tot = tot + 1
                end if
            end do
        end do
        write(*,*) "Num of Peaks", tot

    return
    end subroutine show_peacks_console

    ! CALL THE ORIGINAL ORIENTATION FUNCTION OF LUISO
    ! WARNING LUISO ORIGINAL CODE CONTAINS ERRORS, see Laue_Sim_Mod => F_A_Orient
    subroutine CLI_orient(a_x_r, a_y_r, a_z_r,ori_nam)
        real,                        intent (out)   :: a_x_r, a_y_r, a_z_r
        Character(Len=512),          intent(in)     :: ori_nam
        Type (Orient_angl_cond_type)                :: orien_cond                 ! orienting parameteres

        call read_orient_param_from_file(ori_nam)
        orien_cond%xfrom = From_X_Ang
        orien_cond%yfrom = From_Y_Ang
        orien_cond%zfrom = From_Z_Ang

        orien_cond%xto = To_X_Ang
        orien_cond%yto = To_Y_Ang
        orien_cond%zto = To_Z_Ang

        orien_cond%xstep = x_step
        orien_cond%ystep = y_step
        orien_cond%zstep = z_step
        orien_cond%resl  = ini_resol

        call F_A_Orient(orien_cond,a_x_r,a_y_r,a_z_r)


           !     ang_in(1)=a_x_r
           !     ang_in(2)=a_y_r
           !     ang_in(3)=a_z_r
    return
    end subroutine CLI_orient

    !!============PERCEVAL SUBROUTINES=================

    subroutine update_instrument_params(dist,tiltx,tilty,tiltz,resol)

        real, intent(in) :: dist,tiltx,tilty,tiltz,resol


        !write(*,*) "detector distance",LaueDiff%D
        !write(*,*) "detector tilts",LaueDiff%Tiltx_d,LaueDiff%Tilty_d,LaueDiff%Tiltz_d
        !write(*,*) "detector resolution",LaueDiff%d_min

        LaueDiff%D       = dist
        LaueDiff%Tiltx_d = tiltx
        LaueDiff%Tilty_d = tilty
        LaueDiff%Tiltz_d = tiltz
        LaueDiff%d_min   = resol

        !write(*,*) "detector distance",LaueDiff%D
        !write(*,*) "detector tilts",LaueDiff%Tiltx_d,LaueDiff%Tilty_d,LaueDiff%Tiltz_d
        !write(*,*) "detector resolution",LaueDiff%d_min

        call Calc_RD_rOD(LaueDiff)

    end subroutine update_instrument_params


    subroutine find_hkl(hval,kval,lval, an_x,an_y,an_z)

        !! function that returns the phi_angles (alpha, beta, gamma)
        !! in order to bring a given HKL to the center of the detecor

        integer, intent(in)    :: hval,kval,lval
        real   , intent(out)   :: an_x,an_y,an_z

        real   , dimension(3)       :: u_mask, r_mask, tmp_h, u, axis
        real   , Dimension(1:3,1:3) :: tmp_guimat, U_mat, Rl, Rot_mat

        real      :: px,py,cx,cy,rad_ang
        logical   :: ok

        !! ======== We want to bring a HKL to the center =================
        !! ==== to do this, we find the center vector, then the HKL vector ====
        !! ==== we measure the alpha angle between these 2 vectors =================
        !! ==== then we turn alpha around the vector perpendicular to the 2 vectors ==

        !! ============= 1) OBTAINING THE CENTER VECTOR ==================
        !! px and py are pix coordinates in the detector
        !! here px, py = center of the detector

        !px = real(Ncol)/2.
        !py = real(Nrow)/2.

        px = real(LaueDiff%xo)
        py = real(LaueDiff%zo)

        !! we transform px, py in millimeter => cx,cy
        call Pix_To_Mil(LaueDiff,px,py,cx,cy)


        !! get the vector of spot at pos x, z (detector) in Lab system => u_mask
        !! here 'spot at pos x, z' is the center cx, cy
        !! => we get the vector perpendicular to the center of the dector
        !! => u_mask in Lab system
        call Get_Scattered_wavevector_from_xz(LaueDiff, cx, cy, u_mask, ok)


        !! get the reciprocal vector of a wave vector in Lab sys => r_mask
        !! computation of the reciprocal vector of u_mask
        call Get_Reciprocal_vector_from_Scattered_wavevector(u_mask, r_mask)



        !! ======= 2) OBTAINING THE HKL VECTOR ==================
        !! Initialize the rotation matrix
        call set_rotation_matrix( (/0.0, 0.0, 0.0/),  Rot_mat)
        !! multiplication of the burp matrix by the Busing-Levy B-matrix
        Rl = matmul(Rot_mat, BL)


        !! then we choose a HKL => tmp_h => u
        tmp_h = real((/hval, kval, lval/))
        !! Cartesian coordinates of tmp_h in the Lab system
        u = matmul(Rl, tmp_h)


        !! ======= 3) OBTAINING THE ANGLE BETWEEN THE CENTER AND HKL VECTORS =======
        call calc_angle_betwen_vectors(u, r_mask, rad_ang)


        !! ======= 4) ROTATION AROUND THE VECTOR PERPENDICULAR TO THE 2 VECTORS ===
        axis = Cross_Product(u, r_mask)
        call set_rotation_matrix( (/0.0, 0.0, 0.0/),  U_mat)
        call rotate_around_a_vector(axis, rad_ang, U_mat)


        !! ======= 5) AT THE END WE RETURN THE ANGLES OF THE CRYSTAL
        call Get_Phi_xyz(U_mat,an_x,an_y,an_z,"deg")



    end subroutine find_hkl

    subroutine move_hkl(ipx,ipy, hval,kval,lval, an_x,an_y,an_z)

        integer, intent(in)    :: ipx,ipy
        integer, intent(in)    :: hval,kval,lval
        real   , intent(out)   :: an_x,an_y,an_z

        real   , dimension(3)       :: u_mask, r_mask, tmp_h, u, axis
        real   , Dimension(1:3,1:3) :: tmp_guimat, U_mat, Rl, Rot_mat

        real      :: px,py,cx,cy,rad_ang
        logical   :: ok

        !!============= 1) OBTENTION DU VECTEUR POSITION DETECTOR =================
        !! px et py sont des coords en pix ds le detector
        px = real(ipx)
        py = real(ipy)

        call Pix_To_Mil(LaueDiff,px,py,cx,cy)
        call Get_Scattered_wavevector_from_xz(LaueDiff, cx, cy, u_mask, ok)
        call Get_Reciprocal_vector_from_Scattered_wavevector(u_mask, r_mask)

        !!======= 2) OBTENTION DU VECTEUR HKL =================
        call set_rotation_matrix( (/0.0, 0.0, 0.0/),  Rot_mat)
        Rl = matmul(Rot_mat, BL)
        tmp_h = real((/hval, kval, lval/))
        u = matmul(Rl, tmp_h)

        !!======= 3) OBTENTION DE L ANGLE ENTRE LES 2 VECTEURS =======
        call calc_angle_betwen_vectors(u, r_mask, rad_ang)

        !!======= 4) ROTATION AUTOUR DU VECTEUR PERPENDICULAIRE AUX 2 VECTEURS ===
        axis = Cross_Product(u, r_mask)
        call set_rotation_matrix( (/0.0, 0.0, 0.0/),  U_mat)
        call rotate_around_a_vector(axis, rad_ang, U_mat)

        !!======= 5) A LA FIN ON RETOURNE LES ANGLES DU CRYSTAL
        call Get_Phi_xyz(U_mat,an_x,an_y,an_z,"deg")


    end subroutine move_hkl


    subroutine rotate_around_hkl(dang, hval,kval,lval, an_x,an_y,an_z)
        real,       intent(in)    :: dang
        integer,    intent(in)    :: hval,kval,lval

        real                   :: xpc, zpc
        real                   :: x_mm, z_mm
        real, dimension(3)     :: u_mask, r_mask, tmp_h, u, axis
        logical                :: ok

        real, Dimension(1:3,1:3) :: tmp_guimat, U_mat, Rl, Rot_mat
        real                     :: rad_ang_grr
        real, intent(out)        :: an_x,an_y,an_z

        !xpc = real(Ncol)/2.
        !zpc = real(Nrow)/2.

        xpc = real(LaueDiff%xo)
        zpc = real(LaueDiff%zo)

        call Pix_To_Mil(LaueDiff,xpc,zpc,x_mm,z_mm)

        !! get the vector of spot at pos x,z (detector) in Lab system => u_mask
        call Get_Scattered_wavevector_from_xz(LaueDiff, x_mm, z_mm, u_mask, ok)

        !! get the reciprocal vector of a wave vector in Lab sys => r_mask
        call Get_Reciprocal_vector_from_Scattered_wavevector(u_mask, r_mask)

        !! Initialize the rotation matrix
        call set_rotation_matrix( (/0.0, 0.0, 0.0/),  Rot_mat)
        Rl=matmul(Rot_mat, BL)


        tmp_h= real((/hval, kval, lval/))

        u=matmul(Rl, tmp_h) !Cartesian coordinates of h in the Lab system

        !! Initialize U_mat
        call set_rotation_matrix( (/0.0, 0.0, 0.0/),  U_mat)
        !! Calc angle between u and r_mask
        call calc_angle_betwen_vectors(u, r_mask, rad_ang_grr)
        axis = Cross_Product(u, r_mask)
        call rotate_around_a_vector(axis, rad_ang_grr, U_mat)


        rad_ang_grr = dang*pi/180. !-pi/4.0

        tmp_guimat = U_mat
        call rotate_around_a_vector(r_mask,rad_ang_grr,tmp_guimat)
        call Get_Phi_xyz(tmp_guimat,an_x,an_y,an_z,"deg")

    end subroutine rotate_around_hkl



    subroutine rotate_around_lab_vector(dang, vx,vy,vz, an_x,an_y,an_z)
        real,       intent(in)    :: dang
        integer,    intent(in)    :: vx,vy,vz

        real, dimension(3)        :: rot_vect

        real, Dimension(1:3,1:3)  :: U_mat
        real                      :: rad_ang
        real, intent(in out)         :: an_x,an_y,an_z



        !! ROT_VECTOR IS GIVEN IN LAB SYS
        rot_vect = real((/vx, vy, vz/))

        !! PERFORM THE ROTATION AROUND THE VECTOR
        rad_ang = dang*pi/180.
        call set_rotation_matrix( (/an_x, an_y, an_z/),  U_mat) !! force the pattern to start with current orientation
        call rotate_around_a_vector(rot_vect,rad_ang,U_mat)     !! rotate around the vector

        !! OBTAIN THE NEW ORIENTATION ANGLES
        call Get_Phi_xyz(U_mat,an_x,an_y,an_z,"deg")

    end subroutine rotate_around_lab_vector

    subroutine rotate_around_recip_vector(dang, vx,vy,vz, an_x,an_y,an_z)
        real,       intent(in)    :: dang
        integer,    intent(in)    :: vx,vy,vz

        real, dimension(3)     :: rot_vect, u

        real, Dimension(1:3,1:3) :: U_mat, Rl, Rot_mat
        real                     :: rad_ang
        real, intent(in out)        :: an_x,an_y,an_z



        !! EXPRESS THE ROT_VECTOR IN LAB SYS FROM CURRENT ORIENTATION
        rot_vect = real((/vx, vy, vz/))
        call set_rotation_matrix( (/an_x, an_y, an_z/),  Rot_mat) !! force the pattern to start with current orientation
        Rl=matmul(Rot_mat, BL)
        u=matmul(Rl, rot_vect) !Cartesian coordinates of rot_vect


        !! PERFORM THE ROTATION AROUND THE VECTOR
        rad_ang = dang*pi/180.
        call set_rotation_matrix( (/an_x, an_y, an_z/),  U_mat) !! force the pattern to start with current orientation
        call rotate_around_a_vector(u,rad_ang,U_mat)     !! rotate around the vector

        !! OBTAIN THE NEW ORIENTATION ANGLES
        call Get_Phi_xyz(U_mat,an_x,an_y,an_z,"deg")

    end subroutine rotate_around_recip_vector


    subroutine rotate_around_detector_pos(dang, px,py, an_x,an_y,an_z)
        real,       intent(in)    :: dang
        integer,    intent(in)    :: px,py
        real                      :: x1,y1
        real, dimension(3)        :: u1,r1,vect,axis

        logical                   :: ok

        real, Dimension(1:3,1:3)  :: U_mat
        real                      :: rad_ang
        real, intent(in out)         :: an_x,an_y,an_z


        call Pix_To_Mil(LaueDiff,real(px),real(py),x1,y1)
        call Get_Scattered_wavevector_from_xz(LaueDiff, x1, y1, u1, ok)
        call Get_Reciprocal_vector_from_Scattered_wavevector(u1, r1)

        !vect = real((/0, 1, 0/))
        !axis = Cross_Product(vect, r1)

        !! PERFORM THE ROTATION AROUND THE VECTOR
        rad_ang = dang*pi/180.
        call set_rotation_matrix( (/an_x, an_y, an_z/),  U_mat) !! force the pattern to start with current orientation
        call rotate_around_a_vector(r1,rad_ang,U_mat)     !! rotate around the vector

        !! OBTAIN THE NEW ORIENTATION ANGLES
        call Get_Phi_xyz(U_mat,an_x,an_y,an_z,"deg")

    end subroutine rotate_around_detector_pos

    subroutine move_in_detector_plan(px1,py1, px2,py2, an_x,an_y,an_z) !bind(c, name='move_in_detector_plan')
        integer,    intent(in)    :: px1,py1,px2,py2
        real,       intent(in out)   :: an_x,an_y,an_z

        real                      :: x1,y1,x2,y2
        real, dimension(3)        :: u1, r1, u2, r2, axis
        logical                   :: ok

        real, Dimension(1:3,1:3)  :: U_mat
        real                      :: rad_ang


        call Pix_To_Mil(LaueDiff,real(px1),real(py1),x1,y1)
        call Pix_To_Mil(LaueDiff,real(px2),real(py2),x2,y2)

        !! get the vector of spot at pos x,z (detector) in Lab system => u1,u2
        call Get_Scattered_wavevector_from_xz(LaueDiff, x1, y1, u1, ok)
        call Get_Scattered_wavevector_from_xz(LaueDiff, x2, y2, u2, ok)

        !! get the reciprocal vector of a wave vector in Lab sys => r1,r2
        call Get_Reciprocal_vector_from_Scattered_wavevector(u1, r1)
        call Get_Reciprocal_vector_from_Scattered_wavevector(u2, r2)

        !! Initialize the rotation matrix
        call set_rotation_matrix( (/an_x, an_y, an_z/),  U_mat) !! force the pattern to start with current orientation


        call calc_angle_betwen_vectors(r1, r2, rad_ang)
        axis = Cross_Product(r1, r2)

        call rotate_around_a_vector(axis, rad_ang, U_mat)

        call Get_Phi_xyz(U_mat,an_x,an_y,an_z,"deg")


    end subroutine move_in_detector_plan


    subroutine get_number_of_theo_spots(spotnum) bind(c, name='get_number_of_theo_spots')
        integer, intent(out)  :: spotnum

        spotnum = Ref_Lst%nref

    end subroutine get_number_of_theo_spots

    subroutine get_theo_spots(spot_list, spotlen) bind(c, name='get_theo_spots')
        real            :: x_pc,y_pc, x_milm, z_milm
        integer         :: j,idx
        integer         :: x_pix,y_pix
        logical         :: in_img

        integer(c_int), intent(in) :: spotlen
        integer(c_int), intent(out):: spot_list(spotlen)


            hkl_mask=0
            do j=1,Ref_Lst%nref
                x_milm=Ref_Lst%LR(j)%x
                z_milm=Ref_Lst%LR(j)%z

                call mil_to_pix(LaueDiff,x_milm,z_milm,x_pc,y_pc)
                x_pix=nint(x_pc)
                y_pix=nint(y_pc)
                in_img = .true.
                if( x_pix>Ncol )then
                    x_pix = Ncol
                    in_img = .false.
                end if
                if( y_pix>Nrow )then
                    y_pix = Nrow
                    in_img = .false.
                end if

                if( x_pix<1 )then
                    x_pix = 1
                    in_img = .false.
                end if
                if( y_pix<1 )then
                    y_pix = 1
                    in_img = .false.
                end if
                if (in_img ) then

                    idx = (j-1)*6 + 1
                    spot_list(idx) = x_pix
                    spot_list(idx+1) = y_pix
                    spot_list(idx+2) = int(Ref_Lst%LR(j)%h(1))
                    spot_list(idx+3) = int(Ref_Lst%LR(j)%h(2))
                    spot_list(idx+4) = int(Ref_Lst%LR(j)%h(3))
                    spot_list(idx+5) = hkl_mask(y_pix, x_pix)

                    hkl_mask(y_pix,x_pix)=j
                end if
            end do

    end subroutine get_theo_spots


    subroutine set_experimental_pks(peaks,peak_len) bind(c, name='set_experimental_pks')
        integer         :: col,row
        integer         :: j,idx

        integer(c_int), intent(in) :: peak_len
        integer(c_int), intent(in) :: peaks(peak_len)

        Mask2D(:,:,1) = 0

        do j=1,peak_len,2
            col = peaks(j)
            row = peaks(j+1)
            Mask2D(row,col,1) = 1
        end do

    end subroutine set_experimental_pks


    subroutine psl_find_best_match(condt,f_an_x,f_an_y,f_an_z,maxconv,Mask_Expr,Mask_Calc,Mask_Sum,tmp_Ncol,tmp_Nrow)

        Type (psl_orient_params), intent(in out) :: condt
        real,                     intent(in out) :: f_an_x,f_an_y,f_an_z
        integer,                  intent(out)    :: maxconv

        integer, Dimension(:,:),  Allocatable, intent(in out) :: Mask_Calc,Mask_Expr,Mask_Sum
        integer                              , intent(in out) :: tmp_Ncol,tmp_Nrow

        integer                                  :: convg
        integer                                  :: Nm_Ex_Ps
        integer                                  :: resol
        real                                     :: an_x,an_y,an_z


        !write(*,*) "Search orient: resolution = ", condt%resl
        !write(*,*) "alpha = [", condt%xfrom, ",", condt%xto ,"]", "step", condt%xstep
        !write(*,*) "beta  = [", condt%yfrom, ",", condt%yto ,"]", "step", condt%ystep
        !write(*,*) "gamma = [", condt%zfrom, ",", condt%zto ,"]", "step", condt%zstep

        resol=condt%resl

        maxconv = 0

        an_x =condt%xfrom
        an_y =condt%yfrom
        an_z =condt%zfrom

        do while( an_x <= condt%xto  )

            do while( an_y <= condt%yto  )

                do while( an_z <= condt%zto  )

                    call Calc_and_Dif(an_x,an_y,an_z,Mask_Expr,Mask_Calc,Mask_Sum,tmp_Ncol,tmp_Nrow,resol,convg,Nm_Ex_Ps)

                    if( convg > maxconv )then
                        maxconv=convg
                        f_an_x=an_x
                        f_an_y=an_y
                        f_an_z=an_z

                        !write(*,*) f_an_x,f_an_y,f_an_z

                        if( convg == Nm_Ex_Ps )then
                            write(*,*) "EXIT ON MAX CONVERGENCE"

                            an_z = condt%zto
                            an_y = condt%yto
                            an_x = condt%xto

                        end if

                    end if

                    an_z = an_z + condt%zstep

                end do

                an_z =condt%zfrom
                an_y = an_y + condt%ystep

            end do

            an_y =condt%yfrom
            an_x = an_x + condt%xstep

        end do

        !write(unit=*,fmt="(2(a,i6))") ' convergence = ',maxconv, '/', Nm_Ex_Ps
        !write(unit=*,fmt="(a,3f9.4)") ' angles(x,y,z) =',f_an_x,f_an_y,f_an_z
        !write(*,*) "_________________________________________"

        maxconv = int(100*maxconv/real(Nm_Ex_Ps))

    end subroutine psl_find_best_match

    subroutine psl_search_orient(x0,y0,z0, x1,y1,z1, xs,ys,zs, resol, an_x,an_y,an_z, maxconv)

        real,    intent(in)         :: x0,y0,z0
        real,    intent(in)         :: x1,y1,z1
        real,    intent(in)         :: xs,ys,zs
        integer, intent(in)         :: resol

        real,    intent (out)       :: an_x, an_y, an_z
        integer, intent (out)       :: maxconv

        Type (psl_orient_params)    :: orien_cond

        integer, Dimension(:,:),  Allocatable    :: Mask_Calc,Mask_Expr,Mask_Sum
        integer                                  :: tmp_Ncol,tmp_Nrow

        orien_cond%xfrom = x0
        orien_cond%yfrom = y0
        orien_cond%zfrom = z0

        orien_cond%xto   = x1
        orien_cond%yto   = y1
        orien_cond%zto   = z1

        orien_cond%xstep = xs
        orien_cond%ystep = ys
        orien_cond%zstep = zs

        orien_cond%resl  = resol

        call Allocate_Pattern_Masks(Mask_Expr,Mask_Calc,Mask_Sum,tmp_Ncol,tmp_Nrow,resol)

        call psl_find_best_match(orien_cond,an_x,an_y,an_z,maxconv,Mask_Expr,Mask_Calc,Mask_Sum,tmp_Ncol,tmp_Nrow)

    end subroutine psl_search_orient

    subroutine psl_orient(x0,y0,z0, x1,y1,z1, xs,ys,zs, resol, an_x,an_y,an_z, maxconv, step_min)

        real,    intent(in)         :: x0,y0,z0
        real,    intent(in)         :: x1,y1,z1
        real,    intent(in)         :: xs,ys,zs
        integer, intent(in out)     :: resol

        real,    intent(out)        :: an_x, an_y, an_z
        integer, intent(out)        :: maxconv
        real,    intent(in out)     :: step_min

        Type (psl_orient_params)    :: orien_cond
        integer                     :: iter,subiter
        real                        :: step,acc


        integer, Dimension(:,:),  Allocatable    :: Mask_Calc,Mask_Expr,Mask_Sum
        integer                                  :: tmp_Ncol,tmp_Nrow

        if (step_min < 0.01) step_min = 0.01

        ! ================================== PHASE 1 ===================================

        !write(*,*) ""
        !write(*,*) "======== PHASE 1 ================="

        orien_cond%xfrom = x0
        orien_cond%yfrom = y0
        orien_cond%zfrom = z0

        orien_cond%xto   = x1
        orien_cond%yto   = y1
        orien_cond%zto   = z1

        orien_cond%xstep = xs
        orien_cond%ystep = xs !ys => FORCE SAME STEP SIZE FOR X Y Z
        orien_cond%zstep = xs !zs => FORCE SAME STEP SIZE FOR X Y Z

        orien_cond%resl  = resol

        call Allocate_Pattern_Masks(Mask_Expr,Mask_Calc,Mask_Sum,tmp_Ncol,tmp_Nrow,resol)

        call psl_find_best_match(orien_cond,an_x,an_y,an_z,maxconv,Mask_Expr,Mask_Calc,Mask_Sum,tmp_Ncol,tmp_Nrow)



        ! ================================== PHASE 2 ===================================

        !write(*,*) ""
        !write(*,*) "======== PHASE 2 ================="



        !! =>  WITH X_STEP = Y_STEP = Z_STEP


        orien_cond%xfrom = an_x - orien_cond%xstep
        orien_cond%yfrom = an_y - orien_cond%ystep
        orien_cond%zfrom = an_z - orien_cond%zstep

        orien_cond%xto   = an_x + orien_cond%xstep
        orien_cond%yto   = an_y + orien_cond%ystep
        orien_cond%zto   = an_z + orien_cond%zstep

        orien_cond%xstep = orien_cond%xstep/2.
        orien_cond%ystep = orien_cond%ystep/2.
        orien_cond%zstep = orien_cond%zstep/2.

        do while( orien_cond%xstep >= step_min  )

            call psl_find_best_match(orien_cond,an_x,an_y,an_z,maxconv,Mask_Expr,Mask_Calc,Mask_Sum,tmp_Ncol,tmp_Nrow)

            orien_cond%xfrom = an_x - orien_cond%xstep
            orien_cond%yfrom = an_y - orien_cond%ystep
            orien_cond%zfrom = an_z - orien_cond%zstep

            orien_cond%xto   = an_x + orien_cond%xstep
            orien_cond%yto   = an_y + orien_cond%ystep
            orien_cond%zto   = an_z + orien_cond%zstep

            orien_cond%xstep = orien_cond%xstep/2.
            orien_cond%ystep = orien_cond%ystep/2.
            orien_cond%zstep = orien_cond%zstep/2.


        end do


        write(*,*) "BEST MATCH:",  maxconv ,"%"
        write(unit=*,fmt="(a,3f9.4)") ' angles',an_x, an_y, an_z


    end subroutine psl_orient

    subroutine psl_orient_nodal(px,py, hval,kval,lval, resol, an_x,an_y,an_z, maxconv, rotrange, stepsize, stepmin)

        integer, intent(in)     :: px,py
        integer, intent(in)     :: hval,kval,lval
        integer, intent(in)     :: resol
        real,    intent(out)    :: an_x,an_y,an_z
        integer, intent(out)    :: maxconv

        real,    intent(in)     :: rotrange, stepsize, stepmin


        integer, Dimension(:,:),  Allocatable    :: Mask_Calc,Mask_Expr,Mask_Sum
        integer                                  :: tmp_Ncol,tmp_Nrow
        integer                                  :: convg
        integer                                  :: Nm_Ex_Ps
        real                                     :: f_an_x,f_an_y,f_an_z
        real                                     :: p_an_x,p_an_y,p_an_z

        real       :: dang, f_dang, step, dang_start, dang_stop, step_min

        ! 1) BRING HKL SPOT TO THE DETECTOR POSITION PX,PY

        call move_hkl(px,py, hval,kval,lval, an_x,an_y,an_z)
        p_an_x = an_x
        p_an_y = an_y
        p_an_z = an_z

        ! 2) SEARCH ROTATING AROUND THE HKL SPOT AT PX,PY

        call Allocate_Pattern_Masks(Mask_Expr,Mask_Calc,Mask_Sum,tmp_Ncol,tmp_Nrow,resol)


        maxconv = 0
        dang_start = 0.
        step = stepsize
        step_min = stepmin
        dang_stop = rotrange

        do while(step>step_min)

            dang = dang_start
            f_dang = 0.0
            maxconv = 0

            do while( dang < dang_stop  )

                an_x=p_an_x
                an_y=p_an_y
                an_z=p_an_z

                call rotate_around_detector_pos(dang, px,py, an_x,an_y,an_z)

                call Calc_and_Dif(an_x,an_y,an_z,Mask_Expr,Mask_Calc,Mask_Sum,tmp_Ncol,tmp_Nrow,resol,convg,Nm_Ex_Ps)

                !write(*,*) "dang", dang, convg, step

                if( convg > maxconv )then
                    maxconv=convg
                    f_an_x=an_x
                    f_an_y=an_y
                    f_an_z=an_z
                    f_dang = dang


                    if( convg == Nm_Ex_Ps )then
                        write(*,*) "EXIT ON MAX CONVERGENCE"
                        exit
                    end if

                end if

                dang = dang + step

            end do

            dang_start = f_dang - step
            dang_stop  = f_dang + step
            step = step/2.

        end do

        an_x = f_an_x
        an_y = f_an_y
        an_z = f_an_z

        maxconv = int(100*maxconv/real(Nm_Ex_Ps))

        write(*,*) "BEST MATCH:",  maxconv ,"%"
        write(unit=*,fmt="(a,3f9.4)") ' angles',an_x, an_y, an_z


    end subroutine psl_orient_nodal


    subroutine psl_orient_exe(a_x_r, a_y_r, a_z_r,ori_nam)
        real,                        intent (out)   :: a_x_r, a_y_r, a_z_r
        Character(Len=512),          intent(in)     :: ori_nam
        integer                     :: maxconv
        real                        :: step_min = 0.03

        call read_orient_param_from_file(ori_nam)
        call psl_orient(real(From_X_Ang),real(From_Y_Ang),real(From_Z_Ang), &
                        real(To_X_Ang),real(To_Y_Ang),real(To_Z_Ang), &
                        real(x_step),real(y_step),real(z_step), &
                        ini_resol, a_x_r,a_y_r,a_z_r, &
                        maxconv, step_min)

        write(unit=*,fmt="(a)")       '  *********************************************************************'
        write(unit=*,fmt="(a,i6)")    '  Final_resol =', ini_resol
        write(unit=*,fmt="(a,i6)")    '  Match_rate =', maxconv
        write(unit=*,fmt="(a,3f10.4)") '  Best_angles(x,y,z) =',a_x_r,a_y_r,a_z_r
        write(unit=*,fmt="(a)")       '  *********************************************************************'


    end subroutine psl_orient_exe

    subroutine psl_orient_old(x0,y0,z0, x1,y1,z1, xs,ys,zs, resol, an_x,an_y,an_z, maxconv)

        real,    intent(in)         :: x0,y0,z0
        real,    intent(in)         :: x1,y1,z1
        real,    intent(in)         :: xs,ys,zs
        integer, intent(in out)     :: resol

        real,    intent (out)       :: an_x, an_y, an_z
        integer, intent (out)       :: maxconv

        Type (psl_orient_params)    :: orien_cond
        integer                     :: iter,subiter
        real                        :: step,acc


        integer, Dimension(:,:),  Allocatable    :: Mask_Calc,Mask_Expr,Mask_Sum
        integer                                  :: tmp_Ncol,tmp_Nrow

        ! ================================== PHASE 1 ===================================

        !write(*,*) ""
        !write(*,*) "======== PHASE 1 ================="

        orien_cond%xfrom = x0
        orien_cond%yfrom = y0
        orien_cond%zfrom = z0

        orien_cond%xto   = x1
        orien_cond%yto   = y1
        orien_cond%zto   = z1

        orien_cond%xstep = xs
        orien_cond%ystep = ys
        orien_cond%zstep = zs

        orien_cond%resl  = resol

        call Allocate_Pattern_Masks(Mask_Expr,Mask_Calc,Mask_Sum,tmp_Ncol,tmp_Nrow,resol)

        call psl_find_best_match(orien_cond,an_x,an_y,an_z,maxconv,Mask_Expr,Mask_Calc,Mask_Sum,tmp_Ncol,tmp_Nrow)



        ! ================================== PHASE 2 ===================================

        !write(*,*) ""
        !write(*,*) "======== PHASE 2 ================="


        step=.1
        do iter=1,10

            orien_cond%xstep = step
            orien_cond%ystep = step
            orien_cond%zstep = step

            orien_cond%resl  = resol

            if (2>1) then
                !write(*,*) "DOING SEARCH ON EACH AXIS ONE BY ONE "
                do subiter=1,3
                    ! PHASE 2a

                    orien_cond%xfrom = an_x - 20.*step
                    orien_cond%yfrom = an_y !- 20.*step
                    orien_cond%zfrom = an_z !- 20.*step

                    orien_cond%xto = an_x + 20.*step
                    orien_cond%yto = an_y !+ 20.*step
                    orien_cond%zto = an_z !+ 20.*step

                    call psl_find_best_match(orien_cond,an_x,an_y,an_z,maxconv,Mask_Expr,Mask_Calc,Mask_Sum,tmp_Ncol,tmp_Nrow)

                    ! PHASE 2b

                    orien_cond%xfrom = an_x !- 20.*step
                    orien_cond%yfrom = an_y - 20.*step
                    orien_cond%zfrom = an_z !- 20.*step

                    orien_cond%xto = an_x !+ 20.*step
                    orien_cond%yto = an_y + 20.*step
                    orien_cond%zto = an_z !+ 20.*step

                    call psl_find_best_match(orien_cond,an_x,an_y,an_z,maxconv,Mask_Expr,Mask_Calc,Mask_Sum,tmp_Ncol,tmp_Nrow)

                    ! PHASE 2c

                    orien_cond%xfrom = an_x !- 20.*step
                    orien_cond%yfrom = an_y !- 20.*step
                    orien_cond%zfrom = an_z - 20.*step

                    orien_cond%xto = an_x !+ 20.*step
                    orien_cond%yto = an_y !+ 20.*step
                    orien_cond%zto = an_z + 20.*step

                    call psl_find_best_match(orien_cond,an_x,an_y,an_z,maxconv,Mask_Expr,Mask_Calc,Mask_Sum,tmp_Ncol,tmp_Nrow)

                end do

            else

                acc = 5.

                orien_cond%xfrom = an_x - acc*step
                orien_cond%yfrom = an_y - acc*step
                orien_cond%zfrom = an_z - acc*step

                orien_cond%xto = an_x + acc*step
                orien_cond%yto = an_y + acc*step
                orien_cond%zto = an_z + acc*step

                call psl_find_best_match(orien_cond,an_x,an_y,an_z,maxconv,Mask_Expr,Mask_Calc,Mask_Sum,tmp_Ncol,tmp_Nrow)

            end if

            step=step/2
            if( step < 0.01 )then
                !step = 0.01
                exit
            end if

            resol=resol-1
            if( resol < 2 )then
                !resol = 2
                !write(*,*) "Redundant"
                exit

            end if

            call Allocate_Pattern_Masks(Mask_Expr,Mask_Calc,Mask_Sum,tmp_Ncol,tmp_Nrow,resol)

        end do



        write(*,*) "BEST MATCH:",  maxconv ,"%", "resol", resol, "step", step
        write(unit=*,fmt="(a,3f9.4)") ' angles',an_x, an_y, an_z




    end subroutine psl_orient_old



    subroutine elementwise( arr1, arr2, arr3, dim_h, dim_w ) bind(c, name='elementwise')


        integer(c_int),intent(in) :: dim_h, dim_w
        real(c_float), intent(in) :: arr1(dim_h, dim_w), arr2(dim_h, dim_w)
        real(c_float), intent(out):: arr3(dim_h, dim_w)

        integer :: i,j

        forall (i=1:dim_h,j=1:dim_w)
            arr3(i,j) = arr1(i,j) * arr2(i,j)
        end forall


    end subroutine elementwise

    subroutine rotate_around_vector_test(dang, hval,kval,lval, an_x,an_y,an_z)
        real,       intent(in)    :: dang
        integer,    intent(in)    :: hval,kval,lval

        real                   :: xpc, zpc
        real                   :: x_mm, z_mm
        real, dimension(3)     :: u_mask, r_mask, tmp_h, u, axis
        logical                :: ok

        real, Dimension(1:3,1:3) :: tmp_guimat, U_mat, Rl, Rot_mat
        real                     :: rad_ang_grr
        real, intent(in out)        :: an_x,an_y,an_z

        !Real,    Dimension(3,3)             :: MRUB
        !integer                  :: i


        !xpc = real(Ncol)/2.
        !zpc = real(Nrow)/2.

        xpc = real(LaueDiff%xo)
        zpc = real(LaueDiff%zo)

        !!write (*,*) 'Ncol,Nrow',Ncol,Nrow  !! = image (w,h)

        !!write(*,*) 'xpc, zpc', xpc,zpc

        call Pix_To_Mil(LaueDiff,xpc,zpc,x_mm,z_mm)

        !!======Cf F_A_Orient_1:  PhASE 1 for each Nodal spot at pos x_mm, z_mm ==========
        !!  pour un nodal a la position ds le detecteur x_mm, z_mm
        !!  on initialise u_mask => r_mask => Rl


        !! get the vector of spot at pos x,z (detector) in Lab system => u_mask
        call Get_Scattered_wavevector_from_xz(LaueDiff, x_mm, z_mm, u_mask, ok)
        !write(unit=*,fmt="(a,3f8.3)") ' wave_vector =', u_mask

        !! get the reciprocal vector of a wave vector in Lab sys => r_mask
        call Get_Reciprocal_vector_from_Scattered_wavevector(u_mask, r_mask)
        !write(unit=*,fmt="(a,3f8.3)") ' reciproc_vector =', r_mask

        !! Initialize the rotation matrix
        !! Rot = Rx(ang(3)) . Ry(ang(2)) . Rz(ang(1))
        write(*,*) 'angles in', an_x,an_y,an_z
        call set_rotation_matrix( (/an_x, an_y, an_z/),  Rot_mat)
        !call set_rotation_matrix( (/0.0, 0.0, 0.0/),  Rot_mat)
        Rl=matmul(Rot_mat, BL)

        !write(*,*) 'BL = '
        !write(*,"(3f8.3)") transpose(Bl)

        !write(*,*) 'RL = '
        !write(*,"(3f8.3)") transpose(Rl)

        !!======Cf F_A_Orient_1:  PhASE 2 for hkl in [(H,K,L)....] ==========
        !! ensuite on choisi un HKL => tmp_h => u
        !! puis on calcul l angle entre u et r_mask => rad_ang_grr
        !! on calcul le vecteur perpendiculair a  u et r_mask => axis
        !! puis on tourne autour de axis d un angle rad_ang_grr => donc on aligne u et r_mask
        !!
        !! => hypothese => on drag le nodal vers le centre (= hkl=0 -1 0)


        tmp_h= real((/hval, kval, lval/))
        !write(unit=*,fmt="(a,3f8.3)") ' => Current H K L =', tmp_h

        u=matmul(Rl, tmp_h) !Cartesian coordinates of h in the Lab system
        !write(unit=*,fmt="(a,3f8.3)") 'HKL in Lab sys',u

        !! Initialize U_mat
        call set_rotation_matrix( (/an_x, an_y, an_z/),  U_mat) !! force the pattern to start with current orientation
        !call set_rotation_matrix( (/0.0, 0.0, 0.0/),  U_mat)
        !! Calc angle between u and r_mask
        call calc_angle_betwen_vectors(u, r_mask, rad_ang_grr)
        axis = Cross_Product(u, r_mask)

        !rad_ang_grr = dang*pi/180.
        !call rotate_around_a_vector(axis, rad_ang_grr, U_mat)

        !write(*,*) 'rad_ang_grr', rad_ang_grr

        !!======Cf F_A_Orient_1:  PhASE 3 Rotate around hkl  ==========
        !! finalement on tourne d un angle rad_ang_grr (valeur qqc, differnte de l etape precedente) autour de r_mask
        !!
        !! => hypothese => on tourne autour du Nodal ramene au centre (qd hkl=0 -1 0)

        rad_ang_grr = dang*pi/180.
        !write(*,*) 'rotate dang', dang, rad_ang_grr

        tmp_guimat = U_mat
        !call rotate_around_a_vector(r_mask,rad_ang_grr,tmp_guimat)  !! => rotate around center
        call rotate_around_a_vector(u,rad_ang_grr,tmp_guimat)     !! => donot rotate around center


        call Get_Phi_xyz(tmp_guimat,an_x,an_y,an_z,"deg")

        !write(*,*) 'angles out', an_x,an_y,an_z

        !write(unit=*,fmt="(a,3f8.3)") ' Curr_orient =', Curr_orient
        !write(unit=*,fmt="(a,3f8.3)") ' Curr_U_angles =', Curr_U_angles
        !write(unit=*,fmt="(a,3f8.3)") ' Saved_U_angles =', Saved_U_angles

        !write(*,*) ' img_spdl =',img_spdl
        !write(*,*) ' img_phi_ang =',img_phi_ang
        !write(*,*) ' img_Chi_ang =',img_Chi_ang
        !write(*,*) ' img_Gam_ang =',img_Gam_ang

        !write(*,*) 'Current_UB = '
        !write(*,"(3f8.3)") transpose(Current_UB)

        !write(*,*) 'Current_R = '
        !write(*,"(3f8.3)") transpose(Current_R)

        !write(*,*) 'Current_M = '
        !write(*,"(3f8.3)") transpose(Current_M)

        !write(*,*) 'Current_Rot = '
        !write(*,"(3f8.3)") transpose(Current_Rot)

        !write(*,*) 'Current_U = '
        !write(*,"(3f8.3)") transpose(Current_U)

        !write(*,*) 'BL = '
        !write(*,"(3f8.3)") transpose(BL)

        !write(*,*) 'Saved_UB = '
        !write(*,"(3f8.3)") transpose(Saved_UB)

        !! call set_rotation_matrix((/phi_x, phi_y, phi_z/),UM) ! UM here is the U matrix
        !MRUB=matmul(U_mat,BL)
        !write(unit=*,fmt="(a/3(10x,3(f9.4),/))") " => Recip-to-Lab matrix: ", (MRUB(i,:),i=1,3)



    return
    end subroutine rotate_around_vector_test

    subroutine rot_test(dang, hval,kval,lval, x_pix,z_pix, an_x,an_y,an_z)
        real,       intent(in)    :: dang
        integer,    intent(in)    :: hval,kval,lval
        real,    intent(in)    :: x_pix,z_pix
        real                   :: xpc, zpc
        real                   :: x_mm, z_mm
        real, dimension(3)     :: u_mask, r_mask, tmp_h, u, axis
        logical                :: ok

        real, Dimension(1:3,1:3) :: tmp_guimat, U_mat, Rl, Rot_mat
        real                     :: rad_ang_grr
        real, intent(out)        :: an_x,an_y,an_z

        xpc = real(x_pix)
        zpc = real(z_pix)

        write (*,*) 'Ncol,Nrow',Ncol,Nrow  !! = image (w,h)



        write(*,*) 'xpc, zpc', xpc,zpc
        call Pix_To_Mil(LaueDiff,xpc,zpc,x_mm,z_mm)

        !!======Cf F_A_Orient_1:  PhASE 1 for each Nodal spot at pos x_mm, z_mm ==========
        !!  pour un nodal a la position ds le detecteur x_mm, z_mm
        !!  on initialise u_mask => r_mask => Rl


        !! get the vector of spot at pos x,z (detector) in Lab system => u_mask
        call Get_Scattered_wavevector_from_xz(LaueDiff, x_mm, z_mm, u_mask, ok)
        write(unit=*,fmt="(a,3f8.3)") ' wave_vector =', u_mask

        !! get the reciprocal vector of a wave vector in Lab sys => r_mask
        call Get_Reciprocal_vector_from_Scattered_wavevector(u_mask, r_mask)
        write(unit=*,fmt="(a,3f8.3)") ' reciproc_vector =', r_mask

        !! Initialize the rotation matrix
        !! Rot = Rx(ang(3)) . Ry(ang(2)) . Rz(ang(1))
        call set_rotation_matrix( (/0.0, 0.0, 0.0/),  Rot_mat)
        Rl=matmul(Rot_mat, BL)
        !!write(unit=*,fmt="(a,3f8.3)") 'Rl',Rl(1,:)
        !!write(unit=*,fmt="(a,3f8.3)") 'Rl',Rl(2,:)
        !!write(unit=*,fmt="(a,3f8.3)") 'Rl',Rl(3,:)

        !write(*,*) 'BL = '
        !write(*,"(3f8.3)") transpose(Bl)

        write(*,*) 'RL = '
        write(*,"(3f8.3)") transpose(Rl)
        !!print "(3f8.3)", transpose(Rl)

        !!======Cf F_A_Orient_1:  PhASE 2 for hkl in [(H,K,L)....] ==========
        !! ensuite on choisi un HKL => tmp_h => u
        !! puis on calcul l angle entre u et r_mask => rad_ang_grr
        !! on calcul le vecteur perpendiculair a  u et r_mask => axis
        !! puis on tourne autour de axis d un angle rad_ang_grr => donc on aligne u et r_mask
        !!
        !! => hypothese => on drag le nodal vers le centre (= hkl=0 -1 0)


        tmp_h= real((/hval, kval, lval/))
        !tmp_h= real((/-1, -3, -1/))
        write(unit=*,fmt="(a,3f8.3)") ' => Current H K L =', tmp_h

        u=matmul(Rl, tmp_h) !Cartesian coordinates of h in the Lab system
        write(unit=*,fmt="(a,3f8.3)") 'HKL in Lab sys',u

        !! Initialize U_mat
        call set_rotation_matrix( (/0.0, 0.0, 0.0/),  U_mat)
        !! Calc angle between u and r_mask
        call calc_angle_betwen_vectors(u, r_mask, rad_ang_grr)
        axis = Cross_Product(u, r_mask)
        call rotate_around_a_vector(axis, rad_ang_grr, U_mat)

        write(*,*) 'rad_ang_grr', rad_ang_grr

        !!======Cf F_A_Orient_1:  PhASE 3 Rotate around hkl  ==========
        !! finalement on tourne d un angle rad_ang_grr (valeur qqc, differnte de l etape precedente) autour de r_mask
        !!
        !! => hypothese => on tourne autour du Nodal ramene au centre (qd hkl=0 -1 0)

        rad_ang_grr = dang*pi/180. !-pi/4.0
        write(*,*) 'rotate dang', dang, rad_ang_grr

        tmp_guimat = U_mat
        call rotate_around_a_vector(r_mask,rad_ang_grr,tmp_guimat)
        call Get_Phi_xyz(tmp_guimat,an_x,an_y,an_z,"deg")

        write(*,*) 'angles', an_x,an_y,an_z

    return
    end subroutine rot_test

    subroutine print_test(num)

        integer, intent(in)   :: num

        integer     :: iter

        do iter=1,10

            write(*,*) "print_test", num

        end do


    end subroutine print_test

    subroutine output_theo_spots()
        real            :: x_pc,y_pc, x_milm, z_milm
        integer         :: j
        integer         :: x_pix,y_pix
        logical         :: in_img


        hkl_mask=0
        do j=1,Ref_Lst%nref
            x_milm=Ref_Lst%LR(j)%x
            z_milm=Ref_Lst%LR(j)%z

            call mil_to_pix(LaueDiff,x_milm,z_milm,x_pc,y_pc)
            x_pix=nint(x_pc)
            y_pix=nint(y_pc)
            in_img = .true.
            if( x_pix>Ncol )then
                x_pix = Ncol
                in_img = .false.
            end if
            if( y_pix>Nrow )then
                y_pix = Nrow
                in_img = .false.
            end if

            if( x_pix<1 )then
                x_pix = 1
                in_img = .false.
            end if
            if( y_pix<1 )then
                y_pix = 1
                in_img = .false.
            end if
            if (in_img ) then

                write(*,*) x_pix,y_pix,int(Ref_Lst%LR(j)%h(1)),int(Ref_Lst%LR(j)%h(2)),int(Ref_Lst%LR(j)%h(3))

                hkl_mask(y_pix,x_pix)=j
            end if
        end do

    end subroutine output_theo_spots



  END module lafg_psl_image
