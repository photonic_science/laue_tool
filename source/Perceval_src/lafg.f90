!! Program LAFG
!! the purpose of this program is to do almost the same duties
!! that ESMERALDA RefUB and LaueOrient does. But from the programmer
!! point of view in a more simple way. So it will be easier to continue
!! developing and extending this new program.
!! Luiso -> CrysFML -> Winteracter -> SXtalSoft

    program lafg_psl_main
   ! Use laue_siml_modl
    use lafg_psl_image
    implicit none
    integer                   :: num_com
    real                      :: ang_x, ang_y, ang_z
    Character(Len=512)        :: comm, img_nam = "", cfl_nam = "PSLCRYS.cfl"
    Character(Len=512)        :: ori_nam = "orient_par.dat", pk_nam = "exp_pk.dat"
    Character(Len=512)        :: ang_nam = "angles.dat", calc_nam = "calc_pk.dat"
    Character(Len=512)        :: char_x, char_y, char_z
    Character(Len=512)        :: char_px1, char_py1, char_px2, char_py2
    
    integer                   :: px1, py1, px2, py2
    
    num_com=COMMAND_ARGUMENT_COUNT()
    !write(*,*) "num arg", num_com
    
    if( num_com>0 )then
        call GET_COMMAND_ARGUMENT(1, comm)
        if( num_com > 1 )then
            if( comm(1:1) == "o" .or. comm(1:1) == "O" )then
                call GET_COMMAND_ARGUMENT(2, ang_nam)
                if(num_com > 2)then
                    call GET_COMMAND_ARGUMENT(3, ori_nam)
                    if(num_com > 3)then
                        call GET_COMMAND_ARGUMENT(4, pk_nam)
                        if(num_com > 4)then
                            call GET_COMMAND_ARGUMENT(5, cfl_nam)
                        endif
                    endif
                endif
            elseif( comm(1:1) == "d" .or. comm(1:1) == "D" )then
                call GET_COMMAND_ARGUMENT(2, calc_nam)
                if(num_com > 4)then
                    call GET_COMMAND_ARGUMENT(3, char_x)
                    call GET_COMMAND_ARGUMENT(4, char_y)
                    call GET_COMMAND_ARGUMENT(5, char_z)
                    read (char_x,*) ang_x
                    read (char_y,*) ang_y
                    read (char_z,*) ang_z
                    if(num_com > 5)then
                        call GET_COMMAND_ARGUMENT(6, cfl_nam)
                    endif
                else
                    write(*,*) "Missing arguments, switching to default args"
                    comm = "c"
                endif
                
            elseif( comm(1:1) == "m" .or. comm(1:1) == "M" )then
                if(num_com == 11)then
                    call GET_COMMAND_ARGUMENT(2, calc_nam)
                    call GET_COMMAND_ARGUMENT(3, ang_nam)
                    call GET_COMMAND_ARGUMENT(4, cfl_nam)
                    call GET_COMMAND_ARGUMENT(5, char_x)
                    call GET_COMMAND_ARGUMENT(6, char_y)
                    call GET_COMMAND_ARGUMENT(7, char_z)
                    call GET_COMMAND_ARGUMENT(8, char_px1)
                    call GET_COMMAND_ARGUMENT(9, char_py1)
                    call GET_COMMAND_ARGUMENT(10, char_px2)
                    call GET_COMMAND_ARGUMENT(11, char_py2)
                    
                    read (char_x,*) ang_x
                    read (char_y,*) ang_y
                    read (char_z,*) ang_z
                    
                    read (char_px1,*) px1
                    read (char_py1,*) py1
                    read (char_px2,*) px2
                    read (char_py2,*) py2
                    
                    
                endif
            else
                call GET_COMMAND_ARGUMENT(2, calc_nam)
                if(num_com > 2)then
                    call GET_COMMAND_ARGUMENT(3, ang_nam)
                    if(num_com > 3)then
                        call GET_COMMAND_ARGUMENT(4, cfl_nam)
                    endif
                endif
            endif 
        end if
    else
        comm = "c"
    end if
    
    !write(*,*) "command =", comm
    !write(*,*) "crystal =", cfl_nam
    !write(*,*) "angles =", ang_nam
    !write(*,*) "calc_peaks =", calc_nam
    !write(*,*) "ori_params =", ori_nam
    !write(*,*) "exp_peaks =", pk_nam
    
    call Open_cfl(cfl_nam)
    
    if( comm(1:1) == "c" .or. comm(1:1) == "C" )then
        call read_angles_from_file(ang_x,ang_y,ang_z,ang_nam)
        write (*,*) '(C)alculate(  ang_x, ang_y, ang_z  ) =', ang_x, ang_y, ang_z
        call repaint_img(ang_x, ang_y, ang_z)
        call calc_mask_n_write_file(calc_nam)
        
    elseif( comm(1:1) == "d" .or. comm(1:1) == "D" )then
        write (*,*) '(D)irectCalc(  ang_x, ang_y, ang_z  ) =', ang_x, ang_y, ang_z
        call repaint_img(ang_x, ang_y, ang_z)
        call calc_mask_n_write_file(calc_nam)
        
    !elseif( comm(1:1) == "o" .or. comm(1:1) == "O" )then
    elseif( comm == "o" .or. comm == "O" )then
        call read_experimental_pks_from_file(pk_nam)
        call repaint_img(ang_x, ang_y, ang_z)
        call CLI_orient(ang_x, ang_y, ang_z,ori_nam)
        !call write_angles_into_file(ang_x,ang_y,ang_z,ang_nam)
        write(unit=*,fmt="(a,3f10.4)") '(O)rient found: ', ang_x, ang_y, ang_z
    elseif( comm == "o2" .or. comm == "O2" )then
        call read_experimental_pks_from_file(pk_nam)
        call repaint_img(ang_x, ang_y, ang_z)
        call psl_orient_exe(ang_x, ang_y, ang_z,ori_nam)
        !call write_angles_into_file(ang_x,ang_y,ang_z,ang_nam)
        write(unit=*,fmt="(a,3f10.4)") '(O)rient found: ', ang_x, ang_y, ang_z
    
    elseif( comm(1:1) == "m" .or. comm(1:1) == "M" )then
        call move_in_detector_plan(px1,py1, px2,py2, ang_x,ang_y,ang_z)
        call repaint_img(ang_x, ang_y, ang_z)
        call write_angles_into_file(ang_x,ang_y,ang_z,ang_nam)
        call calc_mask_n_write_file(calc_nam)
        
    end if
    
    
    
    end program lafg_psl_main
