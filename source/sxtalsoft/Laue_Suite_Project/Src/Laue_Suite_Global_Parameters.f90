!!---- This file is part of the "Esmeralda" project,
!!---- a tool for the treatment of Laue diffraction data
!!----
!!---- The "Esmeralda" project is distributed under LGPL. In agreement with the
!!---- Intergovernmental Convention of the ILL, this software cannot be used
!!---- in military applications.
!!----
!!---- Copyright (C)  2010-2012  Institut Laue-Langevin (ILL), Grenoble, FRANCE
!!----
!!---- Authors: Luis Fuentes-Montero    (ILL)
!!----          Juan Rodriguez-Carvajal (ILL)
!!----
!!---- This library is free software; you can redistribute it and/or
!!---- modify it under the terms of the GNU Lesser General Public
!!---- License as published by the Free Software Foundation; either
!!---- version 3.0 of the License, or (at your option) any later version.
!!----
!!---- This library is distributed in the hope that it will be useful,
!!---- but WITHOUT ANY WARRANTY; without even the implied warranty of
!!---- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
!!---- Lesser General Public License for more details.
!!----
!!---- You should have received a copy of the GNU Lesser General Public
!!---- License along with this library; if not, see <http://www.gnu.org/licenses/>.
!!----
  Module Laue_Suite_Global_Parameters

    !!----    Created: April 2010
    Use Laue_Mod,                      only: Laue_Ref_list_type, Laue_Instrument_Type, Twin_Type,      &
                                             Image_Distortion_Type
    Use Data_Trt,                      only: Peak_Type, Peak_List_Type, Finding_Peaks_Parameters_Type, &
                                             Excluded_Regions_Type
    Use CFML_GlobalDeps,               only: to_rad
    Use CFML_Crystallographic_Symmetry,only: Space_Group_Type
    Use CFML_Crystal_Metrics,          only: Crystal_Cell_Type
    use CFML_Atom_TypeDef,             only: Atom_List_Type
    use CFML_Propagation_Vectors,      only: Group_k_Type

    Use Laue_Tiff_Read,                only: Image_Conditions

    implicit none
    public

    Character(len=80)   :: Program_Name = " " !could be either "Esmeralda" or "Laue Simulator" or "LAFG"
    Integer             :: stereo_mem_bmp   ! Bitmap identifier that will show stereographic projections (memory bitmap)
    Integer             :: Circl_Siz=10     ! Variable to adjust Circle Size
    Integer             :: i_sf=31          ! Logical unit for structure factors output and other information
    Integer             :: i_ref=32         ! Logical unit for information about indexed reflections
                                            ! The attribute "parameter" has been removed in order to handle
                                            ! automatically the logical units through Get_LogUnit
    Integer, parameter  :: nk_vect=4        ! Maximum number of independent propagation vectors
    Integer             :: Ncol=900,Nrow=450! Resolution in pixels (initialised to aspect ration H/V=2)
    Integer             :: img_n=0,n_img=0  ! Image number, number of images
    Integer             :: nkvec=0          ! Number of independent propagation vectors
    Real                :: dklim=1.5        ! D-spacing limit for satellite generation


    logical :: Init_Orient_given=.false.    ! Flag : Init orientation (U_angles) given in CFL file ?
    logical :: Init_UB_given=.false.        ! Flag : Init UB-matrix given in CFL file
    logical :: Orient_known=.false.         ! Flag : True if one of the above is true or if successful orientation
    logical :: Instrm_Loaded = .false.      ! Flag : was CFL (instrm) loaded ?
    logical :: Cell_Loaded = .false.        ! Flag : was CFL (Cell) loaded ?
    logical :: struc_fact_new  = .true.     ! Needed for (re)calculation of structure factors
    logical :: repaint                      ! Flag : should experimental image be repainted ?
    logical :: slid_diag                    ! Flag : is orienting window opened ? Calculated peaks are on the image
    logical :: Sim_slid_diag                ! Flag : is orienting simulation window opened ? Calculated peaks are on the image
    logical :: drag_orient                  ! Flag : is dragging a nodal spot?
    logical :: main_win_foc                 ! Flag : is the focus on the image (main) window?
    logical :: new_spindle  = .false.       ! Flag : is needed to update the spindle
    logical :: view_stereo , stereo_old     ! Flag : is (was) the dialog of stereographic  projection visible?
    logical :: dummy_image=.false.          ! True if a dummy image has been generated
    logical :: peaks_found=.false.          ! True if one of the peak search routines have been invoked and peaks are found
    logical :: peaks_threshold=.false.      ! True if Peak_List is allocated by invoking the threshold peak-find routine
    logical :: atoms_given=.false.          ! True if atom positions have been provided in the CFL file
    logical :: twin_given=.false.           ! True if twin commands have been provided in the CFL file
    logical :: kvec_given=.false.           ! True if propagation vector commands have been provided in the CFL file
    logical :: kvec_supress=.false.         ! True for supressing display of K vectors
    logical :: newly_rotated=.false.        ! the crystal has rotated recently, so the position of calculated peaks was updated
    logical :: MonoChromatic=.false.        ! True if the data or simulation correspond to monochromatic radiation
    logical :: iPeak_file=.false.           ! True if indexed peak file of name "current_image_code"_peaks.inf exists
    logical :: uPeak_file=.false.           ! True if un-indexed peak file of name "Current_image_code"_unindexed_peaks.inf exists


    integer, dimension(6) :: cfl_file_cond  ! parameters for creating CFL file

    Type (Crystal_Cell_Type),    save :: Cell     ! Cell information on the current model
    Type (Crystal_Cell_Type),    save :: aCell    ! auxiliary cell when provisory corrections are requested
    Type (Space_Group_Type),     save :: SpG      ! Space group of the current model
    Type (Laue_Ref_list_type),   save :: Ref_Lst,&! Data related to all reflections when dynamically
                                         old_refl ! drawing the visible reflections when moving sliders
    Type (Twin_Type),            save :: Tw       ! Twin type containing info about domains

    Type (Atom_list_Type),       save :: A        ! List of atoms in the asymmetric unit

    Real :: mosaic_spread = 0.3 * to_rad  ! Mosaic spread in radians (for the moment isotropic)
    Real :: max_divergence= 0.2 * to_rad  ! Beam divergence in radians
    Real :: Spindle_Value = 0.0           ! Value of a spindle given by the user when the image has no definite spindle

    Type (Group_k_Type), dimension(nk_vect), save :: G_k       !Propagation vectors groups
    Real, dimension(3,nk_vect),              save :: kvec      !Propagation vectors
    Integer, dimension(nk_vect),             save :: nharm=1   ! Maximum harmonic number for propagation vectors
    logical, dimension(nk_vect),             save :: multik=.false.  ! True if all arm of the star have to be included

    Type (Laue_Instrument_Type),             save :: LaueDiff   ! Geometrical characteristics of the instrument, dimensions,
                                                                ! wavelength and angular limits of the Laue diffractometer
    Type (Image_Conditions),                 save :: ICondt     ! Laue image information obtained from TIFF file
    Type (Image_Distortion_Type),            save :: distort    ! MOSFLM distortion type of the images
    Type (Finding_Peaks_Parameters_Type),    save :: Fnd_param  ! Parameters edited by user for searching peaks

    !The Peak_List variable has been introduced for maintaining the full information of peaks
    !when whatever peak-find algorithm is used, it should be allocated to the number of
    !opened images.
    Type(Peak_List_Type),    dimension(:),allocatable, save :: Peak_List ! n_img lists
    Type(Laue_Ref_list_type),dimension(:),allocatable, save :: VRef_List ! Visible reflections
                                                                   ! Obtained after refinement
                                                                   ! Completed by integration
    logical,                 dimension(:),allocatable :: VRef_List_Set !True is the corresponding list has bee set

    integer(kind=1), Dimension(:,:,:),Allocatable :: Mask2D        ! Mask Data (varying with different calculations)
    Integer,         Dimension(:,:,:),Allocatable :: Data2D        ! Experimental Patterns
    integer,         Dimension(:,:),  Allocatable :: hkl_mask      ! this matrix will be used to know if a pixel has a calculated spot
    Integer,         Dimension(:,:),  Allocatable :: background2D  ! Experimental background
    Integer,         Dimension(:,:),  Allocatable :: Calc_Image    ! Calculated Image
    real,            Dimension(:),    Allocatable :: img_spdl      !spindle angle corresponding to the current experimental image
    real,            Dimension(:),    Allocatable :: img_phi_ang   !Phi angle corresponding to the current experimental image (omega angle)
    real,            Dimension(:),    Allocatable :: img_Chi_ang   !Chi angle corresponding to the current experimental image
    real,            Dimension(:),    Allocatable :: img_Gam_ang   !Gamma angle of the detector corresponding to the current experimental image

    Type (Laue_Instrument_Type), dimension(:),allocatable, save :: Mod_LDiff  ! Modified Laue instruments

    Type (Excluded_Regions_Type), public :: ExclR ! Excluded regions: Mask2D=-1
    Character(len=4)   :: op_system   ! operating system ( "LINU" .or. "MACO" .or. "WIND" .or. "UNKN" )
    Character(len=512) :: CFL_File    ! Name of the CFL file
    Character(len=512) :: filecod     ! Code of the CFL file (e.g. my_file.cfl => filecod=my_file)
    Character(len=512) :: Image_File=" "  ! Full name of the image files including path as first item separated by char(0)
    Character(len=512) :: Instrm_File ! Name of the Instrument file
    Character(len=512) :: hkl_file    ! Name of a file containing reflections (provided by the user)
                                      ! It may change in the course of a session
    Character(len=80), dimension(:), allocatable  :: File_Im     ! Name of the image files excluding path

    Character(len=256) :: var_editor = " "
    Integer            :: ic_editor=-1

    Real, dimension(3)   :: Curr_orient=(/0.0,0.0,0.0/)      !Current orienting angles
    Real, dimension(3)   :: Curr_U_angles=(/0.0,0.0,0.0/)
    Real, dimension(3)   :: Saved_U_angles=(/0.0,0.0,0.0/)
    Real, dimension(3,3) :: Current_UB, Current_R, Current_M ! => M = R UB, U= Rt M B^-1
    Real, dimension(3,3) :: Current_Rot,Current_U            ! => M = R UB = Rot B => Rot = R.U = M B^-1
    real, dimension(3,3) :: BL, Saved_UB                     ! Busing-Levy B-matrix, saved UB-matrix
    real, dimension(3,3) :: BL_inv                           ! Busing-Levy inverse B-matrix
    real, Dimension(1:3) :: near_mouse_r_vec                 ! near mouse pos rotation reciprocal vec

  End Module Laue_Suite_Global_Parameters
