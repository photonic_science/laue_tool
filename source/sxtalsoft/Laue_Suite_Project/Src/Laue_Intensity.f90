!!---- This file is part of the "Esmeralda" project,
!!---- a tool for the treatment of Laue diffraction data
!!----
!!---- The "Esmeralda" project is distributed under LGPL. In agreement with the
!!---- Intergovernmental Convention of the ILL, this software cannot be used
!!---- in military applications.
!!----
!!---- Copyright (C)  2010-2012  Institut Laue-Langevin (ILL), Grenoble, FRANCE
!!----
!!---- Authors: Luis Fuentes-Montero    (ILL)
!!----          Juan Rodriguez-Carvajal (ILL)
!!----
!!---- This library is free software; you can redistribute it and/or
!!---- modify it under the terms of the GNU Lesser General Public
!!---- License as published by the Free Software Foundation; either
!!---- version 3.0 of the License, or (at your option) any later version.
!!----
!!---- This library is distributed in the hope that it will be useful,
!!---- but WITHOUT ANY WARRANTY; without even the implied warranty of
!!---- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
!!---- Lesser General Public License for more details.
!!----
!!---- You should have received a copy of the GNU Lesser General Public
!!---- License along with this library; if not, see <http://www.gnu.org/licenses/>.
!!----
  Module Laue_Intensity_Mod
    Use CFML_GlobalDeps
    Use CFML_String_Utilities, only: l_case, Get_LogUnit
    Use CFML_Math_General,     only: Locate, sind, cosd
    Use Laue_Mod
    implicit none
    private
    public :: Lorentz_Factor, Polarization, Incident_Spectrum, Read_Incident_Spectrum, Absorption_Xrays, &
              Read_Chebychev_Lambda, allocate_corrections, Efficiency !, Extinction
    public :: Integrated_Intensities

    logical,                         public  :: read_lamb=.false.
    integer,                         private :: nlamb
    real, dimension(:), allocatable, private :: lamb
    real, dimension(:), allocatable, private :: plamb

    type, public :: Int_corrections_type
      integer                         :: nref
      real, dimension(:), allocatable :: lambda
      real, dimension(:), allocatable :: Spectrum
      real, dimension(:), allocatable :: Lorentz
      real, dimension(:), allocatable :: Polar
      real, dimension(:), allocatable :: Absorpt
      real, dimension(:), allocatable :: Eff
    End type Int_corrections_type

    Type (Int_corrections_type), public :: Icorr

    Interface Incident_Spectrum
      Module Procedure Incident_Spectrum_Chebychev
      Module Procedure Incident_Spectrum_Interpolate
      Module Procedure Incident_Spectrum_Maxwellian
    End Interface Incident_Spectrum

    Contains

    Function Efficiency(LaueDiff,R) result(ef)
      Type(Laue_Instrument_Type), intent(in) :: LaueDiff
      Type(Laue_Ref_Type),        intent(in) :: R
      real                                   :: ef
      real, dimension(3) :: xn,u


      u=(/ sind(R%gamma)*cosd(R%nu), cosd(R%gamma)*cosd(R%nu),sind(R%nu)/)

      Select Case (LaueDiff%dtype)
        Case('Cyl')
          xn=(/ sind(R%gamma), cosd(R%gamma),0.0/)
        Case('Rec')
          xn=matmul(LaueDiff%RD,(/0.0,1.0,0.0/))  !normal to the detector in L system
      End Select
      ef=abs(dot_product(xn,u))
      if(ef > 0.00001) ef=1.0/ef
    End Function Efficiency

    Function Lorentz_Factor(ttheta) result(lf)
      real, intent(in) :: ttheta
      real             :: lf
      real             :: st
      st=sind(ttheta*0.5)
      lf=1.0/(2.0*st*st)
    End Function Lorentz_Factor

    Function Polarization(ttheta,phi,rho) result(p)
      real, intent(in) :: ttheta,phi,rho
      real             :: p
      real             :: st,ct,cp,tau
      st=sind(ttheta)
      ct=cosd(ttheta)
      cp=cosd(2.0*phi)
      tau=sind(rho)
      p=(1.0+ct*ct-tau*cp*st*st)/2.0
    End Function Polarization

    Subroutine Allocate_Corrections(n,Ic,ok)
      integer,                    intent(in)     :: n
      type(Int_corrections_type), intent(in out) :: Ic
      logical,                    intent(out)    :: ok
      !
      ok=.false.
      if(allocated(Ic%lambda)) deallocate(Ic%lambda)
      allocate(Ic%lambda(n))
      if(allocated(Ic%Spectrum)) deallocate(Ic%Spectrum)
      allocate(Ic%Spectrum(n))
      if(allocated(Ic%Lorentz)) deallocate(Ic%Lorentz)
      allocate(Ic%Lorentz(n))
      if(allocated(Ic%Polar)) deallocate(Ic%Polar)
      allocate(Ic%Polar(n))
      if(allocated(Ic%Absorpt)) deallocate(Ic%Absorpt)
      allocate(Ic%Absorpt(n))
      if(allocated(Ic%Eff)) deallocate(Ic%Eff)
      allocate(Ic%Eff(n))
      Ic%lambda=0.0; Ic%Spectrum=1.0; Ic%Lorentz=1.0; Ic%Absorpt=1.0; Ic%Eff=1.0
      Ic%nref=n
      ok=.true.
      return
    End Subroutine Allocate_Corrections

    Subroutine Absorption_Xrays(lambda,x,z,xmin,xmax,zmin,zmax,xr,zr,nl,A,C,D,cx,cz,Absp,derx,derz)
      real,                         intent(in)  :: lambda,x,z,xmin,xmax,zmin,zmax,xr,zr,A,C,D
      Integer,                      intent(in)  :: nl
      real,           dimension(:), intent(in)  :: cx,cz
      real,                         intent(out) :: Absp
      real, optional, dimension(:), intent(out) :: derx,derz
      !---- Local Variables ----!
      integer            :: i
      real               :: xn,zn,smx,smz,c1x,c2x,c1z,c2z,ri,xrc,zrc,l3,l4,vict
      real,dimension(nl) :: dvx,dvz

      xn=2.0*(x-0.5*(xmax+xmin))/(xmax-xmin)
      zn=2.0*(z-0.5*(zmax+zmin))/(zmax-zmin)
      if(xr < -1.0 .or. xr > 1.0) then
        xrc=0.0
      else
        xrc=xr
      end if
      if(zr < -1.0 .or. zr > 1.0) then
        zrc=0.0
      else
        zrc=zr
      end if
      c1x=acos(xn)
      c2x=acos(xrc)
      c1z=acos(zn)
      c2z=acos(zrc)
      smx=0.0 ; smz=0.0
      l3=lambda*lambda*lambda
      l4=l3*lambda
      vict= A+C*l3-D*l4
      do i=1,nl
         ri=real(i)
         dvx(i)=cos(ri*c1x)-cos(ri*c2x)
         smx = smx + cx(i)* dvx(i)
         dvz(i)=cos(ri*c1z)-cos(ri*c2z)
         smz = smz + cz(i)* dvz(i)
      end do
      Absp=exp(-smx*smz*vict)

      if(present(derx)) then
        derx(1:nl)=-Absp*smz*vict*dvx(1:nl)
      end if
      if(present(derz)) then
        derz(1:nl)=-Absp*smx*vict*dvz(1:nl)
      end if
    End Subroutine Absorption_Xrays

    Subroutine Integrated_Intensities(LaueDiff,L,sizd,DataI,Data_bck)
      Type(Laue_Instrument_Type), intent(in)     :: LaueDiff
      Type(Laue_Ref_List_Type),   intent(in out) :: L
      Integer,                    intent(in)     :: sizd
      Integer, dimension(:,:),    intent(in)     :: DataI
      Integer, dimension(:,:),    intent(in)     :: Data_bck
      ! Local variables
      integer, dimension(:,:), allocatable :: Img
      real :: suma,xp,zp,sx,sz,delta,d2,d2s,x,z
      Integer :: i,j,k,j1,j2,k1,k2,npix
      allocate(Img(LaueDiff%Np_v,LaueDiff%Np_h))

      !Estimation of the integrated intensities
      sx=(LaueDiff%x_max -LaueDiff%x_min)/real(LaueDiff%Np_h-1)
      sz=(LaueDiff%z_max -LaueDiff%z_min)/real(LaueDiff%Np_v-1)
      Img=DataI-Data_bck

      delta = 0.5*LaueDiff%H * real(sizd) /real(LaueDiff%np_h-1)   !sizd in mm
      d2s=delta * delta
      delta= 2.0 * delta

      do i=1,L%nref
           xp=L%LR(i)%x;  zp=L%LR(i)%z
           j1=max(nint((xp-delta-LaueDiff%x_min)/sx)+1,1)
           j2=min(nint((xp+delta-LaueDiff%x_min)/sx)+1,LaueDiff%Np_h)
           k1=max(nint((zp-delta-LaueDiff%z_min)/sz)+1,1)
           k2=min(nint((zp+delta-LaueDiff%z_min)/sz)+1,LaueDiff%Np_v)
           npix=0
           suma=0.0
           do j=j1,j2
              x= LaueDiff%x_min +real(j-1)*sx
             do k=k1,k2
               z= LaueDiff%z_min +real(k-1)*sz
               d2=(x-xp)**2 +(z-zp)**2
               if( d2 < d2s ) then
                 npix=npix+1
                 suma=suma+Img(k,j)
               end if
             end do
           end do
           !suma=suma/real(npix)
           L%LR(i)%Obs_int=suma
      end do
    End Subroutine Integrated_Intensities

    Subroutine Read_Chebychev_Lambda(filenam,n,ch,ok)
      character(len=*),              intent(in)  :: filenam
      integer,                       intent(out) :: n
      real, dimension(:),allocatable,intent(out) :: ch
      logical,                       intent(out) :: ok

      !---- Local Variables ----!
      integer :: i, ins, ier
      logical :: exists, opnd
      character(len=132) :: line

      ok=.false.
      ! Check if the file exist and it is already opened
      inquire(file=filenam, exist=exists, opened=opnd, number=ins)
      if(.not. exists) return
      if(opnd) then
         rewind(unit=ins)
      else
         Call Get_Logunit(ins)
         open(unit=ins, file=trim(filenam), status="old", action="read", position="rewind")
      end if

      do
        read(unit=ins,fmt="(a)", iostat=ier) Line
        if(ier /= 0) exit
        line=adjustl(l_case(line))
        if(line(1:17) /= "chebychev_lambda") cycle
        ! The proper line has been found ... read in the number of points for allocating
        read(unit=line(18:),fmt=*) n
        if(n < 2) exit
        if(allocated( ch)) deallocate( ch)
        allocate(ch(n))
        read(unit=ins,fmt=*,iostat=ier) ch(1:n)
        if(ier /= 0) return
        ok=.true.
        exit
      end do
      return
    End Subroutine Read_Chebychev_Lambda


    Subroutine Read_Incident_Spectrum(filenam)
      character(len=*), intent(in)  :: filenam

      !---- Local Variables ----!
      integer :: i, ins, ier
      logical :: exists, opnd
      character(len=132) :: line

      read_lamb=.false.
      ! Check if the file exist and it is already opened
      inquire(file=filenam, exist=exists, opened=opnd, number=ins)
      if(.not. exists) return
      if(opnd) then
         rewind(unit=ins)
      else
         Call Get_Logunit(ins)
         open(unit=ins, file=trim(filenam), status="old", action="read", position="rewind")
      end if

      do
        read(unit=ins,fmt="(a)", iostat=ier) Line
        if(ier /= 0) exit
        line=adjustl(l_case(line))
        if(line(1:17) /= "incident_spectrum") cycle
        ! The proper line has been found ... read in the number of points for allocating
        read(unit=line(18:),fmt=*) nlamb
        if(nlamb < 2) exit
        if(allocated( lamb)) deallocate( lamb)
        if(allocated(plamb)) deallocate(plamb)
        allocate(lamb(nlamb),plamb(nlamb))
        do i=1,nlamb
          read(unit=ins,fmt=*,iostat=ier) lamb(i), plamb(i)
          if(ier /= 0) return
        end do
        read_lamb=.true.
        exit
      end do
      return
    End Subroutine Read_Incident_Spectrum

    Subroutine Incident_Spectrum_Chebychev(lambda,lmin,lmax,lr,nl,c,Isp,der)
      real,                         intent(in)  :: lambda,lmin,lmax,lr
      Integer,                      intent(in)  :: nl
      real,           dimension(:), intent(in)  :: c
      real,                         intent(out) :: Isp
      real, optional, dimension(:), intent(out) :: der
      !---- Local Variables ----!
      integer            :: i
      real               :: lp,sm,c1,c2,ri,lrc
      real,dimension(nl) :: dv

      lp=2.0*(lambda-0.5*(lmax+lmin))/(lmax-lmin)
      if(lr < -1.0 .or. lr > 1.0) then
        lrc=0.0
      else
        lrc=lr
      end if
      c1=acos(lp)
      c2=acos(lrc)
      sm=0.0
      do i=1,nl
         ri=real(i)
         dv(i)=cos(ri*c1)-cos(ri*c2)
         sm = sm + c(i)* dv(i)
      end do
      Isp=exp(-sm)

      if(present(der)) then
        der(1:nl)=-Isp*dv(1:nl)
      end if
    End Subroutine Incident_Spectrum_Chebychev

    Subroutine Incident_Spectrum_Interpolate(lambda,Isp)
      real, intent(in)  :: lambda
      real, intent(out) :: Isp
      !---- Local Variables ----!
      integer :: i
      real    :: p

      Isp=0.0
      if(.not. read_lamb) return
      if(lambda < lamb(1) .or. lambda > lamb(nlamb)) return
      ! Lambda in is range
      i=Locate(lamb,nlamb,lambda)
      !Linear interpolation
      p = (plamb(i+1)-plamb(i))/(lamb(i+1)-lamb(i))
      Isp= p*(Lambda-lamb(i)) + plamb(i)

      return
    End Subroutine Incident_Spectrum_Interpolate


    Subroutine Incident_Spectrum_Maxwellian(Lambda,Temp,Isp)
       real, intent(in)  :: Lambda
       real, intent(in)  :: Temp
       real, intent(out) :: Isp

       Isp=Maxwell_int(Lambda,Temp)
       return
    End Subroutine Incident_Spectrum_Maxwellian
    !
    !  Maxwellian f(v)= 4pi  (m/2kT)^(3/2)  v^2 exp{ -mv^2/2kT}
    !  Maxwellian f(E)= 2 sqrt[E/pi/(kT)^2]  exp{ -E/kT}
    !
    !  Maxwellian I(Lda)= 4/sqrt(pi) (m/2kT)^3/2 (h/m)^3 Lda^(-4) exp{ -h^2/2kTmLda^2}
    !
    !  E(meV) = 81.8/Lda^2 (Lda in Angstroms)
    !  Lda(Angst) = 39.66 t(us)/L(m)
    !
     Elemental Function Maxwell_int(Lambda,Temp) result(Intm)
       real,           intent(in) :: Lambda
       real, optional, intent(in) :: Temp
       real                       :: Intm
       real (kind=dp), parameter :: &
                           m=1.674927211e-27,     &  !Neutron mass (kg)
                           kb=1.3806504e-23,      &  !Boltzmann constant (J/K)
                           h=6.62606896e-34          !Plank constant (J.s)

       real (kind=dp):: kbt,hom,ex,l2
       real (kind=dp):: Temperature, Intf
       Temperature=300.0_dp  !in K
       if(present(Temp)) Temperature=Temp
       kbt=2.0*kb*Temperature
       hom=(h/m)*(h/m)*(h/m)*1.0e31_dp !*1.0e40_dp !10^40 is the convertion from angstroms to meters for 1/Lambda^4
       Intf=4.0_dp/sqrt(pi)*hom*(m/kbt)**1.5
       l2=Lambda*Lambda
       ex=h*h/(kbt*m*l2*1.0d-20)
       Intm=Intf*exp(-ex)/(l2*l2)
       return
     End Function Maxwell_int

  End Module Laue_Intensity_Mod
