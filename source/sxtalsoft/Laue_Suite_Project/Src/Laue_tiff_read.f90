  Module Laue_TIFF_READ
  use CFML_String_Utilities,        only: get_logunit
    Implicit none
   ! Module constructed from subroutines provided by Ross Piltz for reading
   ! Laue-TIFF images.
   ! The code has been converted to Fortran 95 syntax and a new type has been
   ! created containing all the useful information about a particular image.
   ! Juan Rodriguez-Carvajal
   !
   ! Original comments:
   ! KNOWN ISSUES:
   ! Only accepts little-endian (no problem for Laue data files)
   ! Unchecked for big-endian computers (especially double & short strings)
   ! Probably should run the check for big-endian in OPEN_TIF_DATA().
   private
   Public :: open_tif_data,read_tiff_image, Init_Tiff_Read, Write_ICd, Init_ICd &

   ,   read_raw_image

   Character(len=132), public :: TIFF_READ_Error_Message=" "
   Logical, public            :: TIFF_READ_Error=.false.


   Type, public :: Image_Conditions
      Character(Len=80) :: file_name         ! 0
      Character(Len=50) :: host              ! 1
      Character(Len=50) :: datetime          ! 2
      Character(Len=50) :: user              ! 3
      Character(Len=50) :: sample            ! 4
      Character(Len=250):: comment           ! 5
      Real              :: temp_begin        ! 6
      Real              :: temp_end          ! 7
      Real              :: expose_time       ! 8
      Real              :: expose_phi        ! 9
      Real              :: temp_min          !10
      Real              :: temp_max          !11
      Integer           :: xyres             !12
      Integer           :: Numx              !13
      Integer           :: Numy              !14
      Integer           :: startx            !15
      Integer           :: starty            !16
      Integer           :: speed             !17
   End Type Image_Conditions

   Type, public :: Image_File_names
     integer                                      :: n_images
     character(len=132),dimension(:), allocatable :: Names
   End Type Image_File_names

   Integer(kind=1), dimension(4000), private :: buffer1
   Integer, private :: irec, iunit

   Contains

    Subroutine Init_Tiff_Read()
       TIFF_READ_Error_Message=" "
       TIFF_READ_Error=.false.
    End Subroutine Init_Tiff_Read

    Subroutine Init_ICd(ICd)
      Type(Image_Conditions), Intent(out)    :: ICd
      ICd%file_name    =   "No Name"
      ICd%host         =   "No Host"
      ICd%datetime     =   " 0-0-2010"
      ICd%user         =   "No user"
      ICd%sample       =   "Unknown"
      ICd%comment      =   " "
      ICd%temp_begin   =   293.0
      ICd%temp_end     =   293.0
      ICd%expose_time  =   1.0
      ICd%expose_phi   =   0.0
      ICd%temp_min     =   293.0
      ICd%temp_max     =   293.0
      ICd%xyres        =   1
      ICd%Numx         =   4000
      ICd%Numy         =   2000
      ICd%startx       =   1
      ICd%starty       =   1
      ICd%speed        =   1
    End Subroutine Init_ICd

    Subroutine Write_ICd(ICd,lun)
      Type(Image_Conditions), Intent(in) :: ICd
      Integer, optional,      Intent(in) :: lun
      !----
      integer :: i_out

      i_out=6
      if(present(lun)) i_out=lun
      write(unit=i_out,fmt="(a)")      " => Image information from file: "//trim(ICd%file_name)
      write(unit=i_out,fmt="(a)")      "    Host_Name           : "//trim(ICd%host)
      write(unit=i_out,fmt="(a)")      "    Time & Date         : "//trim(ICd%datetime)
      write(unit=i_out,fmt="(a)")      "    User                : "//trim(ICd%user)
      write(unit=i_out,fmt="(a)")      "    Sample              : "//trim(ICd%sample)
      write(unit=i_out,fmt="(a)")      "    Comment             : "//trim(ICd%comment)
      write(unit=i_out,fmt="(a,f8.3)") "    Spindle Angle       : ",ICd%expose_phi
      write(unit=i_out,fmt="(a,f8.1)") "    Exposition Time     : ",ICd%expose_time
      write(unit=i_out,fmt="(a,f8.3)") "    Initial  Temperature: ",ICd%temp_begin
      write(unit=i_out,fmt="(a,f8.3)") "    Final    Temperature: ",ICd%temp_end
      write(unit=i_out,fmt="(a,f8.3)") "    Minimal Temperature : ",ICd%temp_min
      write(unit=i_out,fmt="(a,f8.3)") "    Maximal Temperature : ",ICd%temp_max
      write(unit=i_out,fmt="(a,i6)")   "    Number of pixels (H): ",ICd%Numx
      write(unit=i_out,fmt="(a,i6)")   "    Number of pixels (V): ",ICd%Numy
      return
    End Subroutine Write_ICd

    Subroutine read_tiff_image(file_name,image,nrows,ncols,ok,ICd,Col)
      Character (Len=*),               Intent(In)     :: file_name
      Integer(kind=4), dimension(:,:), Intent(Out)    :: image
      Integer,                         Intent(in Out) :: nrows
      Integer,                         Intent(in Out) :: ncols
      logical,                         Intent(out)    :: Ok
      Type(Image_Conditions),          Intent(out)    :: ICd
      Character(len=*), optional,      Intent(in)     :: Col

      Integer :: i, lun, ioffset_data, ic,ir, nr,nc, npix

      lun=77
      call Init_ICd(ICd)
      nr=nrows; nc=ncols
      ok=.false.
      Call open_tif_data(ncols, nrows,ICd, file_name,lun)
      if(TIFF_READ_Error) then
         !write(*,"(a)") " => TIFF_READ_Error: "//Trim(TIFF_READ_Error_Message)
         !write(*,"(a)") " => Use offset = 640, adequate for cyclops "
         nrows=nr; ncols=nc
         ioffset_data=640 !Valid for cyclops?
         ICd%numx=ncols   !Put the correct dimension ins ICd
         ICd%numy=nrows
      else
         ioffset_data=8   !Vivaldi
      end if
      npix=ncols*nrows
      write(unit=*,fmt="(2(a,i6),a)") " => TIFF image with ",nrows," rows and ",ncols," columns"
      write(unit=*,fmt="(2(a,i10))")  " => TIFF image with ",npix," pixels"
      if(npix > 0) ok=.true.
      if(present(col)) Then
        Do i=1,npix
          ic=1+(i-1)/Nrows
          ir=i-(ic-1)*Nrows
          Call tiff_read_int2(image(ir,ic),ioffset_data+2*i-2)
        End Do
      else
        Do i=1,npix
          ir=1+(i-1)/Ncols
          ic=i-(ir-1)*Ncols
          Call tiff_read_int2(image(ir,ic),ioffset_data+2*i-2)
        End Do
      end if
      close(unit=lun)
      Return
    End Subroutine read_tiff_image

    Subroutine open_tif_data(numx,numy, ICd, file_name,iunit0)
      Integer,                Intent(In Out) :: numx
      Integer,                Intent(In Out) :: numy
      Type(Image_Conditions), Intent(out)    :: ICd
      Character (Len=*),      Intent(in)     :: file_name
      Integer,optional,       Intent(In)     :: iunit0

      Character (Len=250) :: string
      Integer :: ierr, imagic, i42, ntags, ioff_ifd, itag, i_value, ioffset
      Integer :: itag_id, itag_type, itag_count, itag_val2, itag_val4, itag_offset
      Real    :: value, ratio

      ! Initialise some parameters for TIFF_READ_INT1() which actually reads the file

      irec=-1

      ! Open file by direct access and 4000 byte records
      if(present(iunit0)) then
        iunit=iunit0
        Open(Unit=iunit,File=trim(file_name),Status='OLD',Form='UNFORMATTED',Access='DIRECT',Recl=4000,iostat=ierr)
      else
        call get_logunit(iunit)
        Open(Unit=iunit,File=trim(file_name),Status='OLD',Form='UNFORMATTED',Access='DIRECT',Recl=4000,iostat=ierr)
      end if

      if(ierr /= 0) then
        TIFF_READ_Error=.true.
        TIFF_READ_Error_Message= ' => ERROR: Unable to open TIF image file: '//trim(file_name)
        return
      end if
      ! Complain and die if not a little-endian TIF

      Call tiff_read_int2(imagic,0)
      If(imagic == 257*Ichar('M'))then
        TIFF_READ_Error=.true.
        TIFF_READ_Error_Message= ' => ERROR: File is not a Laue TIF'
        return
      End If
      If(imagic /= 257*Ichar('I'))Then
        TIFF_READ_Error=.true.
        TIFF_READ_Error_Message= ' => ERROR: File is not a TIF file'
        return
      End if

      Call tiff_read_int2(i42,2)
      If(i42 /= 42) then
        TIFF_READ_Error=.true.
        TIFF_READ_Error_Message= ' => ERROR: File is not a valid TIF file'
        return
      End If

      ! Get file offset to the first IFD

      Call tiff_read_int4(ioff_ifd,4)

      ! Loop through all records in the IFD

      Call tiff_read_int2(ntags,ioff_ifd)
      ioff_ifd=ioff_ifd+2
      Do itag=1,ntags
      ! Read and unpack the next record in the IFD
        Call tiff_read_int2(itag_id,ioff_ifd)
        Call tiff_read_int2(itag_type,ioff_ifd+2)
        Call tiff_read_int4(itag_count,ioff_ifd+4)
        Call tiff_read_int2(itag_val2,ioff_ifd+8)
        Call tiff_read_int4(itag_val4,ioff_ifd+8)
        itag_offset=itag_val4         ! the last I4 may also be an offset
        ! Create the value (STRING, I_VALUE, VALUE) depending on ITAG_TYPE
        Select Case(itag_type)
          Case(2)
             Call tiff_get_string(string,itag_offset,itag_count)
          Case(3)
             i_value=itag_val2
          Case(4)
             i_value=itag_val4
          Case(5)
             Call tiff_get_rational(value,itag_offset)
          Case(12)
             Call tiff_get_double(value,itag_offset)
        End Select

        ! Record the value for some of the general TIFF ID's
        Select Case (itag_id)
           case(256)
              numx=i_value                   !  13
           case(257)
              numy=i_value                   !  14
           case(273)
              !The offset to the image data is stored as an array, so we must use ITAG_OFFSET
              Call tiff_read_int4(ioffset,itag_offset)
              If(ioffset /= 8) Then
                TIFF_READ_Error=.true.
                TIFF_READ_Error_Message= ' => ERROR: Bug(open_tif_data): Unexpected IOFFSET '
                return
              End If
           case(270)
              ICd%comment=string(1:min(250,itag_count-1))     !5
           case(282)
              Call tiff_get_rational(ratio,itag_offset)
              ICd%xyres=Nint(1.0E4*ratio)                     !12
           case(306)
              ! Convert date to Australian format             !2
              ICd%datetime=string(12:itag_count-1)//' '//string(9:10)//'/'//string(6:7)//'/'//string(1:4)
           case(315)
              ICd%user=string(1:min(50,itag_count-1))         !3
           case(316)
              ICd%host=string(1:min(50,itag_count-1))           !1

            ! Record the value for the user defined TIFF ID's

           case(1000)
              ICd%temp_begin=value               ! 6
           case(1001)
              ICd%temp_end=value                 ! 7
           case(1002)
              ICd%sample=string(1:min(50,itag_count-1))            ! 4
           case(1003)
              ICd%expose_time=value              ! 8
           case(1004)
              ICd%expose_phi=value               ! 9
           case(1005)
              ICd%startx=i_value                 !15
           case(1006)
              ICd%starty=i_value                 !16
           case(1007)
              ICd%speed=i_value                  !17
           case(1008)
              ICd%temp_min=value                 !10
           case(1009)
              ICd%temp_max=value                 !11
        End Select
        ! Advance the offset to the next IFD record, then loop back
        ioff_ifd=ioff_ifd+12
      End Do
      ICd%numx=numx           ! 13
      ICd%numy=numy           ! 14
      ICd%file_name=file_name ! 0
      Return
    End Subroutine open_tif_data

    Subroutine tiff_get_string(string,ioffset,n_values)
      Character (Len=250), Intent(Out)    :: string
      Integer, Intent(In)               :: ioffset
      Integer                           :: n_values

      Integer :: i, i4, ivalue
      ! If a short string the characters are in the bytes of IOFFSET

      ! Can read only up to 250 characters
      If(n_values > 250) Then
        n_values = 250
        write(*,"(a,i6,a)") " => Warning: ASCII Tag at ioffset ",ioffset,&
                            " is longer than 250 charaters and will not be fully read."
      End If
      If(n_values <= 4) Then
      ! This method ensures IOFFSET is unpacked as READ_TIFF_INT4() packed it
        i4=ioffset
        Do i=1,n_values
          string(i:i)=Char(Iand(i4,255))
          i4=i4/256
        End Do
      ! If a long string, get it from the bytes according to the IOFFSET
      Else
        Do i=1,n_values
          Call tiff_read_int1(ivalue,ioffset+i-1)
          string(i:i)=Char(ivalue)
        End Do
      End If
      Return
    End Subroutine tiff_get_string

    Subroutine tiff_get_rational(ratio,ioffset)
      real,    intent(out) :: ratio
      integer, intent(in)  :: ioffset
      integer :: idenom, inumer
      Call tiff_read_int4(idenom,ioffset)
      Call tiff_read_int4(inumer,ioffset+4)
      If(idenom == 0) Then
        ratio=0.0
      Else
        ratio=Real(inumer)/Real(idenom)
      End If
      Return
    End Subroutine tiff_get_rational

    Subroutine tiff_get_double(value,ioffset)
      Real,    Intent(Out)     :: value
      Integer, Intent(In )     :: ioffset
      Real(kind=8),    dimension(1) ::  val_r8
      Integer(kind=4), dimension(2) ::  val_i4
      Equivalence (val_i4,val_r8)
      ! NB: This probably needs to be swapped to work with a big-endian computer
      Call tiff_read_int4(val_i4(1),ioffset)
      Call tiff_read_int4(val_i4(2),ioffset+4)
      value=real(val_r8(1))
      Return
    End Subroutine tiff_get_double

    Subroutine tiff_read_int4(ivalue,ioffset)
      Integer, Intent(Out)     :: ivalue
      Integer, Intent(In )     :: ioffset
      Integer :: ival1, ival2
      Call tiff_read_int2(ival1,ioffset)
      Call tiff_read_int2(ival2,ioffset+2)
      ! Use a shift (not a multiplication) to avoid an integer overflow
      ivalue=ival1+Ishft(ival2,16)
      Return
    End Subroutine tiff_read_int4

    Subroutine tiff_read_int2(ivalue,ioffset)
      Integer, Intent(Out)     :: ivalue
      Integer, Intent(In )     :: ioffset
      Integer :: ival1, ival2
      Call tiff_read_int1(ival1,ioffset)
      Call tiff_read_int1(ival2,ioffset+1)
      ! Multiplication is generally faster than a shift
      !       IVALUE=IVAL1+ISHFT(IVAL2,8)
      ivalue=ival1+ival2*256
      Return
    End Subroutine tiff_read_int2

    Subroutine tiff_read_int1(ivalue,ioffset)
      Integer, Intent(Out)   :: ivalue
      Integer, Intent(In)    :: ioffset
      integer :: irec2, istart, idummy
      ! Read the required record if not in the buffer

      irec2=ioffset/4000+1
      If(irec2 /= irec) Then
        irec=irec2
      ! To read the last record of the file we ignore all I/O errors
        Read(iunit,Rec=irec,Iostat=idummy) buffer1
      End If

      ! Extract the required byte from the buffer
      istart=ioffset-(irec-1)*4000
      !ivalue=zext(buffer1(istart+1))  ! Zext function is not standard (Only for Intel compiler)
      ivalue=buffer1(istart+1)         ! This below is equivalent to ZEXT
      if(ivalue < 0) ivalue=ivalue + 256
      Return
    End Subroutine tiff_read_int1

    subroutine read_raw_image(Img_File,Nrow,Ncol,Img2D)
        Character(Len=*),            intent(in )    :: Img_File
        integer,                     intent(in )    :: Nrow,Ncol
        Integer, Dimension(:,:),     intent(out)    :: Img2D

        integer                                     :: log_uni, x, y

        integer(kind=2)                             :: int16b
        integer(kind=4)                             :: int32b

        write(unit=*,fmt="(a)")  ' => Raw file selected'
        !Assumes 16 unsigned integers stored without any offset. The image matrix is stored by rows.
        !The raw data are stored in big-endian mode (this may be chosen in the CFL file in forthcoming versions)

        Call Get_Logunit(log_uni)

        open(unit=log_uni, file=Img_File, status="old",action="read", position="rewind", convert= 'BIG_ENDIAN',&
             access="stream", form="unformatted")

        do x=1,Ncol, 1
          do y=1,Nrow, 1
            read(unit=log_uni) int16b
            int32b=int16b
            if(int32b < 0) int32b=int32b+65536
            Img2D(y,x) = int32b
          end do
        end do

        write(*,*) "maxval(intens)", maxval(Img2D(:,:))
        write(*,*) "minval(intens)", minval(Img2D(:,:))

    return
    end subroutine read_raw_image


  End  Module Laue_TIFF_READ
