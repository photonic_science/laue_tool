! Program to convert numors of d19 to binary files taht are read much faster
! thant the ASCII files and occupy much less memory
!
Program d19_convert
  use ReadnWrite_2D_Detector_Images
  implicit none
  integer                         :: i,j,k,lun,Max_Length,luni, numor,spl,num1,num2
  integer, dimension(:,:,:), allocatable       :: Imagen
  real                            :: random, omegaw, tini,tfin
  character(len=80)               :: filename, filen
  type(Header_type)               :: HeaderT, HeaderR
  logical                         :: ok, split=.false., esta
  character(len=80)               :: Mess,newname, Instrm
  character(len=2000)             :: head_string

  !filename="test_binary"
  !filen="re-test"
  Max_Length=2000

  write(unit=*,fmt="(a)") " => Program to Convert SXTAL Numor Files into binary form"
  write(unit=*,fmt="(a)",advance="no") " => Enter the name of the instrument: "
  read(unit=*,fmt="(a)") Instrm
  write(unit=*,fmt="(a)",advance="no") " => Enter, Numor_ini,Numor_fin, omegaw and split: "
  read(unit=*,fmt=*) num1,num2,omegaw,spl
  write(unit=*,fmt="(a)",advance="no") " => Standard Name? <cr>, New name? Enter newname: "
  read(unit=*,fmt="(a)") newname

  call cpu_time(tini)
  if(spl == 1) split=.true.
  k=1
  if(num2 < num1) k=-1
  if(len_trim(newname) == 0) then
     do Numor=num1,num2,k
        write(unit=mess,fmt="(i6.6)") Numor
        inquire(file=trim(mess), exist=esta)
        if( .not. esta) cycle
        call Convert_sxtal_numors(Instrm,Numor,omegaw,split)
     end do
   else
     do Numor=num1,num2,k
        write(unit=mess,fmt="(i6.6)") Numor
        inquire(file=trim(mess), exist=esta)
        if( .not. esta) cycle
        call Convert_SXTAL_numors(Instrm,Numor,omegaw,split,new_name=trim(newname))
     end do
   end if
  call cpu_time(tfin)
  write(*,"(a,f12.5)") " => CPU time for converting the files: ",tfin-tini
  !
  !  For testing purposes this part re-read the binary data
  !
  call cpu_time(tini)
  do Numor=num1,num2,k
     j=0
     do
        j=j+1
        if(len_trim(newname) == 0) then
           write(unit=filename,fmt="(i6.6,a,i4.4)") Numor,"_",j
        else
           write(unit=filename,fmt="(a,i4.4)") trim(newname)//"_",j
        end if
!        write(unit=filename,fmt="(a,i6.6,a,i4.4)") "S",Numor,"_",j
        filename=trim(filename)
        inquire(file=trim(filename)//".hbin",exist=esta)
        if(.not. esta) exit
        write(unit=*,fmt="(a)") " => Reading the file "//trim(filename)//".hbin"
        call Read_Header_2D_Image(filename,Max_Length,headerR,ok,mess,lun,head_string)
        if(.not. ok) write(*,"(a)") trim(mess)
        !write(unit=*,fmt="(a)") trim(head_string)
        if(allocated(imagen)) deallocate(imagen)
        allocate(imagen(HeaderR%Nrow,HeaderR%Ncol,headerR%NFrames))
        do i=1,headerR%NFrames
          !write(unit=*,fmt="(a,i4)") " => Reading frame #:",i
          call Read_Binary_2D_Image(lun,headerR,Imagen(:,:,i),i)
        end do

     end do
  end do
  call cpu_time(tfin)
  write(*,"(a,f12.5)") " => CPU time for reading the files: ",tfin-tini

End Program d19_convert