!!---- This file is part of the "Esmeralda" project,
!!---- a tool for the treatment of Laue diffraction data
!!----
!!---- The "Esmeralda" project is distributed under LGPL. In agreement with the
!!---- Intergovernmental Convention of the ILL, this software cannot be used
!!---- in military applications.
!!----
!!---- Copyright (C)  2010-2012  Institut Laue-Langevin (ILL), Grenoble, FRANCE
!!----
!!---- Authors: Luis Fuentes-Montero    (ILL)
!!----          Juan Rodriguez-Carvajal (ILL)
!!----
!!---- This library is free software; you can redistribute it and/or
!!---- modify it under the terms of the GNU Lesser General Public
!!---- License as published by the Free Software Foundation; either
!!---- version 3.0 of the License, or (at your option) any later version.
!!----
!!---- This library is distributed in the hope that it will be useful,
!!---- but WITHOUT ANY WARRANTY; without even the implied warranty of
!!---- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
!!---- Lesser General Public License for more details.
!!----
!!---- You should have received a copy of the GNU Lesser General Public
!!---- License along with this library; if not, see <http://www.gnu.org/licenses/>.
!!----
  Module Data_Trt
    Use CFML_GlobalDeps,       only: pi, tpi, to_deg, dp
    Use CFML_Math_General,     only: In_Sort, Sort
    Use CFML_IO_Formats,       only: file_list_type,File_to_filelist
    Use CFML_String_Utilities, only: l_case, Get_LogUnit
    Use compilers_specific

    Implicit None
    Private
    Public:: Red_Green_Blue,Mask_Trt_Side_Peak, Check_Limits, Binning_Matrix, cut_peak, Arrange_PeakList, &
             Peak_Find_Threshold, Read_Excluded_Regions, Write_Excluded_Regions, noise_out, &
             Peak_Find, calc_curv_points, integrated_intensity, Arrange_Double_PeakList, &
             cyclops_to_cylindr, flip_2D, offset_mov, save_zoomed_img, Invert_Intensities_2D, &
             Point_Nodals, Octagon_to_cylinder !, ini_p_bar, end_p_bar, update_p_bar

    Public:: In_Rect_Excluded_Region, In_Circ_Excluded_Region

    integer(kind=1), Dimension(:,:),  Allocatable, Public :: Mask_Excl     ! 2D Mask used for fixed excluded regions
                                                                           ! (allocated after reading 1 image)

    !!----  Type, public:: Excluded_Regions_Type
    !!----      Integer :: Nexc_rect   !Number of rectangular excluded regions
    !!----      Integer :: Nexc_circ   !Number of circular excluded regions
    !!----      Integer :: imax,jmax   !Maximum pixel indices (it is supposed that they start at 1)
    !!----      integer, dimension(:,:,:),allocatable :: exc_rect  ! Rect. excl. regions (2,2,Nexc_rect) in pixels
    !!----      integer, dimension(:,:),  allocatable :: exc_circ  ! Circ. excl. regions (3,Nexc_circ) in pixels
    !!----
    !!----    {Exc_rect(1,1,n),Exc_rect(2,1,n)} pixels (i1,j1) minimum i,j-corner of n-rectangle
    !!----    {Exc_rect(1,2,n),Exc_rect(2,2,n)} pixels (i2,j2) maximum i,j-corner of n-rectangle
    !!----
    !!----    {Exc_circ(1,n),Exc_circ(2,n)} Centre of n-circle, of radius Exc_circ(3,n)
    !!----
    !!----
    !!----  End Type Excluded_Regions_Type

    Type, public:: Excluded_Regions_Type
        Integer :: Nexc_rect   !Number of rectangular excluded regions
        Integer :: Nexc_circ   !Number of circular excluded regions
        Integer :: imax,jmax   !Maximum pixel indices (it is supposed that they start at 1)
        integer, dimension(:,:,:), allocatable :: exc_rect  ! Rect. excl. regions (2,2,Nexc_rect) in pixels
        logical, dimension(:),     allocatable :: eliptic   ! if true then should be painted an elipse
        integer, dimension(:,:),   allocatable :: exc_circ  ! Circ. excl. regions (3,Nexc_circ) in pixels
    End Type Excluded_Regions_Type

   Type, public :: Peak_Type
      integer           :: npix=0               !Number of pixels contributing to peak
      integer           :: pnt=-1               !Pointer to the position in the list of calculated peaks
      real, dimension(2):: cdm=(/0.0,0.0/)      !Centroid of the peak
      real, dimension(2):: pos=(/0.0,0.0/)      !Position of the Peak (maximum or baricentre)
      real, dimension(3):: Itens=(/0.0,0.0,0.0/)!2 eingenvalues of Inertia tensor + rotation angle (Lx,Lz,alpha)
      real              :: Intensity=0.0        !Integrated intensity or intensity of maximum (depends on the algorithm using the type)
      real              :: Sigma=0.0            !Estimated standard deviation
   End Type Peak_Type

   Type, public :: Peak_List_Type
      integer   :: N_Peaks
      Type(Peak_Type), dimension(:), allocatable :: Peak
   End Type Peak_List_Type

!**********************************************************************************************************
    !!----  Type, public:: Finding_Peaks_Parameters_Type
    !!----      Integer :: d_min = 5    ! minimum distance between peaks
    !!----      Integer :: m_pk  = 0    ! minimum slope of peaks
    !!----      Integer :: pk_ar = 6    ! minimum area of peaks
    !!----      Integer :: L_siz = 3    ! how many times pk_ar to consider maximum area of peaks
    !!----      Integer :: CutOff= 15   ! Consider peaks only when Intensity > Threshold = bck + CutOff * Sigma(bck)
    !!----      Integer :: blocksz=50   ! Size in pixels of the squares for searching local backgrounds in threshold algorithm
    !!----  End Type Finding_Peaks_Parameters_Type

    Type, public:: Finding_Peaks_Parameters_Type
        Integer :: d_min = 5    ! minimum distance between peaks
        Integer :: m_pk  = 0    ! minimum slope of peaks
        Integer :: pk_ar = 6    ! minimum area of peaks
        Integer :: L_siz = 3    ! how many times pk_ar to consider maximum area of peaks
        Integer :: CutOff= 15   ! Consider peaks only when Intensity > Threshold = bck + CutOff * Sigma(bck)
        Integer :: blocksz=50   ! Size in pixels of the squares for searching local backgrounds in threshold algorithm
    End Type Finding_Peaks_Parameters_Type
!*************************************************************************************************************
    !Type, public  :: Pck_Ps
    !   Real                             :: X,Y       ! Position Of Max In Pixels
    !   Real                             :: Intst     ! Intensity Of Max Pixel or integrated intensity
    !   integer                          :: pnt=-1    ! position in list
    !   Real                             :: sigma     ! sigma
    !   Real                             :: sig_d_its ! sigma divided by intensity
    !End Type Pck_Ps
    !
    !Type, public  :: lst_pks       !Equivalent to Peak_List_Type
    !    Type(Pck_Ps), dimension(:), allocatable :: Peak
    !    integer                                 :: n_pks
    !end type lst_pks
    !
    !Type, public  :: ls_lst_pks        ! this is not needed!!!!!!
    !    Type(lst_pks), dimension(:), allocatable :: lst
    !end type

    integer :: mult_pos, stp ! vars used for console progress bar


!*************************************************************************************************************
    Interface Binning_Matrix
      Module Procedure Binning_Matrix_i
      Module Procedure Binning_Matrix_r
    End Interface Binning_Matrix



    Contains
 !!---- Subroutine Binning_Matrix(A,n,R)
 !!---- Integer, dimension(:,:), intent(in) :: A  !Input matrix of dimension N x M
 !!---- Integer,                 intent(in) :: n  !degree of binning 3,5,7, ... (odd number)
 !!---- Integer, dimension(:,:), intent(out):: R  !Ouput matrix of dimension  N/n x M/n
 !!----
 !!---- Subroutine for averaging contiguous pixels, reduce the size of the image to
 !!---- be treated and diminish the noise. The binning degree is n, the resulting matrix
 !!---- has a size N/n x M/n, compared to the input N x M. Both matrices should be
 !!---- allocated in the calling unit.
 !!----
 !!---- Created June 2010 (JRC)
 !!---- Updated June 2010
 !!----
    Subroutine Binning_Matrix_i(A,n,R)
      Integer, dimension(:,:), intent(in) :: A
      Integer,                 intent(in) :: n
      Integer, dimension(:,:), intent(out):: R
      integer :: i,j,k,l,p,q,n2

      k=0
      p=(n+1)/2; q=n/2; n2=n*n
      do i=p,size(A,1)-p,n
         k=k+1
         l=0
         do j=p,size(A,2)-p,n
            l=l+1
            R(k,l)=sum(A(i-q:i+q,j-q:j+q))/n2
         end do
      end do
    End Subroutine Binning_Matrix_i

    Subroutine Binning_Matrix_r(A,n,R)
      Real, dimension(:,:), intent(in) :: A
      Integer,              intent(in) :: n
      Real, dimension(:,:), intent(out):: R

      integer :: i,j,k,l,p,q
      real    :: rn

      k=0
      p=(n+1)/2; q=n/2; rn=real(n*n)
      do i=p,size(A,1)-p,n
        k=k+1
        l=0
        do j=p,size(A,2)-p,n
          l=l+1
          R(k,l)=sum(A(i-q:i+q,j-q:j+q))/rn
        end do
      end do

    End Subroutine Binning_Matrix_r
    !!----
    !!---- Subroutines controlling excluded regions of the image
    !!----
    !!----
    Function In_Rect_Excluded_Region(i,j,ExclR) result(itis)
      integer,                     intent(in) :: i,j
      Type(Excluded_Regions_Type), intent(in) :: ExclR
      logical                                 :: itis
      !---- Local variables ----!
      integer :: n
      itis=.false.
      if( i > ExclR%imax .or. i < 1 .or. j > ExclR%jmax .or. j < 1) then !bad input indices
        itis=.true. !it is in an excluded region
        return
      end if
      If(allocated(Mask_Excl)) then
        if (Mask_Excl(i,j) == -1)then
          itis=.true.
          return
        end if
      else
        do n=1,ExclR%Nexc_Rect
          if( i >= ExclR%exc_rect(1,1,n) .and. i <= ExclR%exc_rect(1,2,n) .and. &
              j >= ExclR%exc_rect(2,1,n) .and. j <= ExclR%exc_rect(2,2,n)) then
             itis=.true.
             return
          end if
        end do
      end if
    End Function In_Rect_Excluded_Region

    Function In_Circ_Excluded_Region(i,j,ExclR) result(itis)
      integer,                     intent(in) :: i,j
      Type(Excluded_Regions_Type), intent(in) :: ExclR
      logical                                 :: itis
      !---- Local variables ----!
      integer :: n,r2,re

      itis=.false.
      if( i > ExclR%imax .or. i < 1 .or. j > ExclR%jmax .or. j < 1) then !bad input indices
        itis=.true. !it is in an excluded region
        return
      end if

      If(allocated(Mask_Excl)) then
        if (Mask_Excl(i,j) == -1) then
          itis=.true.
          return
        end if
      else
        do n=1,ExclR%Nexc_Circ
          r2= (i-ExclR%exc_circ(1,n))**2+(j-ExclR%exc_circ(2,n))**2
          re= ExclR%exc_circ(3,n)*ExclR%exc_circ(3,n)
          if( r2 <= re) then
             itis=.true.
             return
          end if
        end do
      end if

    End Function In_Circ_Excluded_Region
    !
    ! Subroutine Red_Green_Blue(norm_ints,rd,gr,bl)
    ! from intensity of the particular pixel, (scaled 0 to 1)
    ! calculates components red, green and blue (scaled 0 to 255)
    ! color palette = black < red < yellow < white
    Subroutine Red_Green_Blue(norm_ints,rd,gr,bl,palet)
      real,    intent(In ) :: norm_ints ! intensity normalized (min=0 , max=1)
      integer, intent(In ) :: palet     ! Palette code (1=          heat, 2=          blue, 3=              Black & white
                                        !               4= Inverted HEAT, 5= Inverted blue, 6= Inverted     Black & white
      integer, intent(out) :: rd,gr,bl  ! red, green and blue calculated
      ! local variables
      real :: an,local_norm_ints

      if( palet==6 .or. palet==7 .or. palet==8  .or. palet==9  .or. palet==10 )then
          local_norm_ints=1.0-norm_ints
      else
          local_norm_ints=norm_ints
      end if

      if( local_norm_ints<0.0 )then
          local_norm_ints=0.0
      else if( local_norm_ints>1.0 )then
          local_norm_ints=1.0
      end if

      if ( palet == 1 .or. palet == 6) then

        if (local_norm_ints < (1.0/3.0) .and. local_norm_ints >= 0.0) then
            an=local_norm_ints*3.0
            rd=int(an*255)
            gr=0.0
            bl=0.0
        else if (local_norm_ints >= 1.0/3.0 .and. local_norm_ints < 2.0/3.0) then
            an=(local_norm_ints-1.0/3.0)*3.0
            rd=int(255)
            gr=int(an*255)
            bl=0.0
        else if (local_norm_ints >= 2.0/3.0 .and. local_norm_ints <= 1.0) then
            an=(local_norm_ints-2.0/3.0)*3.0
            rd=int(255)
            gr=int(255)
            bl=int(an*255)
        else
            rd=255.0
            gr=255.0
            bl=255.0
        end if

      else if( palet==2 .or. palet==7 )then

        if ( local_norm_ints < 1.0/3.0 .and. local_norm_ints >= 0.0) then
            an=local_norm_ints*3.0
            bl=int(an*255)
            gr=0.0
            rd=0.0
        else if( local_norm_ints >= 1.0/3.0 .and. local_norm_ints < 2.0/3.0) then
            an=(local_norm_ints-1.0/3.0)*3.0
            rd=0
            gr=int(an*255)
            bl=255-int(an*255)
        else if( local_norm_ints >= 2.0/3.0 .and. local_norm_ints <= 1.0) then
            an=(local_norm_ints-2.0/3.0)*3.0
            rd=int(an*255)
            gr=int(255)
            bl=int(an*255)
        else
            rd=255
            gr=255
            bl=255
        end if

      else if( palet==3 .or. palet==8 )then ! if( palet==3 )then

        !#####################################
        ! the black and white theme
        ! is as simple as the following code
        an=local_norm_ints
        rd=int(an*255)
        gr=int(an*255)
        bl=int(an*255)
        !#####################################

      elseif( palet==4 .or. palet==9 )then ! if( palet==4 )then

        if (local_norm_ints < 1.0/4.0 .and. local_norm_ints >= 0.0) then
          ! red to yellow
          an=local_norm_ints*4.0
          rd=int(255)
          gr=int(an*255)
          bl=0.0
        else if (local_norm_ints >= 1.0/4.0 .and. local_norm_ints < 2.0/4.0) then
          ! yellow to green
          an=(local_norm_ints-1.0/4.0)*4.0
          rd=int(255-an*255)
          gr=int(255)
          bl=0.0
        else if (local_norm_ints >= 2.0/4.0 .and. local_norm_ints < 3.0/4.0) then
          ! green to blue
          an=(local_norm_ints-2.0/4.0)*4.0
          rd=0.0
          gr=int(255-an*255)
          bl=int(an*255)
        else if (local_norm_ints >= 3.0/4.0 .and. local_norm_ints < 1.0) then
          ! blue to violet
          an=(local_norm_ints-3.0/4.0)*4.0
          rd=int(an*50)
          gr=0.0
          bl=int(255-an*205)
        else
          rd=50.0
          gr=0.0
          bl=50.0
        end if

      else

        if (local_norm_ints < 1.0/5.0 .and. local_norm_ints >= 0.0) then

          an=local_norm_ints*5.0
          ! white to red
          rd=255
          gr=int(255-an*255)
          bl=int(255-an*255)
        else if (local_norm_ints >= 1.0/5.0 .and. local_norm_ints < 2.0/5.0) then

          an=(local_norm_ints-1.0/5.0)*5.0
          ! red to yellow
          rd=int(255)
          gr=int(an*255)
          bl=0


        else if (local_norm_ints >= 2.0/5.0 .and. local_norm_ints < 3.0/5.0) then

          an=(local_norm_ints-2.0/5.0)*5.0
          ! yellow to green
          rd=int(255-an*255)
          gr=int(255)
          bl=0


        else if (local_norm_ints >= 3.0/5.0 .and. local_norm_ints < 4.0/5.0) then


          an=(local_norm_ints-3.0/5.0)*5.0
          ! green to blue
          rd=0
          gr=int(255-an*255)
          bl=int(an*255)

        else if (local_norm_ints >= 4.0/5.0 .and. local_norm_ints < 1.0) then

          an=(local_norm_ints-4.0/5.0)*5.0
          ! blue to black
          rd=0.0
          gr=0.0
          bl=int(255-an*255)
         else
          rd=0
          gr=0
          bl=0
        end if
      end if
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        ! black,     (0,  0,  0  )
        ! red,       (255,0,  0  )
        ! yellow,    (255,255,0  )
        ! green,     (0,  255,0  )
        ! blue,      (0,  0,  255)
        ! violet,    (255,0,  255)
        ! white,     (255,255,255)
      return
    End Subroutine Red_Green_Blue
    ! Subroutine Cut_Peak(xmin,ymin,xmax,ymax,InDat2D,Msk2D,bck2D)
    !
    Subroutine Cut_Peak(xmin,ymin,xmax,ymax,InDat2D,Msk2D,bck2D,bord_siz)
      real,                           intent(in)  :: xmin,ymin ! lower left corner of the viewing area in pixels
      real,                           intent(in)  :: xmax,ymax ! uper right corner of the viewing area in pixels
      Integer,Dimension(:,:),         intent(in)  :: InDat2D   ! 2D Pattern
      integer(kind=1),Dimension(:,:), intent(in)  :: Msk2D     ! 2D Mask (0 = Background, 1 = maximum, 2 = nodal maximum, 3 = peak area but not maximum),
      Integer,Dimension(:,:),         intent(out) :: Bck2D
      integer, optional,              intent(in)  :: bord_siz
      ! local vars
      integer                :: lb1,ub1,lb2,ub2
      integer                :: x,y,i,j,k, siz
      integer,Dimension(1:4) :: I_bord

      lb1=lbound(Bck2D,1)
      ub1=ubound(Bck2D,1)
      lb2=lbound(Bck2D,2)
      ub2=ubound(Bck2D,2)
      Bck2D=InDat2D

      siz=5
      if(present(bord_siz)) siz=bord_siz

      !write(unit=*,fmt="(a)") ' => Interpolating to calculate background'
      !call ini_p_bar(nint(xmin),nint(xmax))

      do x=nint(xmin),nint(xmax)
        !call update_p_bar()
        call prog_bar(nint(xmin), x, nint(xmax),"Interpolating")
        do y=nint(ymin),nint(ymax)
          if( Msk2D(y,x)/=0 )then

            do i=y,lb1,-1
              if( Msk2D(i,x) == 0 )then
                ! I_bord(1)=InDat2D(i,x)
                call cuad_average(InDat2D,i,x,siz,I_bord(1))
                exit
              else if(i == lb1)then
                I_bord(1)=-1
              end if
            end do

            do i=y,ub1
              if( Msk2D(i,x) == 0 )then
                ! I_bord(2)=InDat2D(i,x)
                call cuad_average(InDat2D,i,x,siz,I_bord(2))
                exit
              else if(i == ub1)then
                I_bord(2)=-1
              end if
            end do

            do i=x,lb2,-1
              if( Msk2D(y,i) == 0 )then
                ! I_bord(3)=InDat2D(y,i)
                call cuad_average(InDat2D,y,i,siz,I_bord(3))
                exit
              else if(i==lb2)then
                I_bord(3)=-1
              end if
            end do

            do i=x,ub2
              if( Msk2D(y,i) == 0 )then
                ! I_bord(4)=InDat2D(y,i)
                call cuad_average(InDat2D,y,i,siz,I_bord(4))
                exit
              else if(i == ub2)then
                I_bord(4)=-1
              end if
            end do

            j=0
            k=0
            do i=1,4,1
              if( I_bord(i) /= -1 )then
                k=k+1
                j=j+I_bord(i)
              end if
            end do

            Bck2D(y,x)=real(j)/real(k)

          end if
        end do
      end do
      write(unit=*,fmt="(a)") " => Done!"

      do i=1,4
        do x=nint(xmin),nint(xmax)
          do y=nint(ymin),nint(ymax)
            if( Msk2D(y,x)/=0 )then
              if( x+1 < lb2 .and. x-1 > ub2 .and. y+1 < lb1 .and. y-1 > ub1) then
                Bck2D(y,x)=nint(real(Bck2D(y,x+1)+ Bck2D(y,x-1)+ Bck2D(y+1,x)+ Bck2D(y-1,x))/4.0)
              end if
            end if
          end do
        end do

        do x=nint(xmax),nint(xmin),-1
          do y=nint(ymax),nint(ymin),-1
            if( Msk2D(y,x) /= 0 )then
              if( x+1 < lb2 .and. x-1 > ub2 .and. y+1 < lb1 .and. y-1 > ub1 )then
                Bck2D(y,x)=nint(real(Bck2D(y,x+1)+ Bck2D(y,x-1)+ Bck2D(y+1,x)+ Bck2D(y-1,x))/4.0)
              end if
            end if
          end do
        end do
      end do

      return
    End Subroutine cut_peak



    Subroutine cuad_average(InDat2D,y,x,siz,avr)
      Integer,Dimension(:,:),         intent(in)  :: InDat2D
      Integer,                        intent(in)  :: y,x,siz
      integer,                        intent(out) :: avr
      !_________________________________________________________________________________ Local Vars
      integer                                     :: lb1, ub1, lb2, ub2
      integer                                     :: x_lp, y_lp
      integer                                     :: tot, cnt
      lb1=lbound(InDat2D,1)
      ub1=ubound(InDat2D,1)
      lb2=lbound(InDat2D,2)
      ub2=ubound(InDat2D,2)
      tot=0
      cnt=0
      do x_lp = x-siz, x+siz
        do y_lp = y-siz, y+siz
          if( y_lp > lb1 .and. y_lp < ub1 .and. x_lp > lb2 .and. x_lp < ub2 )then
            tot=tot+InDat2D(y_lp,x_lp)
            cnt=cnt+1
          end if
        end do
      end do
      avr = nint( real(tot) / real(cnt) )

      return
    End Subroutine

    ! Arrange_PeakList()
    ! starting from a mask of the maximums of experimental spots,
    ! Creates an ordered list of reflections with their coordinates
    !--- Comment by JRC
    ! This subroutine is not suitable when the peaks have been obtained with the threshold algorithm.
    ! The peak list is destroyed here because reallocation of p_peak. An alternative will be constructed for
    ! integration using the inertia tensor information
    Subroutine Arrange_PeakList(InDat2D,Msk2D,p_peak,pcount,F_par,bck2D)

      Integer,Dimension(:,:),                  intent(in)  :: InDat2D ! 2D Pattern
      integer(kind=1),Dimension(:,:),          intent(in)  :: Msk2D   ! 2D Mask (0 = Background, 1 = maximum, 2 = nodal maximum, 3 = peak area but not maximum),
      type(Peak_Type),dimension(:),allocatable,intent(out) :: p_peak  ! Experimental peaks extracted from the mask.
      integer ,                                intent(out) :: pcount  ! Number of Experimental peaks
      Type(Finding_Peaks_Parameters_Type),     intent(in)  :: F_par
      Integer,Dimension(:,:),optional,         intent(in)  :: Bck2D
      !----
      integer       :: i,j      ! cycle's control variables
      integer       :: y,x      ! coordinates of the experimental maximum
      integer       :: lb1,ub1,lb2,ub2

      type(Peak_Type) :: tmp_p    !Temporal variable used for swapping
      lb1=lbound(Msk2D,1)
      ub1=ubound(Msk2D,1)
      lb2=lbound(Msk2D,2)
      ub2=ubound(Msk2D,2)

      pcount=0
      do y=lb1,ub1
        do x=lb2,ub2
          if ( Msk2D(y,x) == 1 .or. Msk2D(y,x) == 2 ) pcount=pcount+1
        end do
      end do
      if( pcount <= 0 ) return

      if (allocated(p_peak)) deallocate (p_peak)
      allocate(p_peak(1:pcount))
      pcount=0

      if(present(bck2D)) then
        write(unit=*,fmt="(a)") ' => Integrating'
        !call ini_p_bar(lb1,ub1)
        do y=lb1,ub1
          !call update_p_bar()
          call prog_bar(lb1,y,ub1, 'Integrating')
          do x=lb2,ub2
            if ( Msk2D(y,x) == 1 .or. Msk2D(y,x) == 2 ) then
              pcount=pcount+1
              p_peak(pcount)%pos=(/y,x/)
              call integrated_intensity(InDat2D,Msk2D,bck2D,p_peak(pcount),Fnd_par=F_par)
            end if
          end do
        end do
        !call end_p_bar()
        write(unit=*,fmt="(a)") " => Done!"
      else
        do y=lb1,ub1
          do x=lb2,ub2
            if ( Msk2D(y,x) == 1 .or. Msk2D(y,x) == 2 ) then
              pcount=pcount+1
              p_peak(pcount)%pos=(/y,x/)
              p_peak(pcount)%intensity=InDat2D(y,x)
            end if
          end do
        end do
      end if
      if (pcount>=2) then
        ! organising in order of intensities  !This should be eliminated
        do i=1 ,pcount-1,1
          do j=i+1,pcount,1
            if (p_peak(i)%intensity < p_peak(j)%intensity) then
              tmp_p=p_peak(j)
              p_peak(j)=p_peak(i)
              p_peak(i)=tmp_p
            end if
          end do
        end do
      end if

      return
    End Subroutine Arrange_PeakList


!    subroutine integrated_intensity(InDat2D,Msk2D,Bck2D,Ypos,Xpos,Intens,sig,Fnd_par,rad_cut,edg_cut)
    Subroutine Integrated_Intensity(InDat2D,Msk2D,Bck2D,peak,Fnd_par,rad_cut,edg_cut)

      Integer,Dimension(:,:),                         intent(in)  :: InDat2D    ! 2D Pattern
      integer(kind=1),Dimension(:,:),                 intent(in)  :: Msk2D
      Integer,Dimension(:,:),                         intent(in)  :: Bck2D
      Type(Peak_Type),                            intent(in out)  :: peak
      Type (Finding_Peaks_Parameters_Type), optional, intent(in)  :: Fnd_par
      integer,                              optional, intent(in)  :: rad_cut
      integer,                              optional, intent(in)  :: edg_cut
      !local variables
      real    :: Ypos, Xpos
      real    :: Intens
      real    :: sig
      integer :: lb1, ub1, lb2, ub2
      integer :: x_ini, x_fin, y_ini, y_fin
      integer :: x_ini_n, x_fin_n, y_ini_n, y_fin_n
      integer :: dp, dp_cuad, dx, dy, edg, dsq
      integer :: x, y, dif, xn,yn
      integer :: Bck_Vol, shar_pix, Ar_cut, Ar_bord, Per_cut , Per_cut1
      real    :: Factr, Total_d, Local_D, d
      logical :: inside

      Ypos = peak%pos(1)
      Xpos = peak%pos(2)
      lb1=lbound(Bck2D,1)
      ub1=ubound(Bck2D,1)
      lb2=lbound(Bck2D,2)
      ub2=ubound(Bck2D,2)

      edg=5
      dp=10
      if(present(Fnd_par)) then
        if(Fnd_par%L_siz <= 0)then
          dp=Fnd_par%pk_ar*3
        else
          dp=Fnd_par%pk_ar*Fnd_par%L_siz
        end if
      end if
      if(present(rad_cut)) dp=rad_cut
      if(present(edg_cut)) edg=edg_cut

      dp_cuad=dp*dp
      x_ini=Xpos-dp
      x_fin=Xpos+dp
      y_ini=Ypos-dp
      y_fin=Ypos+dp

      if( x_ini < lb2 )  x_ini=lb2
      if( x_fin > ub2 )  x_fin=ub2
      if( y_fin > ub1 )  y_fin=ub1
      if( y_ini < lb1 )  y_ini=lb1

      Intens =0.0
      shar_pix =0
      Bck_Vol =0
      Ar_cut =0
      do x=x_ini,x_fin
        do y=y_ini,y_fin
          dx=x-Xpos
          dy=y-Ypos
          dsq=dx*dx+dy*dy
          if( dsq < dp_cuad )then
            dif=InDat2D(y,x)-Bck2D(y,x)
            Bck_Vol = Bck_Vol + Bck2D(y,x)
            Ar_cut = Ar_cut + 1
            x_ini_n=x-dp
            x_fin_n=x+dp
            y_ini_n=y-dp
            y_fin_n=y+dp
            if( x_ini_n < ub2 .or. x_fin_n > ub2 .or. y_fin_n > ub1 .or. y_ini_n < lb1 )then
              inside=.false.
            else
              inside=.true.
            end if
            Factr=1.0
            if( inside )then
              Total_d=0.0
              Local_d=0.0
              do xn=x_ini_n,x_fin_n
                do yn=y_ini_n,y_fin_n
                  dx=xn-x
                  dy=yn-y
                  if( dsq < dp_cuad )then
                    if( Msk2D(yn,xn)==1 .or. Msk2D(yn,xn)==2 )then
                      d=real(dp)-sqrt(real(dsq))
                      if( xn == Xpos .and. yn == Ypos )then
                        Local_D = d
                      else
                        shar_pix=shar_pix+1
                      end if
                      Total_d=Total_d+d
                    end if
                  end if
                end do
              end do
              if( Local_D /= 0.0 .and. Total_D /= 0.0 )then
                Factr=Local_D/Total_D
              else
                Factr=1.0
              end if
            end if
            Intens=Intens + real(dif)*Factr
          end if
        end do
      end do

      Ar_cut = Ar_cut + shar_pix
      if (edg > 1.0) then
        Per_cut = real(edg) * pi * 4.0 * sqrt( real(Ar_cut)/pi )
      else
        Per_cut = pi* 2.0 * sqrt(real(Ar_cut) / pi)
      end if

      sig=sqrt(Intens + ( 1.0 + real(Ar_cut)/real(Per_cut) ) * real(Bck_Vol) )

      peak%Intensity=Intens
      peak%sigma=sig

      return
    End Subroutine Integrated_Intensity


      !   call Mask_Trt_Side_Peak(xmin,ymin,xmax,ymax,Data2D(:,:,i_img),Mask2D(:,:,i_img),Fnd_param)
    Subroutine Mask_Trt_Side_Peak(xmin,ymin,xmax,ymax,Data2D,Mask2D,Fnd_par)
      real,                                 intent(in)  :: xmin,ymin ! lower left corner of the viewing area in pixels
      real,                                 intent(in)  :: xmax,ymax ! uper right corner of the viewing area in pixels
      Integer,Dimension(:,:),               intent(in)  :: Data2D    ! Converted data
      Type (Finding_Peaks_Parameters_Type), intent(in)  :: Fnd_par
      integer(kind=1),Dimension(:,:),       intent(out) :: Mask2D
      ! 2D Mask (0 = Background, 1 = maximum, 2 = nodal maximum, 3 = peak area but not maximum, 4 = rest of cuting area)
      !---- Local Variables ----!
      logical                              :: slop
      integer                              :: dp,dp_cuad,ytm1,ytm2,xtm1,xtm2,x,y
      integer                              :: dx,dy
      integer                              :: x_ps,y_ps
      integer                              :: lb1,ub1,lb2,ub2

      lb1=lbound(Mask2D,1)
      ub1=ubound(Mask2D,1)
      lb2=lbound(Mask2D,2)
      ub2=ubound(Mask2D,2)

      !write(unit=*,fmt="(a)") '  Mask Pk side Trt'

      do x=nint(xmin),nint(xmax)
        do y=nint(ymin),nint(ymax)
          if( Mask2D(y,x) == 3 .or. Mask2D(y,x) == 4 ) Mask2D(y,x)=0
        end do
      end do

      if(Fnd_par%L_siz <= 0)then
          dp=Fnd_par%pk_ar*3
      else
          dp=Fnd_par%pk_ar*Fnd_par%L_siz
      end if

      dp_cuad=dp*dp

      do x=nint(xmin),nint(xmax)
        do y=nint(ymin),nint(ymax)
          if( Mask2D(y,x) == 1 .or. Mask2D(y,x) == 2 )then

            ytm1=y-dp
            ytm2=y+dp
            xtm1=x-dp
            xtm2=x+dp
            if( ytm1 < lb1 ) ytm1=lb1
            if( ytm2 > ub1 ) ytm2=ub1
            if( xtm1 < lb2 ) xtm1=lb2
            if( xtm2 > ub2 ) xtm2=ub2

            do x_ps=xtm1,xtm2
              do y_ps=ytm1,ytm2
                if( Mask2D(y_ps,x_ps) == 0 )then
                  dx=x_ps-x
                  dy=y_ps-y
                  if( dx*dx+dy*dy < dp_cuad )  Mask2D(y_ps,x_ps)=4
                end if
              end do
            end do
          end if
        end do
      end do
      do x=nint(xmin),nint(xmax)
        do y=nint(ymin),nint(ymax)
          if( Mask2D(y,x) == 4 )then
            call slope_check(x,y,xmin,ymin,xmax,ymax,Data2D,Fnd_par,slop)
            if( slop ) Mask2D(y,x)=3
          end if
        end do
      end do

      !write(unit=*,fmt="(a)") '  End Pk side Trt'
      return
    End Subroutine Mask_Trt_Side_Peak

    Subroutine Peak_Find(xmin,ymin,xmax,ymax,Data2D,Mask2D,ExclR,Fnd_par,Peaks)
      real,                                 intent(in)  :: xmin,ymin 		! lower left corner of the viewing area in pixels
      real,                                 intent(in)  :: xmax,ymax 		! uper right corner of the viewing area in pixels
      Integer,Dimension(:,:),               intent(in)  :: Data2D    		! Converted data
      integer(kind=1),Dimension(:,:),       intent(out) :: Mask2D    		! Mask Data (0 = Background, 1 = maximum, 2 = nodal maximum, 3 = peak area but not maximum)
      Type(Excluded_Regions_Type),          intent(in)  :: ExclR
      Type(Finding_Peaks_Parameters_Type),  intent(in)  :: Fnd_par
      Type(Peak_List_Type),                 intent(out) :: Peaks
      !---- Local Variables ----!
      logical                              :: maxpt
      integer                              :: x,y,n
      logical                              :: fnd
      logical, Dimension(:,:), allocatable :: Mask2D_tmp
      integer                              :: coln,rown
      integer                              :: lb1,ub1,lb2,ub2

      lb1=lbound(Mask2D,1)
      ub1=ubound(Mask2D,1)
      lb2=lbound(Mask2D,2)
      ub2=ubound(Mask2D,2)
      maxpt=.false.
      allocate (Mask2D_tmp(lb1:ub1,lb2:ub2))
      write(*,*) "D_Min =", Fnd_par%d_min
      write(*,*) "Pk_ar = ", Fnd_par%pk_ar
      write(*,*) "M_Pk = ", Fnd_par%m_pk
      write(*,*) "Boundaries (xmin, ymin, xmax, ymax)", xmin, ymin, xmax, ymax

      Mask2D( nint(ymin)+1:nint(ymax)-1 , nint(xmin)+1:nint(xmax)-1) = 0
      Mask2D_tmp=.false.

   !   do x=nint(xmin)+1,nint(xmax)-1
   !     do y=nint(ymin)+1,nint(ymax)-1
   !       if(In_Rect_Excluded_Region(y,x,ExclR)) Mask2D(y,x)=-1     !Excluded regions
   !       if(In_Circ_Excluded_Region(y,x,ExclR)) Mask2D(y,x)=-1
   !     end do
   !   end do

      do x=nint(xmin),nint(xmax)
        do y=nint(ymin),nint(ymax)
          if (Data2D(y,x) > 5 .and. Mask2D(y,x) /= -1  ) then
            call slope_check(x,y,xmin,ymin,xmax,ymax,Data2D,Fnd_par,maxpt)
          end if
          if (maxpt) then
            Mask2D(y,x)=1  !Many pixels are rised as peaks
            Mask2D_tmp(y,x)=.true.
          end if
        end do
      end do
      Mask2D( nint(ymin)+1:nint(ymax)-1 , nint(xmin)+1:nint(xmax)-1)=0   !Nullify the mask (stored provisionally in  Mask2D_tmp

      do y=nint(ymin)+1,nint(ymax)-1
        do x=nint(xmin)+1,nint(xmax)-1
          if (Mask2D_tmp(y,x)) then
            fnd=.false.
            do coln=x-Fnd_par%d_min, x+Fnd_par%d_min, 1
              do rown=y-Fnd_par%d_min, y+Fnd_par%d_min, 1
                if( coln > lb2 .and. coln < ub2 .and. rown > lb1 .and. rown < ub1 )then
                    if( ( Data2D(rown,coln) > Data2D(y,x) .and. (rown /= y .or. coln /= x) ) &
                   .or. ( Data2D(rown,coln) == Data2D(y,x) .and. (rown > y .or. (rown == y .and. coln > x) ) ) )then
                        fnd=.true.
                    end if
                end if
              end do
            end do

            if( .not. fnd ) then
                 Mask2D(y,x)=1
            end if
          end if
        end do
      end do
    !  do x=nint(xmin)+1,nint(xmax)-1
    !    do y=nint(ymin)+1,nint(ymax)-1
    !      if(In_Rect_Excluded_Region(y,x,ExclR)) Mask2D(y,x)=-1     !Excluded regions
    !      if(In_Circ_Excluded_Region(y,x,ExclR)) Mask2D(y,x)=-1
    !    end do
    !  end do

      !Now allocate the output list of peaks
      !Count the number of peaks found
      n=0
      do x=nint(xmin)+1,nint(xmax)-1
        do y=nint(ymin)+1,nint(ymax)-1
          if(Mask2D(y,x) == 1) n=n+1
        end do
      end do

      Peaks%N_peaks=n
      if(allocated(Peaks%Peak)) deallocate(Peaks%Peak)
      allocate(Peaks%Peak(n))
      n=0
      do x=nint(xmin)+1,nint(xmax)-1
        do y=nint(ymin)+1,nint(ymax)-1
          if(Mask2D(y,x) == 1) then
            n=n+1
            Peaks%Peak(n)%pos=(/y,x/)
            Peaks%Peak(n)%Intensity=Data2D(y,x)
          end if
        end do
      end do

      return
    End Subroutine Peak_Find


    Subroutine Slope_Check(x,y,xmin,ymin,xmax,ymax,Data2D,Fnd_par,maxpt)
      Integer,                             intent(in)    :: x,y
      real,                                intent(in)    :: xmin,ymin 		! lower left corner of the viewing area in pixels
      real,                                intent(in)    :: xmax,ymax 		! uper right corner of the viewing area in pixels
      Integer,Dimension(:,:),              intent(in)    :: Data2D    		! Converted data
      Type (Finding_Peaks_Parameters_Type),intent(in)    :: Fnd_par
      logical,                             intent(out)   :: maxpt
      !---- Local Variables ----!

      integer :: cnt,dp,ytm1,ytm2,xpr,xtm1,xtm2,ypr
      integer :: suma,prom,oldprom

      maxpt=.TRUE.
      oldprom=Data2D(y,x)
      do dp=1,Fnd_par%pk_ar
        suma=0
        cnt=0
        ytm1=y-dp
        ytm2=y+dp
        do xpr=x-dp,x+dp
          if (ytm1 > ymin .and. xpr > xmin .and. xpr < xmax) then
            suma=suma+Data2D(ytm1,xpr)
            cnt=cnt+1
          end if

          if (ytm2 < ymax .and. xpr > xmin .and. xpr < xmax) then
            suma=suma+Data2D(ytm2,xpr)
            cnt=cnt+1
          end if
        end do
        xtm1=x-dp
        xtm2=x+dp
        do ypr=y-dp+1,y+dp-1
          if (xtm1 > xmin .and. ypr > ymin .and. ypr < ymax) then
            suma=suma+Data2D(ypr,xtm1)
            cnt=cnt+1
          end if
          if (xtm2 < xmax .and. ypr > ymin .and. ypr < ymax) then
            suma=suma+Data2D(ypr,xtm2)
            cnt=cnt+1
          end if
        end do
        prom=suma/cnt
        if (oldprom <= prom + Fnd_par%m_pk) then
          maxpt=.FALSE.
          exit
        end if
        oldprom=prom
      end do

      return
    End Subroutine Slope_Check


    Subroutine Point_Nodals(xmin,ymin,xmax,ymax,Data2D,Mask2D,num,prior_pos)
      real,                           intent(in)      :: xmin,ymin 		! lower left corner of the viewing area in pixels
      real,                           intent(in)      :: xmax,ymax 		! uper right corner of the viewing area in pixels
      Integer, Dimension(:,:),        intent(in)      :: Data2D    		! Converted data
      integer(kind=1), Dimension(:,:),intent(in out)  :: Mask2D    		! Mask Data (0 = Background, 1 = maximum, 2 = nodal maximum, 3 = peak area but not maximum)
      integer,                        intent(in)      :: num          ! number of nodals to point out
      integer,                        intent(in)      :: prior_pos    ! describes options: options: 1 = prior intensity, 2 = prior loneliness, 3 =only intensity 4 = only loneliness)
      ! local variables
      integer                                         :: x, y
      integer(kind=1), Dimension(:,:), allocatable    :: new_Mask2D

      where(Mask2D(int(ymin):int(ymax),int(xmin):int(xmax)) == 2) Mask2D(int(ymin):int(ymax),int(xmin):int(xmax))=1

      allocate(new_Mask2D( lbound(Mask2D,dim=1) :ubound(Mask2D,dim=1) , lbound(Mask2D,dim=2):ubound(Mask2D,dim=2) ))
      new_Mask2D(int(ymin):int(ymax),int(xmin):int(xmax))=0
      write(unit=*,fmt='(a)')   '  options:'
      write(unit=*,fmt='(a)')   '  1 = prior intensity, 2 = prior loneliness, '
      write(unit=*,fmt='(a,i3)')'  3 = only intensity,  4 = only loneliness:  ', prior_pos

      if (prior_pos == 1) then
          call Find_Max_Nodals(xmin, ymin, xmax, ymax, Data2D, Mask2D, new_Mask2D, num*2, 16)
          call Find_Lone_Nodals(xmin, ymin, xmax, ymax, Data2D, new_Mask2D, Mask2D, num, .true.)
      else if (prior_pos == 2) then
          call Find_Lone_Nodals(xmin, ymin, xmax, ymax, Data2D, Mask2D, new_Mask2D, num*2, .false.)
          call Find_Max_Nodals(xmin, ymin, xmax, ymax, Data2D, new_Mask2D, Mask2D, num, 16)
      else if (prior_pos == 3) then
          call Find_Max_Nodals(xmin, ymin, xmax, ymax, Data2D, Mask2D, Mask2D, num, 16)
      else if (prior_pos == 4) then
          call Find_Lone_Nodals(xmin, ymin, xmax, ymax, Data2D, Mask2D, Mask2D, num, .false.)
      else
          write(unit=*,fmt="(a)") 'ERROR: no selection , nothing to do'
      end if

      return
    End Subroutine Point_Nodals

    Subroutine Find_Max_Nodals(xmin, ymin, xmax, ymax, Data2D, Mask2D, tmp_Mask2D, num, dst_prom)
      real,                                   intent(in)      :: xmin,ymin 		! lower left corner of the viewing area in pixels
      real,                                   intent(in)      :: xmax,ymax 		! uper right corner of the viewing area in pixels
      Integer, Dimension(:,:),                intent(in)      :: Data2D    		! Converted data
      integer(kind=1), Dimension(:,:),        intent(in)      :: Mask2D    		! Mask Data (0 = Background, 1 = maximum, 2 = nodal maximum, 3 = peak area but not maximum)
      integer(kind=1), Dimension(:,:),        intent(in out)  :: tmp_Mask2D    	! temporal result
      integer,                                intent(in)      :: num              ! number of nodals to point out
      integer, optional,                      intent(in)      :: dst_prom

      Integer, Dimension(:), allocatable              :: x_pn, y_pn, i_pn
      integer                                         :: x, y, n_ls, n_bkw
      integer                                         :: ps, xps, yps, its_ref
      integer                                         :: tot, cont
      allocate(x_pn(1:num))
      allocate(y_pn(1:num))
      allocate(i_pn(1:num))

      x_pn=-1
      y_pn=-1
      i_pn=-1
      do x=int(xmin),int(xmax),1
        do y=int(ymin),int(ymax),1
          if (Mask2D(y,x)==1 .or. Mask2D(y,x)==2) then
            if (present(dst_prom)) then
              tot=0
              cont=0
              do ps = 1, dst_prom*2 - 1, 1
                xps= x - dst_prom + ps
                yps= y - dst_prom
                if( xps>=xmin .and. xps<=xmax .and. yps>=ymin .and. yps<=ymax )then
                  tot = tot + Data2D(yps,xps)
                  cont=cont+1
                end if
                xps= x + dst_prom - ps
                yps= y + dst_prom
                if( xps>=xmin .and. xps<=xmax .and. yps>=ymin .and. yps<=ymax )then
                  tot = tot + Data2D(yps,xps)
                  cont=cont+1
                end if
                yps= y - dst_prom + ps
                xps= x - dst_prom
                if( xps>=xmin .and. xps<=xmax .and. yps>=ymin .and. yps<=ymax )then
                  tot = tot + Data2D(yps,xps)
                  cont=cont+1
                end if
                yps= y + dst_prom - ps
                xps= x + dst_prom
                if( xps>=xmin .and. xps<=xmax .and. yps>=ymin .and. yps<=ymax )then
                  tot = tot + Data2D(yps,xps)
                  cont=cont+1
                end if
              end do
              its_ref=Data2D(y,x) - nint(real(tot)/real(cont))
            else
              its_ref=Data2D(y,x)
            end if

            do n_ls=1, num, 1
              if( its_ref>=i_pn(n_ls) )then
                if( n_ls<num )then
                  do n_bkw=num, n_ls+1, -1
                    x_pn(n_bkw)=x_pn(n_bkw-1)
                    y_pn(n_bkw)=y_pn(n_bkw-1)
                    i_pn(n_bkw)=i_pn(n_bkw-1)
                  end do
                  x_pn(n_ls)=x
                  y_pn(n_ls)=y
                  i_pn(n_ls)=its_ref
                end if
                exit
              end if
            end do
          end if
        end do
      end do
      !write(unit=*,fmt="(a)") '  Maxima'
      do n_ls=1, num, 1
          !write(unit=*,fmt="(a,3i6)") 'x y i =', x_pn(n_ls), y_pn(n_ls), i_pn(n_ls)
          tmp_Mask2D(y_pn(n_ls),x_pn(n_ls)) = 2
      end do

      return
    End Subroutine Find_Max_Nodals

    Subroutine Find_Lone_Nodals(xmin, ymin, xmax, ymax, Data2D, Mask2D, tmp_Mask2D, num, another_mask)
      real,                                   intent(in)      :: xmin,ymin 		! lower left corner of the viewing area in pixels
      real,                                   intent(in)      :: xmax,ymax 		! uper right corner of the viewing area in pixels
      Integer, Dimension(:,:),                intent(in)      :: Data2D    		! Converted data
      integer(kind=1), Dimension(:,:),        intent(in)      :: Mask2D    		! Mask Data (0 = Background, 1 = maximum, 2 = nodal maximum, 3 = peak area but not maximum)
      integer(kind=1), Dimension(:,:),        intent(in out)  :: tmp_Mask2D    	! temporal result
      integer,                                intent(in)      :: num              ! number of nodals to point out
      logical,                                intent(in)      :: another_mask     ! use tmp_Mask2D when calculating the distance

      Integer, Dimension(:), allocatable              :: x_pn, y_pn, d_pn
      integer                                         :: x, y, n_ls, n_bkw
      integer                                         :: dst, x_dst, y_dst, fn_dst
      integer                                         :: siz, siz_x, siz_y
      allocate(x_pn(1:num))
      allocate(y_pn(1:num))
      allocate(d_pn(1:num))

      siz_x=xmax-xmin
      siz_y=ymax-ymin
      if (siz_x > siz_y) then
        siz = siz_x
      else
        siz = siz_y
      end if

      x_pn=-1
      y_pn=-1
      d_pn=-1
      do x=int(xmin),int(xmax),1
        do y=int(ymin),int(ymax),1
          if (Mask2D(y,x)==1 .or. Mask2D(y,x)==2) then
                                                      !here starts calculating the distance from the closest maximum
          lp_dst : do dst=1, siz, 1
              do x_dst=x-dst,x+dst,1
                do y_dst=y-dst,y+dst,2*dst
                  if( (y_dst/=y .or. x_dst/=x) .and. (x_dst>=xmin .and. x_dst<=xmax .and. y_dst>=ymin .and. y_dst<=ymax ) )then
                    if (another_mask) then
                      if (tmp_Mask2D(y_dst,x_dst)==1 .or. tmp_Mask2D(y_dst,x_dst)==2) then
                        fn_dst=dst
                        exit lp_dst
                      end if
                    else
                      if (Mask2D(y_dst,x_dst)==1 .or. Mask2D(y_dst,x_dst)==2) then
                        fn_dst=dst
                        exit lp_dst
                      end if
                    end if
                  end if
                end do
              end do
              do x_dst=x-dst,x+dst, 2*dst
                do y_dst=y-dst,y+dst, 1
                  if( (y_dst/=y .or. x_dst/=x) .and. (x_dst>=xmin .and. x_dst<=xmax .and. y_dst>=ymin .and. y_dst<=ymax ) )then
                    if (another_mask) then
                      if (tmp_Mask2D(y_dst,x_dst)==1 .or. tmp_Mask2D(y_dst,x_dst)==2) then
                        fn_dst=dst
                        exit lp_dst
                      end if
                    else
                      if (Mask2D(y_dst,x_dst)==1 .or. Mask2D(y_dst,x_dst)==2) then
                        fn_dst=dst
                        exit lp_dst
                      end if
                    end if
                  end if
                end do
              end do
            end do lp_dst
                                                      !here ends calculating the distance from the closest maximum
            do n_ls=1, num, 1
              if( fn_dst>=d_pn(n_ls) )then
                if( n_ls<num )then
                  do n_bkw=num, n_ls+1, -1
                    x_pn(n_bkw)=x_pn(n_bkw-1)
                    y_pn(n_bkw)=y_pn(n_bkw-1)
                    d_pn(n_bkw)=d_pn(n_bkw-1)
                  end do
                  x_pn(n_ls)=x
                  y_pn(n_ls)=y
                  d_pn(n_ls)=fn_dst
                end if
                exit
              end if
            end do
          end if
        end do
      end do
      !write(unit=*,fmt="(a)") ' dists'
      do n_ls=1, num, 1
        !write(unit=*,fmt="(a,3i6)") 'x y i =', x_pn(n_ls), y_pn(n_ls), d_pn(n_ls)
        tmp_Mask2D(y_pn(n_ls),x_pn(n_ls)) = 2
      end do

      return
    End Subroutine Find_Lone_Nodals

    Subroutine noise_out(xmin,ymin,xmax,ymax,siz,m,Data2D)

      real,                                 intent(in)  :: xmin,ymin 		! lower left corner of the viewing area in pixels
      real,                                 intent(in)  :: xmax,ymax 		! uper right corner of the viewing area in pixels
  		! Converted data

      integer,                              intent(in)  :: siz,m

      Integer,Dimension(:,:),           intent(in out)  :: Data2D

      !---- Local Variables ----!#
      integer(kind=1), Dimension(:,:), allocatable    :: M2D                         ! local 2D mask
      integer(kind=1), Dimension(:,:), allocatable    :: M2D_tmp                     ! local 2D tmp mask
      type (Finding_Peaks_Parameters_Type)    :: Fnd_par,Fnd_par_exld
      logical                                 :: maxpt,maxpt_exld
      integer                                 :: x,y
      integer                                 :: x_sn,y_sn
      integer                                 :: lb1,ub1,lb2,ub2
      integer                                 :: siz_i
      real                                    :: tot
      real                                    :: dst_up,dst_dw,dst_lf,dst_rt
      real                                    :: max_up, max_dw, max_lf, max_rt, tot_pond
      lb1=lbound(Data2D,1)
      ub1=ubound(Data2D,1)
      lb2=lbound(Data2D,2)
      ub2=ubound(Data2D,2)
      allocate (M2D(lb1:ub1,lb2:ub2))
      allocate (M2D_tmp(lb1:ub1,lb2:ub2))

      M2D = 0
      do siz_i=1,siz
        Fnd_par%m_pk=m
        Fnd_par%pk_ar=siz_i
        Fnd_par%d_min=2

        Fnd_par_exld%m_pk=m
        Fnd_par_exld%pk_ar=siz_i+1
        Fnd_par_exld%d_min=2

        do x=nint(xmin)+1,nint(xmax)-1
          do y=nint(ymin)+1,nint(ymax)-1
            if(M2D(y,x)/=-1)then
              call Slope_Check(x,y,xmin,ymin,xmax,ymax,Data2D,Fnd_par,maxpt)
              call Slope_Check(x,y,xmin,ymin,xmax,ymax,Data2D,Fnd_par_exld,maxpt_exld)
              if(maxpt .and. (.not.maxpt_exld)) M2D(y,x)=1
            end if
          end do
        end do
      end do

      M2D_tmp=0
      do x=nint(xmin)+1,nint(xmax)-1
        do y=nint(ymin)+1,nint(ymax)-1
          if (M2D(y,x)==1) then
            do x_sn=x-1,x+1,1
              do y_sn=y-1,y+1,1
                M2D_tmp(y_sn,x_sn)=M2D(y,x)
              end do
            end do
          end if
        end do
      end do
      M2D=M2D_tmp

      do x=nint(xmin)+1,nint(xmax)-1
        do y=nint(ymin)+1,nint(ymax)-1
          if(M2D(y,x)==1)then
            dst_up=0
            dst_dw=0
            dst_lf=0
            dst_rt=0
            tot=0

            do x_sn=x-1,nint(xmin)+1,-1
              if( M2D(y,x_sn)==0)then
                dst_lf=x-x_sn
                max_lf=Data2D(y,x_sn)
                exit
              end if
            end do
            do x_sn=x+1,nint(xmax)-1,1
              if( M2D(y,x_sn)==0)then
                dst_rt=x_sn-x
                max_rt=Data2D(y,x_sn)
                exit
              end if
            end do


            do y_sn=y-1,nint(ymin)+1,-1
              if( M2D(y_sn,x)==0)then
                dst_dw=y-y_sn
                max_dw=Data2D(y_sn,x)
                exit
              end if
            end do
            do y_sn=y+1,nint(ymax)-1,1
              if( M2D(y_sn,x)==0)then
                dst_up=y_sn-y
                max_up=Data2D(y_sn,x)
                exit
              end if
            end do

            if(dst_up/=0 .and.  dst_dw/=0 .and. dst_lf/=0 .and. dst_rt/=0  )then
              tot_pond = 1.0/dst_up + 1.0/dst_dw + 1.0/dst_lf + 1.0/dst_rt
              Data2D(y,x)=(max_up/dst_up + max_dw/dst_dw + max_lf/dst_lf + max_rt/dst_rt) / tot_pond
            else
              Data2D(y,x)=0
            end if
          end if
        end do
      end do
      return
    End Subroutine noise_out


    Subroutine offset_mov(dat_2d,ofst)
      integer, Dimension(:,:), intent(in out) :: dat_2d
      integer                , intent(in)     :: ofst
      integer, Dimension(:,:), allocatable    :: D2D_tmp
      integer     :: x,y,xpos,offset_px
      integer     :: Nrow,Ncol,lb1,lb2

      lb1=lbound(dat_2d,1)
      Nrow=ubound(dat_2d,1)
      lb2=lbound(dat_2d,2)
      Ncol=ubound(dat_2d,2)

      if (lb1 /= 1 .or. lb2 /= 1 ) then
        write(unit=*,fmt="(a)") ' => Error: image not starting at (1,1)'
        return
      end if
      allocate(D2D_tmp(Nrow,Ncol))
      offset_px=ofst
      do x=1,Ncol,1
        do y=1,Nrow,1
          xpos=x+offset_px
          if (xpos < 1) then
              xpos=xpos+Ncol
          else if (xpos > Ncol) then
              xpos=xpos-Ncol
          end if
          if (xpos < 1) then
              xpos=1
          else if (xpos > Ncol) then
              xpos=Ncol
          end if

          D2D_tmp(y,x)=dat_2d(y,xpos)
        end do
      end do
      dat_2d=D2D_tmp

      return
    End Subroutine offset_mov

    !!---- Subroutine Octagon_to_cylinder(H,V,datam, nrow_cyl, ncol_cyl, r_cyl, V_cyl)
    !!----  real,                                     intent(in)     :: H,V    !Horizontal and Vertical dimensions (mm) of the deployed octagon
    !!----  integer, Dimension(:,:), allocatable,     intent(in out) :: datam  !On input Image on the octagon, on output image on cylinder
    !!----  integer,                                  intent(in)     :: nrow_cyl, ncol_cyl  !number of pixels of the final image
    !!----  real,                                     intent(in)     :: r_cyl, V_cyl  !Radius and vertical dimension of the cylinder
    !!----
    !!----  This subroutine construct the projection by exploring the cyclindrical image
    !!----  and determining the indices of the pixels in the rectangular image. The advantage
    !!----  of this method is that the fill up of the the cylindrical image is granted.
    !!----  See documentation for an explanation of the different formulae.
    !!----
    !!----  Created: 29 July 2012 (JRC)
    !!----
    Subroutine Octagon_to_cylinder(H,V,datam, nrow_cyl, ncol_cyl, r_cyl, V_cyl)
      real,                                     intent(in)     :: H,V
      integer, Dimension(:,:), allocatable,     intent(in out) :: datam
      integer,                                  intent(in)     :: nrow_cyl, ncol_cyl
      real,                                     intent(in)     :: r_cyl, V_cyl
      !--- Local variables ---!
      integer, Dimension(:,:), allocatable    :: D2D_tmp
      real(kind=dp),parameter :: pi8=pi/8.0_dp, tanpi8=tan(pi8) !tanpi8=sqrt(2)-1
      real(kind=dp)           :: hypo_fact, xL, D_p  !, px, pz, cx, cz
      real(kind=dp)           :: alpha, h_half, v_half, H_rec,V_rec
      real(kind=dp)           :: x_midblock, nh_r,nh_c,nv_r,nv_c, tn
      real(kind=dp)           :: n_set, gamma_mid, rad_eqv, fv_fact,fh_fact
      integer                 :: ic,ir,jc,jr,ndet,nph
      integer                 :: Nrow,Ncol,lb1,lb2,irp,irm,jrp,jrm

      if(.not. allocated(datam)) then
        write(unit=*,fmt="(a)") ' => Error: the provided image is not allocated!'
        return
      end if
      lb1=lbound(datam,1)
      Nrow=ubound(datam,1)
      lb2=lbound(datam,2)
      Ncol=ubound(datam,2)

      if (lb1/=1 .or. lb2/=1 ) then
        write(unit=*,fmt="(a)") ' => Error: image not starting at (1,1)'
        return
      end if

      !Previous calculations with input data
      nh_r = real(Ncol-1,kind=dp)       !Ncol-1
      nv_r = real(Nrow-1,kind=dp)       !Nrow-1
      nh_c = real(Ncol_cyl-1,kind=dp)   ! ...
      nv_c = real(Nrow_cyl-1,kind=dp)
      H_rec = H/8.0_dp
      V_rec = V
      rad_eqv= nh_c/tpi
      fv_fact=0.5_dp*nv_c*V_rec/V_cyl  ! fv_fact = pz *  nv_r / (2.0_dp*cz)
      fh_fact= (R_cyl/H)*(nh_r/nv_r)   ! fh_fact =  cx * nh_c )/( px * nv_r* 2.0_dp*tpi )
      h_half= nh_r/16.0_dp             ! h=Hr/2 in pixels
      v_half= nv_r/2.0_dp              ! v=Vr/2 in pixels
      D_p = h_half/tanpi8              ! D in pixels
      nph= Ncol_cyl/8     !Notice this value is not the same as in the other algorithm

      allocate(D2D_tmp(Nrow_cyl,Ncol_cyl))
      D2D_tmp = 0

      do jc = 1, Ncol_cyl
        ndet=(jc-1)/nph + 1   !integer division
        n_set= 2*ndet-1
        gamma_mid = n_set* pi8
        x_midblock=   n_set*h_half
        jr=nint( D_p*tan(real(jc-1,kind=dp)/rad_eqv-gamma_mid )+ x_midblock) + 1
        if (jr < 1 .or. jr > Ncol) cycle
        xL = real(jr-1,kind=dp) - x_midblock !Notice that here it is jr
        hypo_fact = sqrt( D_p * D_p + xL * xL  )/fh_fact
        jrp=min(Ncol,jr+1); jrm=max(1,jr-1)
        do ic=1, Nrow_cyl
          tn= (1.0_dp-real(ic-1,kind=dp)/fv_fact)* hypo_fact
          ir = nint( 0.5_dp*(nv_r - tn) )+1
          if (ir < 1 .or. ir > Nrow) cycle
          irp=min(Nrow,ir+1); irm=max(1,ir-1)
          D2D_tmp(ic, jc) = (3*datam(irm,jr)+18*datam(ir,jr)+3*datam(irp,jr)+ &
                             3*datam(ir,jrm)+3*datam(ir,jrp)+3*datam(irp,jrp)+ &
                             3*datam(irm,jrm)+3*datam(irp,jrm)+3*datam(irm,jrp))/42
          !D2D_tmp(ic, jc) = datam(ir,jr) !A smoothing can be done here is wished
        end do
      end do

      deallocate(Datam)
      allocate(Datam(nrow_cyl, ncol_cyl))
      datam = D2D_tmp
      return
    End Subroutine Octagon_to_cylinder

    !!---- Subroutine cyclops_to_cylindr(dat_2d,n_of_det, tense)
    !!----   integer, Dimension(:,:), intent(in out) :: dat_2d
    !!----   integer                , intent(in)     :: n_of_det
    !!----   logical, optional      , intent(in)     :: tense
    !!----
    !!----   This subroutine is the Luiso's implementation
    !!----   (Corrected from the original)
    !!----   Is it a particular case of "Octagon_to_cylinder"? ... to be checked!
    !!----
    Subroutine cyclops_to_cylindr(dat_2d,n_of_det, tense)
      integer, Dimension(:,:), intent(in out) :: dat_2d
      integer                , intent(in)     :: n_of_det
      logical, optional      , intent(in)     :: tense
      integer, Dimension(:,:), allocatable    :: D2D_tmp
      logical                                 :: deform

      real(kind=8):: hypotenuse, opp_leg, ady_leg
      real(kind=8):: dx_ang, rad_eqv, x_lengdet, y_dist
      real(kind=8):: x_midblock, y_midblock
      integer     :: xrec,yrec, xcyl,ycyl
      integer     :: Nrow,Ncol, lb1,lb2


      real  (kind = 8 )      :: stretch

      lb1=lbound(dat_2d,1)
      Nrow=ubound(dat_2d,1)
      lb2=lbound(dat_2d,2)
      Ncol=ubound(dat_2d,2)

      if (lb1/=1 .or. lb2/=1 ) then
        write(unit=*,fmt="(a)") ' => Error: image not starting at (1,1)'
        return
      end if

      allocate(D2D_tmp(Nrow,Ncol))
      D2D_tmp = -1
      deform = .false.
      if(present(tense)) then
        if(tense)then
          deform=.true.
        end if
      end if

      y_midblock = Nrow / 2


      write(unit=*,fmt="(a,i4)") ' => Number of detectors: ',n_of_det
      x_lengdet = real(Ncol)/real(n_of_det)
      ady_leg = (   Ncol / (16.0 * ( sqrt(2.0) - 1 ) ) )       * (8.0/n_of_det)
      rad_eqv = ( (real(Ncol)) / ( pi * 2.0)  )                * (8.0/n_of_det)

      do xrec=1, Ncol, 1

        x_midblock = (real(int( real(xrec) / x_lengdet ))  + 0.5 ) * x_lengdet

        opp_leg = real(xrec) - x_midblock
        dx_ang = atan( opp_leg / ady_leg )

        xcyl=x_midblock + dx_ang * rad_eqv

        if (xcyl < 1) then
            xcyl = xcyl + Ncol
        else if (xcyl > Ncol) then
            xcyl = xcyl-Ncol
        end if

        hypotenuse = sqrt( opp_leg**2.0 + ady_leg**2.0 )
        do yrec=1, Nrow, 1
          y_dist = yrec - y_midblock
          if (deform) then
            ycyl = y_midblock + y_dist * ( rad_eqv /  hypotenuse) / 1.055
          else
            ycyl = y_midblock + y_dist * (rad_eqv /  hypotenuse )
          end if

          if (ycyl<1 .or. ycyl>Nrow) cycle

          if (ycyl < Nrow .and. xcyl < Ncol .and. yrec < Nrow .and. xrec  <  Ncol) then
            D2D_tmp(ycyl:ycyl+1,xcyl:xcyl+1) = (dat_2d(yrec,     xrec) + dat_2d(yrec + 1, xrec + 1) &
                                              + dat_2d(yrec + 1, xrec) + dat_2d(yrec,     xrec + 1)) / 4
          else
            D2D_tmp(ycyl,xcyl) = dat_2d(yrec, xrec)
         end if
        end do
      end do


      dat_2d = D2D_tmp
      return
    End Subroutine cyclops_to_cylindr


    Subroutine flip_2D(dat_2d,ver,hor)
      integer, Dimension(:,:), intent(in out) :: dat_2d
      logical, intent(in)                     :: ver,hor

      integer, Dimension(:,:), allocatable    :: D2D_tmp
      integer                                 :: ubi,ubj,lbi,lbj
      integer                                 :: i,j,k,l

      !Quit the procedure if no flip is required
      if( .not. ver .and.  .not. hor) return

      lbi=lbound(dat_2d,1)
      ubi=ubound(dat_2d,1)
      lbj=lbound(dat_2d,2)
      ubj=ubound(dat_2d,2)

      allocate(D2D_tmp(lbi:ubi,lbj:ubj))

      D2D_tmp=dat_2d

      do j=lbj, ubj
        do i=lbi, ubi, 1
          if(hor)then
              k=ubj-j+lbj
          else
              k=j
          end if
          if(ver)then
              l=ubi-i+lbi
          else
              l=i
          end if
          dat_2d(i,j)=D2D_tmp(l,k)
        end do
      end do

      return
    End Subroutine flip_2D

   Subroutine Invert_Intensities_2D(dat_2d)
      integer, Dimension(:,:), intent(in out) :: dat_2d
      integer                                 :: v_max

        v_max=MaxVal(dat_2d(:,:))
        dat_2d=v_max-dat_2d

      return
    End Subroutine

    Subroutine save_zoomed_img(x1,x2,y1,y2,dat2d,filnam)
      Integer,Dimension(:,:),      intent(in)  :: dat2d
      Integer,                     intent(in)  :: x1,x2,y1,y2
      Character(Len=*),            intent(in)  :: filnam
      ! local variables
      ! Changes to save the image in 16 bits (JRC)
      integer                                      :: log_uni, i, j
      integer(kind=2), dimension(:,:), allocatable :: short_Image
      integer                                      :: dx,dy, y,x

      dx=(x2-x1)+1
      dy=(y2-y1)+1

      write(unit=*,fmt="(5(a,i5))") ' => Saving in file: '//trim(filnam)//', the image from (',x1,',',y1,')  to (',x2,',',y2,')'

      !Create the image of 16 bits
      if(allocated(short_image)) deallocate(short_image)
      allocate(short_image(dy,dx))
      x=0
      do j=x1,x2
       x=x+1
       y=0
       do i=y1,y2
         y=y+1
         if(Dat2d(i,j) > 32767) then
           short_image(y,x) = Dat2d(i,j) - 65536
         else
           short_image(y,x) = Dat2d(i,j)
         end if
       end do
      end do

      !Save the image in 16 bits
      Call Get_Logunit(log_uni)
      OPEN(unit=log_uni, file=filnam, status="replace", access="stream", form="unformatted")
       write(unit=log_uni) dy,dx  !This occupy 8 bytes nrow x ncol
       write(unit=log_uni) short_image
      close(unit=log_uni)

      write(unit=*,fmt="(a,2i6)") ' => res[dx,dy](pixels)= ',dx,dy
      write(unit=*,advance='yes',fmt='(a)') char(13)
      write(unit=*,fmt="(a)") " => Result image in File: "//filnam
      return
    End Subroutine save_zoomed_img

    !!----  Subroutine Peak_Find_Threshold(Fnd_par, Data2D, Mask2D, ExclR,Peaks)
    !!----     Type(Finding_Peaks_Parameters_Type), intent(in)  :: Fnd_par
    !!----     Integer,Dimension(:,:),              intent(in)  :: Data2D ! Converted data
    !!----     integer(kind=1),Dimension(:,:),      intent(out) :: Mask2D ! Mask Data (0 = Background, 1 = maximum, 2 = nodal maximum, 3 = peak area but not maximum)
    !!----     Type(Excluded_Regions_Type),         intent(in)  :: ExclR
    !!----     Type(Peak_List_Type),                intent(out) :: Peaks
    !!----
    !!---- Subroutine adapted from Snail (program peakfind of D19, refactoring of prewash
    !!---- by Mike Turner). Here the background is not uniform in the image, a division
    !!---- in blocks is controlled by the user and a more efficient detection of peaks is
    !!---- done. Also the inertia tensors are calculated with respect to the centre of mass.
    !!---- instead of using absolute pixels. The two half-axes of the inertia tensor
    !!---- and the cosine of the angle of the axis along with the maximum half-axis
    !!---- is found with the x-axis are stored in a vector component of Peaks: (a,b,cosAx).
    !!----
    !!---- Created in August 2010 (JRC)
    !!---- Updated: January 2012  (JRC)
    Subroutine Peak_Find_Threshold(Fnd_par, Data2D, Mask2D, ExclR,Peaks)
      Type(Finding_Peaks_Parameters_Type), intent(in)  :: Fnd_par
      Integer,Dimension(:,:),              intent(in)  :: Data2D ! Converted data
      integer(kind=1),Dimension(:,:),      intent(out) :: Mask2D ! Mask Data (0 = Background, 1 = maximum, 2 = nodal maximum, 3 = peak area but not maximum)
      Type(Excluded_Regions_Type),         intent(in)  :: ExclR
      Type(Peak_List_Type),                intent(out) :: Peaks
      !---- Local Variables ----!
      !       Integer,dimension(Size(Data2D,1),Size(Data2D,2)) :: DT
      !      Integer,Dimension(Size(Data2D,1))  :: jmax,jmin

      Integer,dimension(:,:), allocatable :: DT
      Integer,Dimension(:),   allocatable :: jmax,jmin

      Real,   Dimension(2)               :: cg,xpos, wpos
      Real,   Dimension(2,2)             :: tens,tensg, ptens,ptensg
      Real,   Dimension(3)               :: vtens !Vector containing two eigenvalues and
      Logical :: pass                             !and the cosinus of the angle of the Inertia Tensor eigen x-axis
      Integer :: i,j,k,nprev,npks,imin,imax,n,m,npres,mpres,imov, &
                 jmov,mass,imass, isize, jsize, tmp, cnt, re, r2, &
                 j_min,j_max,netint, ii, jj, ie, je, jstep
      real    :: bg,sigma, d2m
      integer, dimension(Size(Data2D,2)*(Size(Data2D,1)+Fnd_par%blocksz)/Fnd_par%blocksz/Fnd_par%blocksz+1) :: threshold, ibg
      Type(Peak_List_Type) :: P_temp
      integer, parameter   :: max_peaks=15000
      integer, dimension(max_peaks) :: poi, qoi
      integer, dimension(:), allocatable :: indx

      !!  -> working around the commented lines that declares this variables
      allocate(DT(Size(Data2D,1),Size(Data2D,2)))
      allocate(jmax(Size(Data2D,1)))
      allocate(jmin(Size(Data2D,1)))


      allocate(P_temp%Peak(max_peaks))
       Mask2D=0
       ! Mask pixels in excluded regions including the pixels with zero intensity
       where ( Data2D == 0)  Mask2D = -1
       where ( Mask_Excl == -1)  Mask2D = -1   !This supposes that

     !  do n=1,ExclR%Nexc_Rect
     !   do j=ExclR%exc_rect(2,1,n), ExclR%exc_rect(2,2,n)
     !     do i=ExclR%exc_rect(1,1,n), ExclR%exc_rect(1,2,n)
     !       Mask2D(i,j) = -1
     !     end do
     !   end do
     !  end do
     !
     !  do n=1,ExclR%Nexc_Circ
     !   imin=ExclR%exc_circ(1,n)-ExclR%exc_circ(3,n)
     !   imax=ExclR%exc_circ(1,n)+ExclR%exc_circ(3,n)
     !   j_min=ExclR%exc_circ(2,n)-ExclR%exc_circ(3,n)
     !   j_max=ExclR%exc_circ(2,n)+ExclR%exc_circ(3,n)
     !   re= ExclR%exc_circ(3,n)*ExclR%exc_circ(3,n)
     !   do j=j_min,j_max
     !     do i=imin,imax
     !        r2= (i-ExclR%exc_circ(1,n))**2+(j-ExclR%exc_circ(2,n))**2
     !        if( r2 <= re) Mask2D(i,j) = -1
     !      end do
     !    end do
     !  end do
       isize = Size(Data2D,1)
       jsize = Size(Data2D,2)
       jstep=jsize/Fnd_par%blocksz
       ! open(unit=77,file="snail.deb",status="replace",action="write")
       ! write(77,"(a,4i5)")  " => Size(Data2D,1), Size(Data2D,2), blocksz, jstep: ", isize, jsize, blocksz, jstep
       ! write(77,"(a)")    "   k   k-c    i    j       ibg       threshold"

        ! Calculation of the local background array and threshold
       k=0
       do ie=1,isize-Fnd_par%blocksz+1,Fnd_par%blocksz
         do je=1,jsize-Fnd_par%blocksz+1,Fnd_par%blocksz
          k=k+1

          cnt=0; bg=0.0
          do ii=0,Fnd_par%blocksz-1
            i=ie+ii
            do jj=0,Fnd_par%blocksz-1
              j=je+jj
              if(Mask2D(i,j) /= -1) then
                 cnt=cnt+1
                 bg=bg+Data2D(i,j)
               end if
             end do
           end do
           if(cnt > 0) then
             bg=bg/real(cnt)  !Average counts per pixel, taken as average local background
             sigma=sqrt(bg)   !Standard deviation of the local background
             ibg(k)=Nint(bg)
             threshold(k)=Nint(bg + Fnd_par%CutOff*sigma+0.5)
           else
            ibg(k) = 0
            threshold(k)=0
           end if
          ! jj=((j-2)/Fnd_par%blocksz)+ ( (((i-2)/Fnd_par%blocksz)+1) -1)*jstep+1
          ! write(77,"(4i5,2i14)") k,jj,i,j,ibg(k),threshold(k)

         end do
       end do
       !close(unit=77)

       nprev = 3
       npks = 0
       DT=Data2D

       global:  Do
            jmax = 0
            jmin = jsize+1 ! just has to be larger than all possible values of j
            pass = .FALSE.

          peak: Do i=nprev,isize-2 ! do whole frame except border
            Do j=3,jsize-2
             If (Mask2D(i,j) == -1) cycle
             k=(j/Fnd_par%blocksz)+ ( ((i/Fnd_par%blocksz)+1) -1)*jstep+1
              If (DT(i,j) > threshold(k)) Then      ! found new peak point
                npks=npks+1                     ! increase number of peak points
                npres=i                         ! save start square
                mpres=j
                imin=i                          ! row of start of peak
                imax=i
                imov=0                          ! going along + y
                jmov=1                          ! "   "   "   "
                pass=.true.
                Exit peak
              End If
            End Do
          End Do peak
          if(npks == max_peaks) then
           npks=npks-1
           write(unit=*,fmt="(a,i6,a,i3)") &
           " => WARNING! maximum number of peaks reached! ",npks, " ... Increase the CutOff!, now at: ",Fnd_par%CutOff
           exit global
          end if
          If(.NOT. pass) Exit global            ! end of this frame (no peaks points found)

          Do
            k=(j/Fnd_par%blocksz)+ ( ((i/Fnd_par%blocksz)+1) -1)*jstep+1
            If (DT(i,j) > threshold(k)) Then
              If (jmax(i) < j) jmax(i)=j      ! record the end position for each row of peak
              If (jmin(i) > j) jmin(i)=j      ! record the start position for each row of peak
              If (imax < i) imax=i            ! record the vertical end position of the peak
              tmp = imov                      ! non-edge peak point, turn left
              imov= jmov
              jmov= -tmp
            Else
              tmp = imov
              imov= -jmov                     ! background,turn right
              jmov= tmp
            End If
            i=i+imov                          ! add move
            j=j+jmov
            If (i < 1 .or. j < 1 .or. i > isize .or. j > jsize) then
              !Write(unit=*,fmt="(a)") 'Warning: Trying to index outside Data2D'
              !Write(unit=*,fmt="(a)") 'Skiping this peak'
              npks=npks-1
              nprev=npres+1
              cycle global
            Endif

            If (i /= npres .or. j /= mpres) Cycle  ! Keep going if not back at the start of peak
            If (imov == -1 .or. jmov == 1) Exit    ! Did we walk in a loop?
                                                   ! Yes, then we have a reflection so exit
                                                   ! No, rogue pixel or horiz pixel line
          End Do

          ! Now munch it up
          nprev=npres
          mass=0                      ! Total points in contour
          imass=0
          cg=0.0                      ! Initialise c. of g.
          tens=0.0                    ! Initialise inertia tensor
          ptens=0.0
          wpos=0.0                    ! Weighted cdg

          Do i=imin,imax
            xpos(1)=Real(i)                ! Load element to XPOS(N)
            Do j=jmin(i),jmax(i)
             k=(j/Fnd_par%blocksz)+ ( ((i/Fnd_par%blocksz)+1) -1)*jstep+1
              If (DT(i,j) > threshold(k)) Then   ! found peak point
                xpos(2)=Real(j)              !
                mass=mass+1
                netint=DT(i,j)-Ibg(k)
                imass=imass+netint
                Do n=1,2
                  cg(n)=cg(n)+xpos(n)                    ! Form c. of g. sum
                  wpos(n)=wpos(n)+xpos(n)*real(netint)   ! Form weighted c. of g. sum
                  Do m=1,2                               ! Inertia tensor
                    tens(m,n)=tens(m,n)+xpos(m)*xpos(n)
                    ptens(m,n)=ptens(m,n)+xpos(m)*xpos(n)*real(netint)
                  End Do
                End Do
                DT(i,j) = 0                      ! remove peak point
                Mask2D(i,j)=3                    ! Pixel in the area of a peak
              End If
            End Do
          End Do
          ! Discard peaks having less than Fnd_par%pk_ar pixels
          if(mass < Fnd_par%pk_ar) then
            Do i=imin,imax
              Do j=jmin(i),jmax(i)
                 if(Mask2D(i,j) == 3) Mask2D(i,j)=0  !Cleaning up the mask for discarded peaks
              End Do
            End Do
            npks=npks-1
            cycle global
          end if
          cg=cg/Real(mass)       ! Normalise (Baricentre)
          wpos=wpos/Real(imass)  ! Normalise (Centroid)
          tens=tens/Real(mass)   ! Normalise (Geometric Inertia tensor)
          ptens=ptens/Real(imass)! Normalise (Mass Inertia tensor)

          Do n=1,2
            Do m=1,2
              tensg(n,m)=tens(n,m)-cg(n)*cg(m) !Inertia tensor w.r.t to Baricentre
              ptensg(n,m)=ptens(n,m)-wpos(n)*wpos(m)
            End Do
          End Do
          !Change i,j to x,z by swapping 11 and 22 indices
          bg=tensg(1,1)
          tensg(1,1)=tensg(2,2)
          tensg(2,2)=bg
          bg=ptensg(1,1)
          ptensg(1,1)=ptensg(2,2)
          ptensg(2,2)=bg

          i=nint(wpos(1)); j= nint(wpos(2))
          if (i < 1 .or. j < 1 .or. i > isize .or. j > jsize) then
           write(unit=*,fmt="(a)") "  WARNING: indices of peak outside the frame!  ... peak discarded"
           npks=npks-1
           cycle
          else
           Mask2D(i,j) = 1  !Maximum of the peak (Centroid)
          end if
          Call Diag_Itens(ptensg,vtens)
          P_temp%Peak(npks)%cdm=cg
          P_temp%Peak(npks)%pos=wpos
          P_temp%Peak(npks)%Itens=vtens
          P_temp%Peak(npks)%npix=mass
          P_temp%Peak(npks)%Intensity=imass
          !----------------------------------------------------------
          !       back for more..
          !----------------------------------------------------------
      End Do global

      if(npks == 0) then
          Peaks%N_peaks=npks
          return
      end if
      ! Treatment of peaks before creating the output list
      ! Remove peaks too close between them (conserving the most representatitive one)
      ! First order the peaks by position: close peaks are close in the list
      ! Eliminate also the peaks with zero or negative integrated intensity just after
      ! getting the pointers for ordering the peaks.
      do i=1,npks
        poi(i)=i
      end do
      call In_Sort(Nint(P_temp%Peak(:)%pos(1)),npks, poi, qoi )
      poi(1:npks)=qoi(1:npks)
      call In_Sort(Nint(P_temp%Peak(:)%pos(2)),npks, poi, qoi )
      poi(1:npks)=0
      d2m=real(Fnd_par%d_min*Fnd_par%d_min)

      !Now mark the peaks with negative intensity
      do i=1,npks
        if(P_temp%Peak(i)%Intensity < 1.0) then
           ii=nint(P_temp%Peak(i)%pos(1)); jj= nint(P_temp%Peak(i)%pos(2))
           poi(i)=1
           Mask2D(ii,jj)=0
        end if
      end do

      !open(unit=77,file="snail.deb",status="replace",action="write")
      do i=npks,2,-1
        n=qoi(i)
        !write(77,"(2f12.4,i8)")  P_temp%Peak(n)%pos, P_temp%Peak(n)%npix
        do j=i-1,1,-1
          m=qoi(j)
          wpos= P_temp%Peak(n)%pos - P_temp%Peak(m)%pos
          if (dot_product(wpos,wpos) < d2m ) then
             if( P_temp%Peak(n)%Intensity > P_temp%Peak(m)%Intensity) then
                poi(m)=1
                ii=nint(P_temp%Peak(m)%pos(1)); jj= nint(P_temp%Peak(m)%pos(2))
             else
                poi(n)=1
                ii=nint(P_temp%Peak(n)%pos(1)); jj= nint(P_temp%Peak(n)%pos(2))
             end if
             Mask2D(ii,jj)=0
          else
             exit
          end if
        end do
      end do
      n= sum(poi(1:npks))

      !Now allocate the output list of peaks
      Peaks%N_peaks=npks-n
      if(allocated(Peaks%Peak)) deallocate(Peaks%Peak)
      allocate(Peaks%Peak(Peaks%N_peaks))
      n = 0
      do i=1,npks
        if(poi(i) == 1) cycle
        n=n+1
        Peaks%Peak(n)= P_temp%Peak(i)
        !write(unit=*,fmt="(i6,i4,2f12.4,f14.0)") n, Peaks%Peak(n)%npix,  Peaks%Peak(n)%pos,  Peaks%Peak(n)%Intensity
      end do
      n=Peaks%N_peaks

      !Now re-copy the Peaks into P_temp to sort them by increasing intensity
      deallocate(P_temp%Peak)
      allocate(P_temp%Peak(n),indx(n))
      P_temp%Peak(1:n)=Peaks%Peak(1:n)
      call Sort(P_temp%Peak(:)%Intensity,n,indx)
      do i=1,n
        j=indx(i)
        Peaks%Peak(n-i+1)= P_temp%Peak(j)
      end do

      !close(unit=77)
      return
    End Subroutine Peak_Find_Threshold

    Subroutine Diag_Itens(itens,vtens)
      real, dimension(2,2), intent(in) :: itens
      real, dimension(3),  intent(out) :: vtens
      !--- Local variables ---!
      real :: a,b,c
      b=(itens(1,1)+itens(2,2))
      c=(itens(1,1)-itens(2,2))
      c=c*c-4.0*itens(1,2)*itens(1,2)
      if(abs(itens(1,2)) < 1.0e-06) then
        if(itens(1,1) > itens(2,2)) then
          vtens(1)= itens(2,2)
          vtens(2)= itens(1,1)
          vtens(3)=0.0
        else
          vtens(1)= itens(1,1)
          vtens(2)= itens(2,2)
          vtens(3)=1.0
        end if
      else
        if(c <= 0.0) then
          vtens(1:2)=0.5*b
          vtens(3) = 1.0
        else
          c=sqrt(c)
          vtens(1)=0.5*(b-c)  !long axis
          vtens(2)=0.5*(b+c)
          c= (vtens(1)-itens(2,2))/itens(1,2)
          vtens(3)= c/sqrt(1.0+c*c)
        end if
      end if
      !Convert to semi-axes a,b
       vtens(1)=sqrt(1.0/vtens(1))
       vtens(2)=sqrt(1.0/vtens(2))
      return
    End Subroutine Diag_Itens

    Subroutine Arrange_Double_PeakList(InDat2D,Msk2D,ppeak_1,pcount_1,ppeak_2,pcount_2)
      Integer,Dimension(:,:),                     intent(in)    :: InDat2D      ! 2D Pattern
      integer(kind=1),Dimension(:,:),             intent(in)    :: Msk2D        ! 2D Mask (0 = Background, 1 = maximum, 2 = nodal maximum, 3 = peak area but not maximum),
      type(Peak_Type),dimension(:), allocatable,  intent(out)   :: ppeak_1      ! Experimental peaks
      integer,                                    intent(out)   :: pcount_1     ! Number of Experimental peaks
      type(Peak_Type),dimension(:), allocatable,  intent(out)   :: ppeak_2      ! Experimental peaks
      integer,                                    intent(out)   :: pcount_2     ! Number of Experimental peaks
      !---- Local Variables ----!
      integer       :: i,j,x,y       ! cycle control vars
      type (Peak_Type) :: tmp_p      !Temporal variable used for swapping
      ! Experimental peaks
      integer         :: lb1,ub1,lb2,ub2

      lb1=lbound(Msk2D,1)
      ub1=ubound(Msk2D,1)
      lb2=lbound(Msk2D,2)
      ub2=ubound(Msk2D,2)

      pcount_1=0
      do y=lb1,ub1
        do x=lb2,ub2
          if ( Msk2D(y,x)==1 ) pcount_1=pcount_1+1
        end do
      end do

      if (allocated(ppeak_1)) deallocate (ppeak_1)
      allocate(ppeak_1(1:pcount_1))

      pcount_1=0
      do y=lb1,ub1
        do x=lb2,ub2
          if ( Msk2D(y,x)==1 ) then
            pcount_1=pcount_1+1
            ppeak_1(pcount_1)%pos=(/y,x/)
            ppeak_1(pcount_1)%intensity=InDat2D(y,x)
          end if
        end do
      end do

      if (pcount_1 >= 2) then
        ! organising in order of intensities !Should be removed
        do i=1 ,pcount_1-1,1
          do j=i+1,pcount_1,1
            if (ppeak_1(i)%intensity < ppeak_1(j)%intensity) then
              tmp_p=ppeak_1(j)
              ppeak_1(j)=ppeak_1(i)
              ppeak_1(i)=tmp_p
            end if
          end do
        end do

      end if

      !  now the same but for mask = 2

      pcount_2=0
      do y=lb1,ub1
        do x=lb2,ub2
          if ( Msk2D(y,x)==2 ) pcount_2=pcount_2+1
        end do
      end do

      if (allocated(ppeak_2)) deallocate (ppeak_2)
      allocate(ppeak_2(1:pcount_2))

      pcount_2=0
      do y=lb1,ub1
        do x=lb2,ub2
          if ( Msk2D(y,x)==2 ) then
            pcount_2=pcount_2+1
            ppeak_2(pcount_2)%pos=(/y,x/)
            ppeak_2(pcount_2)%intensity=InDat2D(y,x)
          end if
        end do
      end do

      if (pcount_2 >= 2) then
        !  organising in order of intensities  !To be removed
        do i=1 ,pcount_2-1,1
          do j=i+1,pcount_2,1
            if (ppeak_2(i)%intensity < ppeak_2(j)%intensity) then
              tmp_p=ppeak_2(j)
              ppeak_2(j)=ppeak_2(i)
              ppeak_2(i)=tmp_p
            end if
          end do
        end do

      end if

      return
    End Subroutine Arrange_Double_PeakList
    !
    !
    Subroutine Check_Limits(cl,cl2,rw,x_mini,x_maxi,y_maxi)
        real    ,  intent(in)       :: x_mini,x_maxi,y_maxi             ! lower left corner of the viewing area in pixels, uper lim
        integer ,  intent(in out)   :: cl,cl2,rw

        if( cl  < x_mini )  cl=x_mini
        if( cl2 > x_maxi ) cl2=x_maxi
        if( rw  > y_maxi )  rw=y_maxi

    return
    End Subroutine Check_Limits

    !SUBROUTINE calc_curv_points()
    !calculates the XY position of Bezier curve points
    !and returns them in a array data.
    Subroutine calc_curv_points(xm,ym,x1,y1,x2,y2,xp,yp)
      real                            , intent(in):: xm,ym,x1,y1,x2,y2      ! coordinates of: mouse pointer, start, end
      real, dimension(:),allocatable , intent(out):: xp,yp                  ! array that stores the coordinates of the curve
      integer                                     :: siz                    ! size of the array
      integer                                     :: i                      ! cycle control vars

      real                                        :: rl_step                ! goes from 0.0 to 1.0
      real                                        :: dx1,dx2,dy1,dy2,dx,dy  ! delta X , delta Y  : of 3 lines
      real                                        :: xl1,yl1,xl2,yl2        ! coordinates of extremes of vector

      siz=20
      allocate(xp(0:siz))
      allocate(yp(0:siz))

      dx1=xm-x1
      dy1=ym-y1
      dx2=x2-xm
      dy2=y2-ym
      xp(1)=x1
      yp(1)=y1
      xp(siz)=x2
      yp(siz)=y2

      do i=0,siz
        rl_step=real(i)/real(siz)
        xl1=x1+dx1*rl_step
        yl1=y1+dy1*rl_step
        xl2=xm+dx2*rl_step
        yl2=ym+dy2*rl_step

        dx=xl2-xl1
        dy=yl2-yl1
        xp(i)=xl1+dx*rl_step
        yp(i)=yl1+dy*rl_step
      end do
    return
    end subroutine calc_curv_points


    !!----
    !!---- Subroutine Read_Excluded_Regions(arkive,ExclR,ok,lun)
    !!----    character(len=*),            intent(in) :: arkive
    !!----    Type(Excluded_Regions_Type), intent(out):: ExclR
    !!----    logical,                     intent(out):: ok
    !!----    integer, optional,           intent(in) :: lun
    !!----
    !!----    Subroutine reading excluded regions from the file "arkive"
    !!----    The file may be a normal CFL file or an instrument file.
    !!----
    !!---- Created: June - 2010 (JRC)
    !!---- Updated: September - 2012 (JRC)
    !!

    Subroutine Read_Excluded_Regions(arkive,ExclR,ok,lun)
      character(len=*),            intent(in) :: arkive
      Type(Excluded_Regions_Type), intent(out):: ExclR
      logical,                     intent(out):: ok
      integer, optional,           intent(in) :: lun
      !---- Local Variables ----!
      Type(File_List_Type):: fich
      Integer             :: i,j,k,n, ier, iou, n_row,n_col
      real                :: aux1,aux2
      character(len=132)  :: line
      logical             :: maxij
      !
      !--- Look for Excluded Regions ---!
      !
      call File_to_filelist(arkive,fich)
      i=0
      iou=6
      ok=.false.
      maxij=.false.
      ExclR%imax=0; ExclR%jmax=0
      if(present(lun)) iou=lun
      ExclR%Nexc_Rect=0
      ExclR%Nexc_Circ=0
      write(unit=iou,fmt="(/,/,a)")  " => Information about Excluded Regions read in file "//trim(arkive)
      write(unit=iou,fmt="(a)")        "   "

      do n=1,fich%nlines
        i=i+1
        if(i > fich%nlines) exit
        line=adjustl(l_case(fich%line(i)))

        if(line(1:7) == "dim_xzp") then
          read(unit=line(8:),fmt=*,iostat=ier)  aux1,aux2, n_col, n_row
          if(ier == 0) then
            ExclR%imax = n_row
            ExclR%jmax = n_col
            maxij=.true.
          end if
        end if

        if(line(1:9) == "excl_rect") then
          j=index(fich%line(i)," ")
          read(unit=fich%line(i)(j:),fmt=*,iostat=ier)   ExclR%Nexc_Rect
          if(ier /= 0) then
            write(unit=*,fmt="(a)") " => Warning! Error reading number of rectangular excluded regions in "//trim(arkive)
            return
          end if
          if(allocated(ExclR%Exc_Rect)) deallocate(ExclR%Exc_Rect)
          allocate(ExclR%Exc_Rect(2,2,ExclR%Nexc_Rect))

          if(allocated(ExclR%eliptic)) deallocate(ExclR%eliptic)
          allocate(ExclR%eliptic(ExclR%Nexc_Rect))
          ExclR%eliptic = .false.

          do k=1, ExclR%Nexc_Rect
            i=i+1
            read(unit=fich%line(i),fmt=*,iostat=ier) ExclR%Exc_Rect(:,1,k), ExclR%Exc_Rect(:,2,k)
            if(ier /= 0) then
              write(unit=*,fmt="(a,i3,a)") " => Error reading rectangular excluded region: ",k, " in file "//trim(arkive)
              return
            end if
            if(maxij) then   !Checking the high
              if(ExclR%Exc_Rect(1,2,k) > ExclR%imax) ExclR%Exc_Rect(1,2,k)=ExclR%imax
              if(ExclR%Exc_Rect(2,2,k) > ExclR%jmax) ExclR%Exc_Rect(2,2,k)=ExclR%jmax
            else
              if(ExclR%Exc_Rect(1,2,k) > ExclR%imax) ExclR%imax=ExclR%Exc_Rect(1,2,k)
              if(ExclR%Exc_Rect(2,2,k) > ExclR%jmax) ExclR%jmax=ExclR%Exc_Rect(2,2,k)
            end if
          end do
        end if

        if(line(1:9) == "excl_circ") then
          j=index(fich%line(i)," ")
          read(unit=fich%line(i)(j:),fmt=*,iostat=ier)   ExclR%Nexc_Circ
          if(ier /= 0) then
            write(unit=*,fmt="(a)") " => Warning! Error reading number of circular excluded regions in "//trim(arkive)
            return
          end if
          if(allocated(ExclR%Exc_Circ)) deallocate(ExclR%Exc_Circ)
          allocate(ExclR%Exc_Circ(3,ExclR%Nexc_Circ))
          do k=1, ExclR%Nexc_Circ
            i=i+1
            read(unit=fich%line(i),fmt=*,iostat=ier) ExclR%Exc_Circ(:,k)
            if(ier /= 0) then
              write(unit=*,fmt="(a,i3,a)") " => Error reading circular excluded region: ",k, " in file "//trim(arkive)
              return
            end if
          end do
        end if

      End do
      if( ExclR%imax > 0 .and. ExclR%jmax > 0) maxij=.true.
      ! Assign provisionally maximum pixels for excluded regions if not read in file.
      ! It should be fixed at the moment of reading the image size.
      if(.not. maxij) then   !This should never happend
        ExclR%imax=4000
        ExclR%jmax=4000
      end if
      ok=.true.
      ! Output the result of reading excluded regions
      if(maxij) then
         write(unit=iou,fmt="(a,2i5)") " => Maximum ij-indices for excluded regions ", ExclR%imax, ExclR%jmax
      else
         write(unit=iou,fmt="(a)")     " => Maximum ij-indices for excluded regions not given in the file"
         write(unit=iou,fmt="(a,2i5)") " => Provisional values: ", ExclR%imax, ExclR%jmax
      end if
      if( ExclR%Nexc_Rect > 0) then
        write(unit=iou,fmt="(a,i4)") " => Number of rectangular excluded regions ", ExclR%Nexc_Rect
        write(unit=iou,fmt="(a)")    "    i-min   j-min       i-max   j-max"
        do k=1, ExclR%Nexc_Rect
          write(unit=iou,fmt="(tr1,2(2i8,tr4))") ExclR%Exc_Rect(:,1,k), ExclR%Exc_Rect(:,2,k)
        end do
      else
        write(unit=iou,fmt="(a)") " => NO rectangular excluded regions "
      end if
      if( ExclR%Nexc_Circ > 0) then
        write(unit=iou,fmt="(a,i4)") " => Number of circular excluded regions ", ExclR%Nexc_Circ
        write(unit=iou,fmt="(a)")    "    i-cent  j-cent  radius  "
        do k=1, ExclR%Nexc_Circ
          write(unit=iou,fmt="(tr2,2i8,tr4,i4)") ExclR%Exc_Circ(:,k)
        end do
      else
        write(unit=iou,fmt="(a)") " => NO circular excluded regions "
      end if
      return
    End Subroutine Read_Excluded_Regions


    Subroutine Write_Excluded_Regions(ExclR,lun)
      Type(Excluded_Regions_Type), intent(in) :: ExclR
      integer,                     intent(in) :: lun
      !---- Local variables ----!
      integer :: k
      write(unit=lun,fmt="(/,a)") "!  EXCLUDED REGIONS INFORMATION:"
      if(ExclR%Nexc_Rect > 0) then
        write(unit=lun,fmt="(a,i4,a)") "EXCL_RECT ", ExclR%Nexc_Rect,"   !(zmin,xmin) - (zmax,xmax) in pixels"
        do k=1, ExclR%Nexc_Rect
          write(unit=lun,fmt="(tr1,2(2i8,tr4))") ExclR%Exc_Rect(:,1,k), ExclR%Exc_Rect(:,2,k)
        end do
      else
        write(unit=lun,fmt="(a)") "!=> NO rectangular Excluded regions"
      end if

      if( ExclR%Nexc_Circ > 0) then
        write(unit=lun,fmt="(a,i4,a)") "EXCL_CIRC ", ExclR%Nexc_Circ,"   !(z,x)coordinates of centre and radius in pixels"
        do k=1, ExclR%Nexc_Circ
          write(unit=lun,fmt="(tr2,2i8,tr4,i4)") ExclR%Exc_Circ(:,k)
        end do
      else
        write(unit=lun,fmt="(a)") "!=> NO circular excluded regions "
      end if
      return
    End Subroutine Write_Excluded_Regions

  End Module Data_Trt
