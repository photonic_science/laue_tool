# Laue_tool #

This tool was created by the collaboration of Photonic Science(PS) and Institut Laue-Langevin(ILL) as a package for analyzing single crystal neutron diffraction data.

This packages components originate from Esmeralda and CrysFML projects that been developed by the ILL.
https://code.ill.fr/scientific-software/esmeralda  
https://code.ill.fr/scientific-software/crysfml



### How do I get set up? ###

* See build scripts in source/bash_scripts directory.
* For further use please see READMEs of modules Esmeralda and CrysFML.

### Who do I talk to? ###

* Repo owner Photonic Science Engineering Ltd.
* email: sales@photonicscience.com
